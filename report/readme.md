# ToMi Platform

## Hardware

<img src="img/diagram.png" width="400" />

* Chassis: Losi Desert Buggy XL-E 4WD
  * Dimensions: 0.9 (L) x 0.5 (W) x 0.3 (H)
  * Wheels: 190 mm diameter, rubber tires
  * Weight: 15 kg
* Powertrain:
  * Electric powered BLDC motor Dynamite Fuze, 800 RPM/V
  * Steering servo Losi S900S
  * Differentials: center, front axle, rear axle
  * Dynamite ESC (Electric Speed Controller)
* Batteries:
  * 3S1P (11.1 V) - electronics
  * 2 x 4S1P (29.6 V) - BLDC motor
* Computing power:
  * Nvidia Jetson AGX Xavier - image processing, control unit
  * Raspberry Pi 3 with shield Navio2 - generating control PWMs
  * Arduino Mega - reading control commands from remote control, reading hall sensor output
* Camera: ZED Stereo Camera
  * Baseline: 120 mm
  * FOV: 100 deg
  * Aperture: f/2.0
  * Resolution: WVGA@100, 720p@60, 1080p@30
* GNSS/INS: Xsens MTi-7-DK
  * 3-axis gyroscope, 3-axis accelerometer, 3-axis magnetometer, barometer, temperature sensor
  * Sensor fusion engine
  * GNSS antenna Ublox

## Pipeline

<img src="img/diagram2.png" width="800" />

ROS is used as framework for the autonomous system 

### Cone detection

Cone detection is done with the YOLOv3 CNN. YOLOv3 offers a large number of objects it detects. These are redundant in this specific use case, and desired categories (blue, yellow, orange, and big orange cones) that are desired are not part of the YOLOv3 output. As a standard procedure, the YOLOv3 was changed, so it outputs only desired categories and then trained using a database of labeled cones. The approximate size of the training set is 8000 pictures. Since database consists of pictures taken from bigger cars, it needs to be finally tuned using pictures obtained with RC car to adapt to a new position of the camera, which is not represented in the former database.

### Motion planning

The motion planning algorithm is used to generate path and speed reference for the trajectory tracking algorithm.. The current implementation of the motion planning does not generate race track in terms of the fastest way through the track. The motion planning has two steps. First, it finds an approximation of the center-line, thus creating path. Second, it assigns reference speed to each waypoint base on the dynamics of the vehicle creating trajectory. 

The speed reference has to be generated for every point of the path. The challenge is to maximize speed while keeping the car stable and safely navigating through the track. The generator makes two passes through the path. The first pass starts at the end of the path. It determines speed for every previous point as the minimum of maximum cornering speed and the maximum speed for the car to safely decelerate to the current point. The second pass goes through the path in a forward direction, and it can only lower speeds from the previous pass. At each point, it is determined if the speed in the following point is kept or it is lowered so that the car can safely accelerate to that point. This method is maximizing tire traction by reaching the limit of Kamm's circle at each point but not exceeding it. The longitudinal controller then uses both speed and acceleration as feedback and feedforward reference respectively.

### Trajectory tracking

Trajectory tracking or control system is divided into two individual controllers according to the axis in which the system acts. These categories are longitudinal and lateral control. Already existing algorithms are implemented for both. Lateral control is a geometrically based law acting as feedback control. Longitudinal control is done with a simple feedback PI regulator with feedforward from speed reference generator.

