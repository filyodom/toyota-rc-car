# dv_ros

The *brain* of the car. All software running on Xavier goes into this repo. Check the docs in `docs/build/html/index.html`.

![Architecture](docs/source/arch.png)

This ROS package has dependecies on **realsense_ros**, **zed_ros_wrapper** and **ouster_ros**. Information how to install
 these packages can be found [here](packages.md)

## CAN Messages

### Mission critical signals
Messages need for car to stay in some state or transit to another
* Mission select
    * ENUM
    * Can only resived when ASM is in _AS_off_
* Traction System On (TS On)
    * Bool
    * When received, ASM will go to either _AS_Ready_ or _Manual Driving_
* RES_Go
    * Bool
    * When received, ASM will go to AS_Driving
* RES_Stop
    * Bool
    * When received, ASM will go to AS_Emergency 
* AS_Finished
    * Bool
    * Send, when ASM finishes it's mission (or thinks so...)
* AS_Emergency 
    * Bool
    * Can be received or sent when things go badly
        * Received - FSB decides that car has malfunction or _RES Stop_ signal is sent
        * Sent - Path planing algorithms has crashed
* EBS_Supervisor_Status
    * SCS
        * Must be received periodically or transit to _AS Emergency_
    * ENUM
    * Received status of _EBS Supervisor_
* ASM_Status
    * SCS
    * ENUM
    * Sent to _EBS Supervisor_, if not sent the CAR goes to emergency

### Controlling messages
Messages that are needed for driving car. These messages are sent during _AS Driving_. Or if _AS Emergency_, only _SA Angle_ will be sent.
* Motor Torque
    * Not defined yet
    * Will be sent to each motor, about the amount of torque they need to apply
* SA Angle
    * Not defined yet
    * Steering angle
* SB Breaking
    * Probably BOOL
        * Unfortunately we won't have opportunity to break by SB 