## Realsense library
To install all necessary things for Realsense, it can be done through 

`sudo apt-get install ros-melodic-realsense2-camera`

This will install it as one of the ros packages. For more info visit [official repo](https://github.com/IntelRealSense/realsense-ros).

## ZED Camera library

First it's needed to have ZED camera library installed from [here](https://www.stereolabs.com/developers/). 