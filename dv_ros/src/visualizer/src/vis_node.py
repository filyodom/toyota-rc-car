#!/usr/bin/env python
import rospy
import os
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from dv_common.msg import Cones, Pose
from motion_planning.msg import Trajectory
import sensor_msgs
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs.point_cloud2
from sensor_msgs import point_cloud2
import struct
from std_msgs.msg import Header

class Visualizer():
    def __init__(self):
        # self.fig = plt.figure()
        # self.ax = self.fig.add_subplot(111)
        self.yx = []
        self.yy = []
        self.bx = []
        self.by = []
        self.wx = []
        self.wy = []
        self.pubs = {}
        self.counter = 0

        self.configure_ros()
        self.subscribe_to_topics()


    def configure_ros(self):
        if DEBUG:
            rospy.init_node('Visualizer', log_level=rospy.DEBUG)
        else:
            rospy.init_node('Visualizer', log_level=rospy.ERROR)

    def subscribe_to_topics(self):
        rospy.Subscriber('trajectory', Trajectory, self.receive_trajectory)
        rospy.Subscriber('cones', Cones, self.receive_cones)
        rospy.Subscriber('odometry', Pose, self.receive_pose)
        self.pubs['path'] = rospy.Publisher('path_pc', PointCloud2, queue_size=1)
        self.pubs['blue'] = rospy.Publisher('blue_pc', PointCloud2, queue_size=1)
        self.pubs['yellow'] = rospy.Publisher('yellow_pc', PointCloud2, queue_size=1)
        self.pubs['pose'] = rospy.Publisher('pose_pc', PointCloud2, queue_size=1)


    def receive_trajectory(self, msg):
        path = [(np.array(msg.waypoint_x)[i], np.array(msg.waypoint_y)[i], 0) for i in range(len(np.array(msg.waypoint_x)))]
        path_pc = self.coords_2_pc2msg(path, [255, 0, 0])
        path_pc.header.frame_id='map'
        self.pubs['path'].publish(path_pc)
        # plt.plot(self.wx, self.wy, 'r-')
        # with open(os.path.dirname(os.path.realpath(__file__)) + "/data/vis_" + str(self.counter) + ".pdf", 'w') as f:
        #     plt.savefig(f)
        # self.counter += 1
        # plt.clf()
        # plt.cla()
        # plt.close()

    def receive_pose(self, msg):
        pose_x = [(msg.x, msg.y, 0)]
        pose_pc = self.coords_2_pc2msg(pose_x, [255, 255, 255])
        self.pubs['pose'].publish(pose_pc)

    def receive_cones(self, msg):

        R = np.array([[np.cos(msg.detection_pose.heading), -np.sin(msg.detection_pose.heading), msg.detection_pose.x],
                      [np.sin(msg.detection_pose.heading), np.cos(msg.detection_pose.heading), msg.detection_pose.y],
                      [0., 0., 1.]])
        yellow = np.vstack((np.array(msg.yellow_x), np.array(msg.yellow_y)))
        yellow = np.matmul(R, np.vstack((yellow, np.ones(len(np.array(msg.yellow_x)))))).T
        yellow = [(yellow[i, 0], yellow[i, 1], 0) for i in range(len(np.array(msg.yellow_x)))]
        yellow_pc = self.coords_2_pc2msg(yellow, [255, 255, 0])
        self.pubs['yellow'].publish(yellow_pc)
        # self.yx = yellow[:, 0]
        # self.yy = yellow[:, 1]
        blue = np.vstack((np.array(msg.blue_x), np.array(msg.blue_y)))
        blue = np.matmul(R, np.vstack((blue, np.ones(len(np.array(msg.blue_x)))))).T
        blue = [(blue[i, 0], blue[i, 1], 0) for i in range(len(np.array(msg.blue_x)))]
        blue_pc = self.coords_2_pc2msg(blue, [0, 0, 255])
        self.pubs['blue'].publish(blue_pc)
        # self.bx = blue[:, 0]
        # self.by = blue[:, 1]

    def update_plot(self):
        # self.ax.remove()
        if len(self.yx) >0:
            # print self.yx, self.yy
            # plt.plot(self.yx, self.yy)
            # plt.show()
            # plt.pause(0.1)
            # self.ax.remove()
            # self.ax = self.fig.add_subplot(111)
            plt.clf()
            plt.cla()
            plt.close()
            plt.plot(self.yx, self.yy, 'yx')
            plt.plot(self.bx, self.by, 'bx')
            plt.plot(self.wx, self.wy, 'r-')
            plt.show()
            # with open(os.path.dirname(os.path.realpath(__file__)) + "/data/vis_" + str(self.counter) + ".pdf", 'w') as f:
            #     plt.savefig(f)
            # self.counter += 1
        # plt.show(block=True)

    def coords_2_pc2msg(self, coords, color=[255, 0, 0]):
        # create x,y,z,brga pc2 msg from list of xy coords and color

        n = len(coords)

        data = []
        for i in range(n):
            x = float(coords[i][0])
            y = float(coords[i][1])
            z = float(coords[i][2])
            r = int(color[0])
            g = int(color[1])
            b = int(color[2])
            a = 255

            # print x, y, z, r, g, b, a
            rgba = struct.unpack('I', struct.pack('BBBB', b, g, r, a))[0]
            # print hex(rgba)

            pt = [x, y, z, rgba]
            data.append(pt)

        fields = [PointField('x', 0, PointField.FLOAT32, 1),
                  PointField('y', 4, PointField.FLOAT32, 1),
                  PointField('z', 8, PointField.FLOAT32, 1),
                  PointField('rgb', 12, PointField.UINT32, 1)
                  ]

        header = Header()
        header.stamp = rospy.Time.now()
        header.frame_id = "base_footprint"
        msg = point_cloud2.create_cloud(header, fields, data)

        return msg

if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)

    # plt.plot(np.linspace(0, 10))
    # plt.show()
    # plt.pause(100)

    v = Visualizer()
    rospy.spin()
    # rate = rospy.Rate(1)
    # while not rospy.is_shutdown():
    #     v.update_plot()
    #     rate.sleep()
