#!/usr/bin/env python
import time
import numpy as np
import rospy
import tf
from std_msgs.msg import Float32MultiArray, Float64, UInt8, Float32
from dv_common.msg import Pose
from motion_planning.msg import Trajectory
from dv_common.BaseNode import BaseNode
from dv_common.constants import CarParams
from tomi.msg import ControlCommand


class getter():
    def __init__(self):
        self.publishers = {}
        self.pos_x = 0
        self.pos_y = 0
        self.vel = 0
        self.heading = 0
        self.steering_angle = 0
        self.previous_steering_angle_meas = 0
        self.steering_cmd = 0
        self.err = 0
        self.yaw = 0
        self.path = np.array([])

        self.init_pubs()
        self.configure_ros()
        self.subscribe_to_topics()
        self.listener = tf.TransformListener()

    def configure_ros(self):
        if DEBUG:
            rospy.init_node('getter', log_level=rospy.DEBUG)
        else:
            rospy.init_node('getter', log_level=rospy.ERROR)

    def subscribe_to_topics(self):
        rospy.wait_for_message('speed', Float32)
        rospy.wait_for_message('trajectory', Trajectory)
        # rospy.Subscriber('odometry', Pose, self.receive_pose, queue_size=1)
        rospy.Subscriber('trajectory', Trajectory,
                         self.receive_trajectory, queue_size=1000)
        rospy.Subscriber('steering_angle', Float64,
                         self.receive_steering, queue_size=1)
        rospy.Subscriber('speed', Float32, self.receive_speed, queue_size=1)
        self.mint = rospy.Time.now()

    def init_pubs(self):
        self.publishers['steering_cmd'] = rospy.Publisher('control_command', ControlCommand, queue_size=1)
        self.publishers['heading_err'] = rospy.Publisher('heading_err', Float64, queue_size=1)
        self.publishers['cross_err'] = rospy.Publisher('cross_err', Float64, queue_size=1)
        self.publishers['steer'] = rospy.Publisher('steer', Float64, queue_size=1)
        # self.publishers['test'] = rospy.Publisher('test', Float32)

    def receive_speed(self, msg):
        self.vel = msg.data

    def receive_pose(self, msg):
        self.pos_x = msg.x
        self.pos_y = msg.y
        self.vel = msg.linear_velocity
        self.yaw = msg.angular_velocity
        self.heading = msg.heading
        #print(self.heading)

    def receive_trajectory(self, msg):
        self.path = np.vstack((msg.waypoint_x, msg.waypoint_y)).T
        self.ref_heading = msg.waypoint_heading
        self.curvature = msg.curvature
        self.speed = np.array(msg.waypoint_speed)
        
    def receive_steering(self, msg):
        self.steering_angle = msg.data

    def receive_as(self, state):
        if self.state == AS.AS_DRIVING and AS(int(state.data)) == AS.AS_READY:
            self.pos_x = 0
            self.pos_y = 0
            self.vel = 0
            self.heading = 0
            self.steering_angle = 0
            self.steering_cmd = 0
            self.err = 0
            self.path = np.array([])

        self.state = AS(int(state.data))

    def receive_mission(self, mission):
        self.mission = Mission(int(mission.data))

    
    def update(self):
        t = rospy.Time.now()
        t = t.secs - self.mint.secs + t.nsecs*1e-9
        
        if len(self.path) == 0:
            self.steering_cmd = 0
            return
        k = 200
        try:
            self.listener.waitForTransform("map", "base_footprint", rospy.Time(0), rospy.Duration(0.01))
            (trans, rot) = self.listener.lookupTransform('map', 'base_footprint', rospy.Time(0))
            # print(trans, rot)
        except:
            print('Wait for transform took too long')
            return

        T_mat = tf.transformations.compose_matrix(angles=tf.transformations.euler_from_quaternion(rot), translate=trans)
        
        self.pos_x = trans[0]
        self.pos_y = trans[1]
        _, _, self.heading = tf.transformations.euler_from_quaternion(rot, axes='sxyz')

        dist_err = np.linalg.norm(self.path - [[self.pos_x, self.pos_y]], axis=1)
        min_idx = np.argmin(dist_err)

        heading_err = (self.ref_heading[min_idx] -
                       self.heading + np.pi) % (2*np.pi) - np.pi

        R = np.array([[np.cos(self.ref_heading[min_idx]), np.sin(self.ref_heading[min_idx])],
                      [-np.sin(self.ref_heading[min_idx]), np.cos(self.ref_heading[min_idx])]])
        p = np.matmul(R, np.array(
            [self.pos_x, self.pos_y]) - self.path[min_idx])

        cross_err = -p[1]
        
        s = "{}, {}, {}, {} ".format(t, self.ref_heading[min_idx], self.heading, cross_err)
        with open('heading.csv','a') as fd:
            fd.write(s+'\n')
        
        if len(self.path) > 0 and self.vel > 0.9:
            speed_ref = self.speed[min_idx]
            
            s = "{}, {}, {}".format(t, speed_ref, self.vel)
            with open('speed.csv','a') as fd:
                fd.write(s+'\n')

        s = "{}, {}, {}".format(t, self.pos_x, self.pos_y)
        with open('pos.csv','a') as fd:
            fd.write(s+'\n')

        end = rospy.Time.now()
        end = end.secs - self.mint.secs + end.nsecs*1e-9
        print((end-t)*1000)

if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)
    # rospy.init_node('lat_control_node')
    get = getter()


    rate = rospy.Rate(100)
    while not rospy.is_shutdown():
        get.update()
        rate.sleep()
