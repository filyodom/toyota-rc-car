from enum import Enum, IntEnum


class AS(IntEnum):
    """Enum describing the Autonomous state

    """
    AS_READY = 0
    AS_DRIVING = 1
    AS_FINISHED = 2


class Mission(IntEnum):
    """Enum describing the current mission

    """
    NOT_SELECTED = 0
    AUTOCROSS = 1
    TRACKDRIVE = 2


class MissionStatus(Enum):
    """Enum describing the status of the current mission

    """
    UNAVAILABLE = 0  #: test
    ONGOING = 1  #: test
    FINISHED = 2

class CarParams():
    lf = 1.296
    lr = 1.230
    I = 15
    m = 4
    R = 0.2
    J = 0.138

    mu = 0.7

    Aref = 0
    ro = 1.225
    C_L = 0
    C_D = 0

    By = 0.184
    Cy = 1.45
    Dy = 1.
    Ey = -0.3

    Bx = 0.184
    Cx = 1.45
    Dx = 1.
    Ex = -0.3

    kss = 0
    kh = 0.8
    kd = 1.2
    kdyaw = 0
    kdsoft = 1

    kp = 0.03
    ki = 0.04

    max_speed = 2.5

    max_acc = 4
    max_dec = 2
    max_cone_dist = 7
