class BaseNode(object):
    def __init__(self):
        self.publishers = dict()
        self.configure_ros()
        self.subscribe_to_topics()

    def configure_ros(self):
        raise NotImplementedError("You need to override this method")

    def subscribe_to_topics(self):
        raise NotImplementedError("You need to override this method")

    def publish(self):
        raise NotImplementedError("You need to override this method")
