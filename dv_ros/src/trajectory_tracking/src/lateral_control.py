#!/usr/bin/env python
import time
import numpy as np
import rospy
import tf
from std_msgs.msg import Float32MultiArray, Float64, UInt8, Float32
from dv_common.msg import Pose
from motion_planning.msg import Trajectory
from dv_common.BaseNode import BaseNode
from dv_common.constants import CarParams
from tomi.msg import ControlCommand


class Lat_controls(BaseNode):
    def __init__(self):
        self.publishers = {}
        self.pos_x = 0
        self.pos_y = 0
        self.vel = 0
        self.heading = 0
        self.steering_angle = 0
        self.previous_steering_angle_meas = 0
        self.steering_cmd = 0
        self.err = 0
        self.yaw = 0
        self.path = np.array([])
        super(Lat_controls, self).__init__()

        self.init_pubs()
        self.listener = tf.TransformListener()

    def configure_ros(self):
        if DEBUG:
            rospy.init_node('lat_control_node', log_level=rospy.DEBUG)
        else:
            rospy.init_node('lat_control_node', log_level=rospy.ERROR)

    def subscribe_to_topics(self):
        rospy.Subscriber('odometry', Pose, self.receive_pose, queue_size=1)
        rospy.Subscriber('trajectory', Trajectory,
                         self.receive_trajectory, queue_size=1)
        rospy.Subscriber('steering_angle', Float64,
                         self.receive_steering, queue_size=1)
        rospy.Subscriber('speed', Float32, self.receive_speed, queue_size=1)
        rospy.wait_for_message('speed', Float32)
        rospy.wait_for_message('trajectory', Trajectory)

    def init_pubs(self):
        self.publishers['steering_cmd'] = rospy.Publisher('control_command', ControlCommand, queue_size=1)
        self.publishers['heading_err'] = rospy.Publisher('heading_err', Float64, queue_size=1)
        self.publishers['cross_err'] = rospy.Publisher('cross_err', Float64, queue_size=1)
        self.publishers['steer'] = rospy.Publisher('steer', Float64, queue_size=1)
        # self.publishers['test'] = rospy.Publisher('test', Float32)

    def receive_speed(self, msg):
        self.vel = msg.data

    def receive_pose(self, msg):
        self.pos_x = msg.x
        self.pos_y = msg.y
        self.vel = msg.linear_velocity
        self.yaw = msg.angular_velocity
        self.heading = msg.heading

    def receive_trajectory(self, msg):
        self.path = np.vstack((msg.waypoint_x, msg.waypoint_y)).T
        self.ref_heading = msg.waypoint_heading
        self.curvature = msg.curvature

    def receive_steering(self, msg):
        self.steering_angle = msg.data

    def receive_as(self, state):
        if self.state == AS.AS_DRIVING and AS(int(state.data)) == AS.AS_READY:
            self.pos_x = 0
            self.pos_y = 0
            self.vel = 0
            self.heading = 0
            self.steering_angle = 0
            self.steering_cmd = 0
            self.err = 0
            self.path = np.array([])

        self.state = AS(int(state.data))

    def receive_mission(self, mission):
        self.mission = Mission(int(mission.data))

    def publish(self):
        msg = ControlCommand()
        msg.command = 1
        msg.value = self.steering_cmd
        self.publishers['steering_cmd'].publish(msg)
        self.publishers['steer'].publish(Float64(data=msg.value))

    def update(self):
        # if self.state is not AS.AS_DRIVING:
        #     # print("LAC not")
        #     return

        # if len(self.path) == 0 or self.vel == 0:
        if len(self.path) == 0:
            self.steering_cmd = 0
            return
        k = 200
        try:
            self.listener.waitForTransform("map", "base_footprint", rospy.Time(0), rospy.Duration(0.1))
            (trans, rot) = self.listener.lookupTransform('map', 'base_footprint', rospy.Time(0))
            # print(trans, rot)
        except:
            return

        T_mat = tf.transformations.compose_matrix(angles=tf.transformations.euler_from_quaternion(rot), translate=trans)
        #print(np.matmul(T_mat, np.array([0,0,0,1])))
        self.pos_x = trans[0]
#        self.pos_x = 0
        self.pos_y = trans[1]
#        self.pos_y = 0
        _, _, self.heading = tf.transformations.euler_from_quaternion(rot, axes='sxyz')
        #self.heading *= -1
#        self.heading = 0
        #print(self.path)
        dist_err = np.linalg.norm(self.path - [[self.pos_x, self.pos_y]], axis=1)
        min_idx = np.argmin(dist_err)
        #print(dist_err[min_idx])
        #print('Car:', self.pos_x, self.pos_y, self.heading)
        #print('Point:', self.path[min_idx], 'Heading:', self.ref_heading[min_idx])

        heading_err = (self.ref_heading[min_idx] -
                       self.heading + np.pi) % (2*np.pi) - np.pi
        self.publishers['heading_err'].publish(Float64(data=heading_err))
        # self.publishers['test'].publish(Float32(data=heading_err))

        R = np.array([[np.cos(self.ref_heading[min_idx]), np.sin(self.ref_heading[min_idx])],
                      [-np.sin(self.ref_heading[min_idx]), np.cos(self.ref_heading[min_idx])]])
        #print(np.array([self.pos_x, self.pos_y]) - self.path[min_idx])
        #print(self.path)
        #print(np.array([self.pos_x, self.pos_y]), self.path[min_idx])
        p = np.matmul(R, np.array(
            [self.pos_x, self.pos_y]) - self.path[min_idx])

        cross_err = -p[1]
        self.publishers['cross_err'].publish(Float64(data=cross_err))

        # heading_vec = np.array([np.cos(self.ref_heading[min_idx]), np.cos(self.ref_heading[min_idx])])
        # cross_err = np.linalg.norm( np.cross(heading_vec, self.path[min_idx] - np.array([self.pos_x, self.pos_y])))/np.linalg.norm(heading_vec)

        if min_idx < len(self.ref_heading) - 1:
            ccw_turn = (
                self.ref_heading[min_idx+1] - self.ref_heading[min_idx] + np.pi) % (2*np.pi) - np.pi > 0
        else:
            ccw_turn = (
                self.ref_heading[min_idx] - self.ref_heading[min_idx-1] + np.pi) % (2*np.pi) - np.pi > 0

#        print(min_idx, self.curvature)
#        if ccw_turn:
#            ref_curv = -self.curvature[min_idx]
#        else:
#            ref_curv = self.curvature[min_idx]
        ref_curv = 0
        yaw_err = self.vel*ref_curv - self.yaw

        self.steering_cmd = CarParams.kh*heading_err - CarParams.kss*(self.vel**2)*ref_curv + \
            np.arctan((CarParams.kd * cross_err) / (CarParams.kdsoft + self.vel)) + \
            CarParams.kdyaw*yaw_err + \
            0*(self.previous_steering_angle_meas - self.steering_angle)
        self.steering_cmd /= np.pi/9
        self.steering_cmd = np.clip(self.steering_cmd, -1, 1)

        print('cross: ', cross_err, 'heading: ', heading_err, 'steering: ', self.steering_cmd)
        # print('Steering:', self.steering_cmd)
        #print( CarParams.kss*(self.vel**2)*ref_curv)
        # print(self.ref_heading[min_idx], heading_err, cross_err, self.steering_cmd, self.steering_angle)


if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)
    # rospy.init_node('lat_control_node')
    controls = Lat_controls()

    rate = rospy.Rate(100)
    while not rospy.is_shutdown():
        controls.update()
        controls.publish()
        rate.sleep()
