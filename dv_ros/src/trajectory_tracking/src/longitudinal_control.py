#!/usr/bin/env python
import time
import numpy as np
import rospy
import tf
from std_msgs.msg import Float32MultiArray, UInt8, Float64, Float32
from dv_common.msg import Pose, MotorTorques
from dv_common.BaseNode import BaseNode
from motion_planning.msg import Trajectory
from tomi.msg import ControlCommand
from dv_common.constants import CarParams
# from fs_msgs.msg import ControlCommand
# from trajectory_tracking.msg import MotorTorques

class Long_controls(BaseNode):
    def __init__(self):
        self.publishers = {}
        self.subscribers = {}
        self.pos_x = 0
        self.pos_y = 0
        self.vel = 0
        self.trq = 0
        self.err_int = 0
        self.path = np.array([])
        self.steering_cmd = 0

        super(Long_controls, self).__init__()

        self.listener = tf.TransformListener()
        self.init_pubs()

    def configure_ros(self):
        if DEBUG:
            rospy.init_node('lon_control_node', log_level=rospy.DEBUG)
        else:
            rospy.init_node('lon_control_node', log_level=rospy.ERROR)

    def subscribe_to_topics(self):
        rospy.Subscriber('speed', Float32, self.receive_speed)
        rospy.Subscriber('trajectory', Trajectory, self.receive_trajectory)
        rospy.wait_for_message('speed', Float32)
        rospy.wait_for_message('trajectory', Trajectory)

    def init_pubs(self):
        self.publishers['trq'] = rospy.Publisher('control_command', ControlCommand, queue_size=1)
        self.publishers['cmd'] = rospy.Publisher('torque_cmd', Float64, queue_size=1)
        self.publishers['ref'] = rospy.Publisher('speed_ref', Float64, queue_size=1)
        

    def receive_speed(self, msg):
        self.vel = msg.data

    def receive_trajectory(self, msg):
        self.path = np.vstack([msg.waypoint_x, msg.waypoint_y]).T
        self.speed = np.array(msg.waypoint_speed)

    def receive_steering_cmd(self, msg):
        self.steering_cmd = msg.data

    def publish(self):
        # if self.state is not AS.AS_DRIVING:
        #     return
        self.trq = np.clip(self.trq, -1, 1)
        msg = ControlCommand()
        msg.command = 0
        msg.value = self.trq
#        msg.value = 0.115
#        print('torque', self.trq)
        #msg.value = 0
        self.publishers['trq'].publish(msg)
        self.publishers['cmd'].publish(Float64(data=self.trq))
        rospy.loginfo(msg)

    def update(self):
        # if self.state is not AS.AS_DRIVING:
        #     #print("LOC not")
        #     return
        try:
            self.listener.waitForTransform("base_footprint", "map", rospy.Time(0), rospy.Duration(0.1))
            (trans, rot) = self.listener.lookupTransform('base_footprint', 'map', rospy.Time(0))
        except:
            return
        self.pos_x = trans[0]
        self.pos_y = trans[1]
        _, _, self.heading = tf.transformations.euler_from_quaternion(rot)

        if len(self.path) > 0 and self.vel > 0.9:
            dist_err = np.linalg.norm(self.path - [[self.pos_x, self.pos_y]], axis=1)
            idx = np.argmin(dist_err)
            speed_ref = self.speed[idx]
            self.publishers['ref'].publish(Float64(data=speed_ref))
            speed_ref = 2
            err = (speed_ref - self.vel)
            if np.isnan(err) or np.abs(err) < 0.2:
                self.trq = 0
                return
            offset = 0. if err < 0 else 0.09
            self.trq = CarParams.kp * err + CarParams.ki * self.err_int + offset
            self.err_int = 2*self.err_int/3 + err
        elif len(self.path) > 0:
            self.trq = 0.115
        else:
            self.trq = 0



if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)
    # rospy.init_node('lon_control_node')
    controls = Long_controls()

    rate = rospy.Rate(100)
    while not rospy.is_shutdown():
        controls.update()
        controls.publish()
        rate.sleep()
