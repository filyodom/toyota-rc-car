#!/usr/bin/env python
import rospy
import cv2 as cv
import numpy as np
from fs_msgs.msg import ControlCommand


if __name__ == '__main__':
    rospy.init_node('drive_forward', log_level=rospy.DEBUG)
    publisher = rospy.Publisher("/fsds/control_command", ControlCommand, queue_size=1)

    rate = rospy.Rate(5)
    while not rospy.is_shutdown():
        
        cmd = ControlCommand()
        cmd.throttle = 0.1
        cmd.steering = 0
        cmd.brake = 0

        publisher.publish(cmd)

        rate.sleep()
