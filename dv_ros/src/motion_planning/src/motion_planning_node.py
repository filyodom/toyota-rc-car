#!/usr/bin/env python
import time
import numpy as np
import warnings
import rospy
import tf
from std_msgs.msg import UInt8, Float32
from dv_common.msg import Pose, Cones
from dv_common.constants import CarParams
from motion_planning.msg import Trajectory
from scipy.interpolate import splprep, splev, interp1d


class MotionPlanner():
    def __init__(self, debugging=False):
        # Init ROS stuff
        self.cone_publisher = None
        self.configure_ros()
        self.subscribe_to_topics()

        # Init state variables
        # self.pos_x = 0
        # self.pos_y = 0

        self.sorted_blue_cones = []
        self.sorted_yellow_cones = []
        self.start_points = np.array([[0,0]])

        self.filling_cones_distance = 0.5
        self.clockwise = -1

        self.k = 0
        self.c = None
        self.k_past = 0
        self.vel = 0
        self.debugging = debugging
        self.path = []
        if self.debugging:
            self.ks = []
            self.cs = []
            self.direction_changes = []

        self.pubs = {}
        self.init_pubs()
        self.listener = tf.TransformListener()

    def configure_ros(self):
        if DEBUG:
            rospy.init_node('Planner', log_level=rospy.DEBUG)
        else:
            rospy.init_node('Planner', log_level=rospy.ERROR)
    def init_pubs(self):
        self.pubs['trajectory'] = rospy.Publisher('trajectory', Trajectory, queue_size=1)

    def subscribe_to_topics(self):
        rospy.Subscriber('cones', Cones, self.receive_cones, queue_size=1)
        rospy.Subscriber('mission', UInt8, self.receive_mission)
        rospy.Subscriber('odometry', Pose, self.receive_pose)
        rospy.Subscriber('speed', Float32, self.receive_speed)

    def publish(self):
        msg = Trajectory()
        msg.waypoint_x = self.path[:, 0].astype(np.float64)
        msg.waypoint_y = self.path[:, 1].astype(np.float64)
        msg.waypoint_heading = self.ref_heading.astype(np.float64)
        msg.waypoint_speed= self.speed.astype(np.float64)
        msg.curvature = self.curvature.astype(np.float64)
        self.pubs['trajectory'].publish(msg)

    def receive_speed(self, msg):
        self.vel = msg.data
        if self.vel < 0.7:
            self.vel = 0

    def receive_cones(self, cones):
        start_time = time.time()

        yellow_cones = np.vstack([cones.yellow_x, cones.yellow_y]).T
        dist = np.linalg.norm(yellow_cones, axis=1)
        yellow_cones = yellow_cones[dist<CarParams.max_cone_dist]

        blue_cones = np.vstack([cones.blue_x, cones.blue_y]).T
        dist = np.linalg.norm(blue_cones, axis=1)
        blue_cones = blue_cones[dist<CarParams.max_cone_dist]

        if len(blue_cones) + len(yellow_cones) > 1 and (len(blue_cones) > 0 or len(yellow_cones) > 0):
            self.reset()
            self.start_points = [[0, 0]]

            self.listener.waitForTransform("map", "base_footprint", cones.header.stamp, rospy.Duration(1.0))
            (trans, rot) = self.listener.lookupTransform("map", "base_footprint", cones.header.stamp)

            self.find_path(blue_cones, yellow_cones, 50)

            self.start_points = np.array(self.start_points)
            if len(self.start_points) < 2:
                return

            # if len(self.path) > 0:
            #     min_idx = np.argmin(np.linalg.norm(self.path-np.array([[trans[0], trans[1]]]), axis=1))
            #     dist_planned = self.path - np.roll(self.path, 1, axis=0)
            #     #print(dist_planned)
            #     # dist_traveled = np.sum(np.linalg.norm(dist_planned[1:min_idx, :], axis=1))
            #     # if dist_traveled < 5:
            #     dist_planned = np.sum(np.linalg.norm(dist_planned[min_idx+1:, :], axis=1))

            #     new_dist = self.start_points[1:, :] - np.roll(self.start_points, 1, axis=0)[1:, :]
            #     new_dist = np.sum(np.linalg.norm(new_dist, axis=1))
            #     #print(new_dist, dist_planned)

            #     if dist_planned > new_dist:
            #         return

            self.path = np.array(self.start_points)

            euler = tf.transformations.euler_from_quaternion(rot, axes='sxyz')
            #euler = (euler[0], euler[1], -1*euler[2])
            T_mat = tf.transformations.compose_matrix(angles=euler, translate=trans)
            self.path = np.matmul(T_mat,
                                  np.vstack((self.path.T, np.zeros(len(self.path)), np.ones(len(self.path))))).T[:, :2]


            # self.smoothening()
            self.calculate_heading()
            if len(blue_cones) == 0 or len(yellow_cones) == 0:
                self.speed = np.ones(len(self.path))
            else:
                self.plan_speed()
            self.path = np.array(self.path)[1:, :]
            self.speed = np.array(self.speed)[1:]
            self.ref_heading = np.array(self.ref_heading)[1:]
            self.curvature = np.array(self.curvature)[1:]

            # rospy.loginfo(cones.detection_pose)
#            self.ref_heading = (self.ref_heading + euler[2] + np.pi)%(2*np.pi) - np.pi
            self.publish()
        # print('Time: ' + str(time.time()-start_time))

    def receive_mission(self, mission):
        self.mission = Mission(int(mission.data))

    def receive_pose(self, msg):
        self.pos_x = msg.x
        self.pos_y = msg.y
        self.heading = msg.heading

    def reset(self, clockwise=-1):
        self.sorted_blue_cones = []
        self.sorted_yellow_cones = []
        self.start_points = []
        self.clockwise = clockwise

        self.k = None
        self.c = None
        self.k_past = 0
        #self.path = []
        self.ref_heading = []
        self.speed = []
        if self.debugging:
            self.ks = []
            self.cs = []
            self.direction_changes = []

    def fill_missing(self, B, Y):
        #print('filling')
        B = B[np.argsort(np.linalg.norm(B, axis=1))]
        Y = Y[np.argsort(np.linalg.norm(Y, axis=1))]
        yellow = True if len(Y) > len(B) else False
        if yellow:
            to_fill = B
            full = Y
        else:
            to_fill = Y
            full = B

        for idx in range(len(to_fill), len(full)):
            if idx == 0:
                vec = full[1, :] - full[0, :]
                vec = vec/np.linalg.norm(vec)
                if yellow:
                    vec = np.matmul(np.array([[0, -1], [1, 0]]), vec)
                else:
                    vec = np.matmul(np.array([[0, 1], [-1, 0]]), vec)
            elif idx == len(full) - 1:
                vec = full[-1, :] - full[-2, :]
                vec = vec/np.linalg.norm(vec)
                if yellow:
                    vec = np.matmul(np.array([[0, -1], [1, 0]]), vec)
                else:
                    vec = np.matmul(np.array([[0, 1], [-1, 0]]), vec)
            else:
                vec1 = full[idx-1, :] - full[idx, :]
                vec2 = full[idx+1, :] - full[idx, :]
                vec1 = vec1/np.linalg.norm(vec1)
                vec2 = vec2/np.linalg.norm(vec2)
                angle = np.arccos(np.dot(vec1, vec2))/2
                if yellow:
                    vec = np.matmul(np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]]), vec2)
                else:
                    vec = np.matmul(np.array([[np.cos(-angle), -np.sin(-angle)], [np.sin(-angle), np.cos(-angle)]]), vec2)

            to_fill = np.vstack((to_fill, full[idx, :] + vec * 3.5))
        if yellow:
            return to_fill, full
        else:
            return full, to_fill

        # mask = np.array([1,-1]) if right else np.array([-1,1])
        # nvs = []370
        # for sv, co in zip((B-A, C-B),(A,B)):
        #     sv = sv/np.linalg.norm(sv)
        #     nv = np.array([sv[1],sv[0]])
        #     nv = nv*mask
        #     nvs.append(co+nv*self.filling_cones_distance)
        # return np.array(nvs)

    def is_already_added(self, point):
        np.all(np.isin(point, self.sorted_yellow_cones))
        return (np.all(np.isin(point, self.sorted_yellow_cones)) or np.all(np.isin(point, self.sorted_blue_cones)))

    def points_above_normal(self, points):
        return points[np.sign((points[:, 1] - self.k * points[:, 0] - self.c)) == np.sign(self.k) * self.clockwise]

    def find_closest_one(self, points):
        closest_index = np.argmin(np.linalg.norm(points - self.start_points[-1], axis=1))
        closest_cone = points[closest_index]
        return closest_cone

    def calculate_center(self, pointB, pointY):
        return np.array([(pointB[0] - pointY[0]) / 2 + pointY[0], (pointB[1] - pointY[1]) / 2 + pointY[1]])

    def return_stack(self, object_name):
        if object_name == "yellow cones":
            return np.vstack(self.sorted_yellow_cones)
        elif object_name == "blue cones":
            return np.vstack(self.sorted_blue_cones)
        elif object_name == "centers":
            return np.vstack(self.start_points)

    def find_line_parameters(self, pointB, pointY, normal=True):
        k = (pointB[1] - pointY[1]) / (pointB[0] - pointY[0])
        self.k_past = self.k
        self.k = -1 / k if normal else k
        c = pointB[1] - self.k * pointB[0]
        self.c = c

    def check_direction(self):
        if self.k != None and (self.k - self.k_past) == 0:  # and not(self.auxiliary_variable):
            self.clockwise = - self.clockwise

    def find_next_center(self, pointsB, pointsY, step=None, verbose=True):
        self.find_line_parameters(self.start_points[-1], self.start_points[-2])
        self.check_direction()

        if self.debugging:
            self.ks.append(self.k)
            self.cs.append(self.c)
            self.direction_changes.append(self.clockwise)

        B_hat = self.points_above_normal(pointsB)
        Y_hat = self.points_above_normal(pointsY)
        # set_trace()
        b = self.find_closest_one(B_hat)
        y = self.find_closest_one(Y_hat)

        if not self.is_already_added(b):
            self.sorted_blue_cones.append(b)
        if not self.is_already_added(y):
            self.sorted_yellow_cones.append(y)

        s = self.calculate_center(b, y)
        self.start_points.append(s)
        if verbose:
            if step != None:
                print("Step {} done!".format(step + 1))
                # print(f"y={-1/self.k*b[0]+self.c},k={self.c}, b={self.c}")
            else:
                print("Step done!")

    def find_path(self, B, Y, n_steps, verbose=False):

        # if we detect only 3 or more cones with same color we add predicted ones
        # if B.size==0 and Y.size >=6:
        #     Y = Y[np.argsort(Y[:,1])]  # sorting cones with respect to "y" => 1 or with respect to "x" => 0
        #     B = self.fill_missing(Y[0,:],Y[1,:],Y[2,:], yellow=False)
        # elif Y.size==0 and B.size >=6:
        #     B = B[np.argsort(B[:,1])]
        #     Y = self.fill_missing(B[0,:],B[1,:],B[2,:], yellow=True)

        if B.size == 0 or Y.size == 0:
            B, Y = self.fill_missing(B, Y)

        if n_steps < 1:
            raise ValueError("Number of steps must be positive!!")
        """
            B is set of all blue cones
            Y is set of all yellow cones
        """
        # initializing loop
        # step 1)
        b_0 = self.find_closest_one(B)
        y_0 = self.find_closest_one(Y)
        self.sorted_blue_cones.append(b_0)
        self.sorted_yellow_cones.append(y_0)

        s_1 = self.calculate_center(b_0, y_0)
        self.start_points.append(s_1)
        if verbose:
            print("Step 1 done!")

        # step 2)
        # TODO upravit do jedne funkce
        if n_steps > 1:
            try:
                self.find_line_parameters(b_0, y_0, normal=False)  # special case of separate line for 2nd step
                B_hat = self.points_above_normal(B)
                Y_hat = self.points_above_normal(Y)
                # set_trace()
                b_1 = self.find_closest_one(B_hat)
                #print("test")
                y_1 = self.find_closest_one(Y_hat)
                self.sorted_blue_cones.append(b_1)
                self.sorted_yellow_cones.append(y_1)

                s_2 = self.calculate_center(b_1, y_1)
                self.start_points.append(s_2)
                if verbose:
                    print("Step 2 done!")
            except ValueError as err:
                    # catching specific error
                    if str(err) == "attempt to get argmin of an empty sequence":
                        warnings.warn("Too many iteration and not enough cones!")
                    else:
                        raise ValueError(str(err))
        # every other step
        if n_steps > 2:
            for step in range(n_steps - 2):
                try:
                    self.find_next_center(B, Y, step + 2, verbose=verbose)
                    if self.debugging:
                        s = 'k=' + self.k + ' c=' + self.c + ' clockwise' + self.clockwise
                except ValueError as err:
                    # catching specific error
                    if str(err) == "attempt to get argmin of an empty sequence":
                        warnings.warn("Too many iteration!")
                        break
                    else:
                        raise ValueError(str(err))

    def calculate_heading(self):
        self.ref_heading = np.empty(len(self.path))
        self.ref_heading[0] = np.arctan2(self.path[1, 1]-self.path[0, 1], self.path[1, 0] - self.path[0, 0])
        for idx in range(1, len(self.path)-1):
            self.ref_heading[idx] = np.arctan2(self.path[idx+1, 1] - self.path[idx-1, 1], self.path[idx+1, 0] - self.path[idx-1, 0])
        self.ref_heading[-1] = np.arctan2(self.path[-1, 1] - self.path[-2, 1], self.path[-1, 0] - self.path[-2, 0])

    def smoothening(self):
        dist = self.start_points[1:, :] - np.roll(self.start_points, 1, axis=0)[1:, :]
        dist = np.append([0], np.cumsum(np.linalg.norm(dist, axis=1)))

        # f = interp1d(dist, self.path.T)
        # self.path = f(np.linspace(0, dist[-1], 100)).T
        
        if len(self.path) > 3:
           tck, u = splprep(self.path.T, k=2)
           self.path = np.array(splev(np.linspace(0, 1, 100), tck)).T
        elif len(self.path) > 2:
            tck, u = splprep(self.path.T, k=1)
            # print tck
            self.path = np.array(splev(np.linspace(0, 1, 50), tck)).T

    def get_curvature(self, A, B, C):
        #D = np.cross(B - A, C - A)
        b = np.linalg.norm(C - A)
        c = np.linalg.norm(A - B)
        a = np.linalg.norm(B - C)
        #R = a * b * c / 2 / np.linalg.norm(D)
        area = np.abs((A[0] * (B[1] - C[1]) + B[0] * (C[1] - A[1]) + C[0] * (A[1] - B[1])) / 2)
        return (4 * area) / (a * b * c)
        #return 1/R

    def plan_speed(self):
        self.curvature = np.empty(len(self.path))

        # self.curvature[0] = np.abs((self.ref_heading[1] - self.ref_heading[0] + np.pi)%(2*np.pi))/np.linalg.norm(self.path[1] - self.path[0])
        self.curvature[0] = 0

        for idx in range(1, len(self.path)-1):
            self.curvature[idx] = self.get_curvature(self.path[idx], self.path[idx-1], self.path[idx+1])

        self.curvature[-1] = 0

        self.speed = np.sqrt(CarParams.mu*9.81/self.curvature) - 0.01
        self.speed[self.speed > CarParams.max_speed] = CarParams.max_speed

        for idx in range(len(self.path)-1, 0, -1):
            ds = np.linalg.norm(self.path[idx] - self.path[idx-1])
            a_lim = (self.speed[idx-1]**2 - self.speed[idx]**2)/(2*ds)
            F_dis = (CarParams.ro*CarParams.C_D*self.speed[idx]**2)/2
            F_y = CarParams.m*(self.speed[idx]**2)*self.curvature[idx]
            F_z = CarParams.m*9.81 + (CarParams.ro*CarParams.C_D*self.speed[idx]**2)/2
            a_max = (F_dis + np.sqrt((CarParams.mu*F_z)**2 - F_y**2))/CarParams.m
            a = np.min([a_lim, a_max, CarParams.max_dec])
            self.speed[idx-1] = np.sqrt(self.speed[idx]**2 + 2*a*ds)

        self.speed[0] = np.min([self.vel, self.speed[0]])
        for idx in range(len(self.path)-1):
            ds = np.linalg.norm(self.path[idx] - self.path[idx-1])
            a_lim = (self.speed[idx+1]**2 - self.speed[idx]**2)/(2*ds)
            F_dis = (CarParams.ro*CarParams.C_D*self.speed[idx]**2)/2
            F_y = CarParams.m*(self.speed[idx]**2)*self.curvature[idx]
            F_z = CarParams.m*9.81 + (CarParams.ro*CarParams.C_D*self.speed[idx]**2)/2
            a_max = (-F_dis + np.sqrt((CarParams.mu*F_z)**2 - F_y**2))/CarParams.m
            a = np.min([a_lim, a_max, CarParams.max_acc])
            self.speed[idx-1] = np.sqrt(self.speed[idx]**2 + 2*a*ds)

if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)

    planner = MotionPlanner()

    rospy.spin()
    # rate = rospy.Rate(1)
    # while not rospy.is_shutdown():
    #     # planner.publish()
    #     rate.sleep()
