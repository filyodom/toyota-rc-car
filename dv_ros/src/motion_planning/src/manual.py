#!/usr/bin/env python
import rospy
import cv2
import numpy as np
from fs_msgs.msg import Track, ControlCommand
import sensor_msgs
from sensor_msgs.msg import PointCloud2, PointField
import sensor_msgs.point_cloud2
from sensor_msgs import point_cloud2
import struct
from std_msgs.msg import Header
import math
import tty
import sys
import termios

orig_settings = termios.tcgetattr(sys.stdin)
tty.setcbreak(sys.stdin)

class ManualSim:
    def __init__(self):
        self.controllpublisher = rospy.Publisher('/fsds/control_command', ControlCommand, queue_size=10)
        

    def control(self):
        msg = ControlCommand()
        x=sys.stdin.read(1)[0]

        if x == "w":
            msg.throttle = 0.3
        
        if x == "s":
            msg.brake = 1
        
        if x == "a":
            msg.steering = -0.5
        elif x == "d":
            msg.steering = 0.5

        if x == "q":
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)


        self.controllpublisher.publish(msg)




if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)

    rospy.init_node('manual', log_level=rospy.DEBUG)
    man = ManualSim()

    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        man.control()
        rate.sleep()
