#!/usr/bin/env python
import time
import numpy as np
import rospy
import tf
import cv2
import os

import matplotlib
import matplotlib.pyplot as plt
import PIL
import torch

from models import *
from utils.utils import *
from utils.datasets import *


from std_msgs.msg import UInt8

from dv_common.msg import Cones

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError




class Detector():

    def __init__(self):
        self.configure_ros()

        # self.image_sub = rospy.Subscriber("/fsds/camera/cam2", Image, self.callback, queue_size=1)
        self.image_pub = rospy.Publisher("image_topic_2",Image)

        self.bridge = CvBridge()

        print(os.curdir)
        print(os.listdir(os.getcwd()))
        print(os.listdir(os.path.dirname(os.path.realpath(__file__))))
        base_path = os.path.dirname(os.path.realpath(__file__))
        self.tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

        # model init
        yolo_architecture= base_path + "/yolov3-tiny-custom.cfg"
        self.img_size = 416
        weights_path = base_path + "/yolov3_ckpt_10.pth"
        class_path = base_path + "/coco.names"

        self.nms_thresh = 0.3
        self.conf_thresh = 0.9

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model = Darknet(yolo_architecture, img_size=self.img_size).to(device)
        self.model.load_state_dict(torch.load(weights_path, map_location=device))
        classes = load_classes(class_path)  # Extracts class labels from file

        #print("self tensor: " + str(self.tensor))
        self.model.eval()
        # self.detections = np.array([[]])
        self.counter = 0
        self.H = np.array([[8.042281333713394023e-06, -1.961361274836211739e-04, 5.294045563920569686e-01],
                          [-6.056359897488317046e-04, -1.130852262006681874e-04, 4.595069236367048671e-01],
                          [2.904333431894579065e-05, 1.528968366879885753e-03, -7.131489648707345363e-01]])
        self.subscribe_to_topics()
        self.listener = tf.TransformListener()


    def configure_ros(self):
        if DEBUG:
            rospy.init_node('Detector', log_level=rospy.DEBUG)
        else:
            rospy.init_node('Detector', log_level=rospy.ERROR)

        self.publishers = {}
        self.publishers["cone"] = rospy.Publisher('cones', Cones, queue_size=1)

    def subscribe_to_topics(self):
        self.image_sub = rospy.Subscriber("/zed/zed_node/left/image_rect_color", Image, self.callback, queue_size=1)
        # self.image_sub = rospy.Subscriber("/zed_camera_node/left/image_rect_color", Image, self.callback, queue_size=1)
        #rospy.Subscriber('mission', UInt8, self.receive_mission)

    def callback(self, data):
        self.timestamp = rospy.Time.now()
        start_time = time.time()
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "rgb8")
            cv_image = cv2.flip(cv_image, 0)
            cv_image = cv2.flip(cv_image, 1)
        except CvBridgeError as e:
            print(e)

        (rows, cols, channels) = cv_image.shape

        img = transforms.ToTensor()(cv_image)
        img, _ = pad_to_square(img, 0)
        img = resize(img, self.img_size)
        img = img.unsqueeze(0)
        #print(img.shape)
        input_imgs = Variable(img.type(self.tensor))

        cv_image = np.ascontiguousarray(cv_image, dtype=np.uint8)
        # try:
        #    cv2.imwrite('/home/xavier/images/img_' + str(self.counter) + ".png", cv2.cvtColor(cv_image, cv2.COLOR_RGB2BGR))
        #    self.counter += 1
        #except:
        #    pass

        # Get detections
        with torch.no_grad():
            detections = self.model(input_imgs)
            detections = non_max_suppression(detections, self.conf_thresh, self.nms_thresh)

        # self.detections = np.array([[]])
        if detections != [None]:
            # Rescale boxes to original image
            detections = rescale_boxes(detections[0], self.img_size, (cv_image.shape[0], cv_image.shape[1]))
            #self.image2car(np.vstack(((detections[:, 0] + detections[:, 2])/2, detections[:, 3])).T)
            #self.cones_cols = detections[:, 6]
            #self.publish()
            cones_x = []
            cones_y = []
            cones_col = []
            #print(detections)
            for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
                colors = [(255, 0, 0), (0, 255, 255), (0, 165, 255), (255, 255, 255)]
                color_bgr = colors[int(cls_pred)]

                # cone mid point
                #print(int(x1), int(x2))
                cone_x, cone_y = (int((x1 + x2) / 2), int(y2))
                cones_x.append(cone_x)
                cones_y.append(cone_y)
                cones_col.append(cls_pred)

                # if len(self.detections) == 0:
                #     self.detections = np.array([[cone_x, cone_y, int(cls_pred)]])
                # else:
                #     self.detections = np.vstack((self.detections, [cone_x, cone_y, int(cls_pred)]))


                # print("\t+ Label: %s, Conf: %.5f" % (classes[int(cls_pred)], cls_conf.item()))
                cv_image = np.ascontiguousarray(cv_image, dtype=np.uint8)

                cv_image = cv2.rectangle(cv_image, (x1, y1), (x2, y2), color_bgr, 3)
                # cv2.putText(cv_image, "(%.2f, %.2f), dist: %.2f," % (x_pos, y_pos, dist), (cone_x, y1), font, 0.3, (0, 0, 255), 1, cv2.LINE_AA)
                # cv2.putText(color_image, "conf: %.2f, dist: %.2f," % (cls_conf, dist), (cone_x, y1), font, 0.7, (0, 0, 255), 1, cv2.LINE_AA)
                # cv2.putText(color_image, "conf: %.2f, dist: %.2f," % (cls_conf, dist), (cone_x, y1), font, 0.7, (0, 0, 255), 1, cv2.LINE_AA)

                # x_point = int(field_w * (x_pos / field_meters[0]))
                # y_point = int(field_h * (y_pos / field_meters[1]))

                # frame = cv2.circle(color_image, (car_x - x_point, car_y - y_point), 4, color_bgr, -1)
            self.image2car_coor(np.vstack((cones_x, cones_y, np.ones(len(cones_y)))))
            self.cones_cols = np.array(cones_col)
            self.publish()
            


        #print("rows: %d, cols: %d" % (rows, cols))
        #print("Seconds: " + str(time.time()-start_time))

        #try:
        #    cv2.imwrite('/home/xavier/images/img_' + str(self.counter) + ".png", cv2.cvtColor(cv_image, cv2.COLOR_RGB2BGR))
        #    self.counter += 1
        #except:
        #    pass

        try:
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "rgb8"))
        except CvBridgeError as e:
            print(e)





        # img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
        # img = PIL.Image.fromarray(img)
        # img.show()





    def image2car_coor(self, cones_image):
        #print(cones_image)
        self.cones_car = np.matmul(self.H, cones_image)
        self.cones_car = self.cones_car/self.cones_car[2, :]
        self.cones_car = self.cones_car[:2, :].T
        


    def publish(self, ):
        msg = Cones()
        msg.header.stamp = self.timestamp
        msg.yellow_x = self.cones_car[self.cones_cols == 1, 0]
        msg.yellow_y = self.cones_car[self.cones_cols == 1, 1]
        msg.blue_x = self.cones_car[self.cones_cols == 0, 0]
        msg.blue_y = self.cones_car[self.cones_cols == 0, 1]
        self.publishers["cone"].publish(msg)

    def receive_as(self, state):
        self.state = AS(int(state.data))

    def receive_mission(self, mission):
        self.mission = Mission(int(mission.data))


if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)


    detector = Detector()
    print("starting subscription")
    # detector.subscribe_to_topics()

    # plt.imshow(detector.cv_image)
    # plt.plot([1,2,3,4])
    # plt.show()
    rospy.spin()
    #rate = rospy.Rate(1)
    #while not rospy.is_shutdown():
    #    detector.publish()
    #    rate.sleep()
