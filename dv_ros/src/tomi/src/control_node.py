#!/usr/bin/env python
import time
import numpy as np
import rospy

from tomi.msg import ControlCommand
from dv_common.BaseNode import BaseNode


class ControlNode(BaseNode):
    def __init__(self):
        super(ControlNode, self).__init__()

    def configure_ros(self):
        if DEBUG:
            rospy.init_node('ControlNode', log_level=rospy.DEBUG)
        else:
            rospy.init_node('ControlNode', log_level=rospy.ERROR)

        self.command_publisher = rospy.Publisher('/xavier/control_command', ControlCommand, queue_size=1)

    def subscribe_to_topics(self):
        pass
                                    
    def publish(self):
        pass

    def setThrottle(self, throttle):
        cc = ControlCommand(command=0, value=throttle)
        self.command_publisher.publish(cc)

    def setSteer(self, steering_angle):
        cc = ControlCommand(command=1, value=steering_angle)
        self.command_publisher.publish(cc)

if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)

    control_node = ControlNode()

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        control_node.setSteer(1)
        rate.sleep()
        #control_node.setSteer(1)
        #rate.sleep()