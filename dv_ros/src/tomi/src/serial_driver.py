#!/usr/bin/env python
import time
import numpy as np
import rospy
import serial
import logging
import struct
import csv

from tomi.msg import ControlCommand
from dv_common.BaseNode import BaseNode

import raspberry_data

# Constants
MESSAGE_SIZE_BYTES = raspberry_data.DATA_SIZE  # Message data size in bytes (w/o start bytes)
MESSAGE_START_FRAME = raspberry_data.DATA_START_FRAME

# Data types definition
DATA_STRUCT = raspberry_data.DATA_STRUCT  # Data types list compatible with struct.unpack
DATA_NAMES = raspberry_data.DATA_NAMES


class SerialDriver(BaseNode):
    def __init__(self, serial_port):
        super(SerialDriver, self).__init__()
        self.serial_ifc = serial_port
        self.steering = 0

    def configure_ros(self):
        rospy.init_node('SerialDriver', log_level=rospy.DEBUG, disable_signals=True)
        
        #self.cone_publisher = rospy.Publisher('path', Vector3Array, queue_size=1)

    def subscribe_to_topics(self):
        #rospy.Subscriber('control_command', ControlCommand, self.receive_command)
        pass

    def publish(self):
        pass

    def receive_command(self, control_command):
        if np.isnan(control_command.value):
            return
        if control_command.command == 0:
            value = 400 * control_command.value + 1520
            #            print("throttle:", value)
        else:
            if control_command.value - self.steering > 0.1:
                self.steering += 0.2
                value = -400 * self.steering + 1420
            elif self.steering - control_command.value > 0.1:
                self.steering -= 0.2
                value = -400 * self.steering + 1420
            else:
                value = -400 * control_command.value + 1420
                #            print("steer:", value)
        print(control_command.command, value)
        #self.serial_ifc.write((struct.pack('i', control_command.command))) 
        self.serial_ifc.write(b'A'.encode('utf-8'))
        self.serial_ifc.write(b'S'.encode('utf-8'))
        self.serial_ifc.write(b'D'.encode('utf-8'))
        self.serial_ifc.write(b'F'.encode('utf-8'))
        self.serial_ifc.write((struct.pack('>i', value)))
        self.serial_ifc.write(b'\n'.encode('utf-8'))

    def receive_data(self):
        if self.serial_ifc.isOpen():
            no_data_counter = 0
            if not self.serial_ifc.in_waiting:
                no_data_counter += 1
                if no_data_counter > 10e5:
                    logging.info("No data recieved from serial. Waiting...")
                    no_data_counter = 0
                return

            start_bytes = []
            reading = b''
            while True:
                current_byte = self.serial_ifc.read(1)
                if current_byte != b'\xaa':
                    start_bytes = []
                    continue
                start_bytes.append(current_byte)
                if start_bytes == MESSAGE_START_FRAME:
                    break

            timestamp = time.time()
            reading = self.serial_ifc.read(MESSAGE_SIZE_BYTES)
        else:
            logging.warning("Serial port disconnected!")
            rospy.signal_shutdown("Serial port disconnected!")

        if len(reading) < MESSAGE_SIZE_BYTES:
            logging.info("No or corrupted data recieved. "
                        "Data length is {}".format(len(reading)))
            return

        temp_data = self.process_message(reading, timestamp)

    def process_message(self, msg, timestamp):
        """ Decodes message and saves values to file.
        """
        #logging.debug("Data recieved:\t {}".format(''.join(r'\x'+hex(letter)[2:] for letter in msg)))    
        fmt = DATA_STRUCT
        data = struct.unpack(fmt, msg)
        #logging.debug("Data unpacked:\t {}".format(data))
        # with open('data/meas_data.txt', 'a') as data_file:
        with open('/home/xavier/data.txt', 'a') as data_file:     # external SSD
            writer = csv.writer(data_file, delimiter=',')
            templist = list(data)
            templist.insert(0, timestamp)
            writer.writerow(templist)
        return data

    def asdf(self):
        self.serial_ifc.write(b'A'.encode('utf-8'))
        print(self.serial_ifc.write((struct.pack('>i', 1000))))
        self.serial_ifc.write(b'\n'.encode('utf-8'))
        #print('baf')



if __name__ == '__main__':
    # DEBUG = rospy.get_param("debug", False)

    try:
        serial_ifc = serial.Serial(
            port="/dev/ttyTHS0",
            baudrate=115200,
            parity=serial.PARITY_EVEN
            #stopbits=serial.STOPBITS_TWO,
            #timeout=0.5
            )
        rospy.sleep(0.1)
    except:
        rospy.signal_shutdown("Cannot connect to serial port!")

    # Write first row of data file
    #with open('/home/xavier/data.txt', 'w') as data_file:
        #data_file.write("Time,{}\n".format(','.join(DATA_NAMES))) # Python < 3.7 does not preserve order of the list
        #data_file.write("Time,ax1,ay1,az1,ax2,ay2,az2,gx1,gy1,gz1,gx2,gy2,gz2,magx1,magy1,magz1,magx2,magy2,magz2,lon,lat,hAcc,vAcc, \
        #velN,velE,velD,gSpeed,heading,sAcc,headAcc,PWM_steer_ref,PWM_throttle_ref,RPM-quick,RPM-precise,PWM_steer,PWM_throttle,abs_k, \
        #throttle_center,steer_center,throttle_boundary,steer_boundary,full_brake,fail_time,abs_ramp_time,abs_acc_threshold,abs_rpm_threshold,FLAG\n")

    serial_driver = SerialDriver(serial_ifc)

    #rate = rospy.Rate(1)
    #while not rospy.is_shutdown():
        #serial_driver.receive_data()
        #pass
    
    rate = rospy.Rate(100)
    while True:
        cmd=ControlCommand(0,0.5)
        serial_driver.receive_command(cmd)
        rate.sleep()

    #rospy.spin()