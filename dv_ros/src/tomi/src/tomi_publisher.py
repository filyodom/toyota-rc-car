#!/usr/bin/env python
import numpy as np
import rospy
import utm
import tf

from dv_common.BaseNode import BaseNode
from std_msgs.msg import Float32
from geometry_msgs.msg import TwistStamped, Vector3Stamped, QuaternionStamped

class TomiPublisher(BaseNode):
    def __init__(self):
        super(TomiPublisher, self).__init__()
        self.x_origin = np.nan
        self.y_origin = np.nan
        self.z_origin = np.nan
        self.theta_origin = np.nan
        self.tf_broadcaster = tf.TransformBroadcaster()
        self.timestamp = rospy.Time.now()

    def configure_ros(self):
        if DEBUG:
            rospy.init_node('TomiPublisher', log_level=rospy.DEBUG)
        else:
            rospy.init_node('TomiPublisher', log_level=rospy.ERROR)

        self.speed_publisher = rospy.Publisher('/xavier/speed', Float32, queue_size=1)

    def subscribe_to_topics(self):
        rospy.Subscriber('/xsens/filter/twist', TwistStamped, self.publish_speed, queue_size=1)
        rospy.Subscriber('/xsens/filter/positionlla', Vector3Stamped, self.receive_gps, queue_size=1)
        rospy.Subscriber('/xsens/filter/quaternion', QuaternionStamped, self.receive_quat, queue_size=1)
                                    
    def publish(self):
        pass
        
    def publish_speed(self, msg):
        vec = ([msg.twist.linear.x, msg.twist.linear.y, msg.twist.linear.z])

        speed = Float32(data=np.linalg.norm(vec))
#	speed = Float32(data=msg.twist.linear.x)
        self.speed_publisher.publish(speed)

    def receive_gps(self, msg):
        lat = msg.vector.x
        lon = msg.vector.y
        alt = msg.vector.z
        if self.timestamp <= msg.header.stamp:
            self.timestamp = msg.header.stamp
        else:
            return

        easting, northing, zone_number, zone_letter = utm.from_latlon(lat, lon)

        if np.isnan(self.x_origin) or np.isnan(self.y_origin) or np.isnan(self.z_origin):
            self.x_origin = easting
            self.y_origin = northing
            self.z_origin = alt

        if np.isnan(self.theta_origin):
            return

        self.x = easting-self.x_origin
        self.y = northing-self.y_origin
        #self.z = alt-self.z_origin
        self.tf_broadcaster.sendTransform((self.x, self.y, 0),
                                          tf.transformations.quaternion_from_euler(0, 0, self.theta),
                                          self.timestamp, 'base_footprint', 'map')

    def receive_quat(self, msg):
        _, _, theta = tf.transformations.euler_from_quaternion([msg.quaternion.x, msg.quaternion.y,
                                                               msg.quaternion.z, msg.quaternion.w])
        if self.timestamp <= msg.header.stamp:
            self.timestamp = msg.header.stamp
        else:
            return

        if np.isnan(self.theta_origin):
            self.theta_origin = theta

        if np.isnan(self.x_origin) or np.isnan(self.y_origin) or np.isnan(self.z_origin):
            return

        self.theta = theta - self.theta_origin
        self.tf_broadcaster.sendTransform((self.x, self.y, 0),
                                          tf.transformations.quaternion_from_euler(0, 0, self.theta),
                                          self.timestamp, 'base_footprint', 'map')



if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)

    tomi_publisher = TomiPublisher()

    rospy.spin()
