#!/usr/bin/env python
from __future__ import print_function
import time
from threading import Lock
import rospy
from std_msgs.msg import String, Bool, UInt8
from dv_common.car_info import CarInfo
from dv_common.constants import AS, Mission as EMission, SubSystem, GO_SIGNAL_DELAY, MissionStatus, EBSSupervisorStatus
from dv_common.BaseNode import BaseNode
from asm.msg import CarState, Mission, EBSSupervisor
from asm.asm_class import ASM


class ASMRos(BaseNode):
    """
    Implementation of the autonomous state machine
    """

    def __init__(self):
        self.ASM = ASM()
        super(ASMRos, self).__init__()

    # region ROS
    def configure_ros(self):
        if DEBUG:
            rospy.init_node('ASM', log_level=rospy.DEBUG)
        else:
            rospy.init_node('ASM', log_level=rospy.ERROR)

        self.publishers["as"] = rospy.Publisher('as', UInt8, queue_size=1)
        self.publishers["mission"] = rospy.Publisher('mission', UInt8, queue_size=1)

    def subscribe_to_topics(self):
        rospy.Subscriber('car_state', CarState, self.ASM.check_as_state)  # for control with CAN input
        # individual CAN input messages
        rospy.Subscriber('can/res_go', Bool, self.ASM.receive_res_go_signal)
        rospy.Subscriber('can/res_stop', Bool, self.ASM.receive_res_stop_signal)
        rospy.Subscriber('can/ts_on', Bool, self.ASM.receive_ts_on_signal)
        rospy.Subscriber('can/supervisor', EBSSupervisor, self.ASM.receive_supervisor_status)
        rospy.Subscriber('can/mission', Mission, self.ASM.receive_mission_selection)

    def publish(self):
        self.publishers["as"].publish(self.ASM.state)
        self.publishers["mission"].publish(self.ASM.mission)
    # endregion


if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)

    asm = ASMRos()

    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        asm.publish()
        rate.sleep()

