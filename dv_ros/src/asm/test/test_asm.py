#!/usr/bin/env python

import unittest
from dv_common.constants import Mission, AS, EBSSupervisorStatus
from asm.asm_class import ASM
from asm.msg import Mission as MissionMsg, EBSSupervisor
import time


class ASMTest(unittest.TestCase):
    def test_autonomous_mission(self):
        missions = [Mission.ACCELERATION, Mission.AUTOCROSS, Mission.EBS_TEST, Mission.INSPECTION,
                    Mission.SKIDPAD, Mission.TRACKDRIVE]
        for m in missions:
            state_machine = ASM()

            # test transition to AS_READY
            ebss = EBSSupervisor()
            ebss.status = EBSSupervisorStatus.OPEN

            mission = MissionMsg()
            mission.mission = Mission.ACCELERATION

            self.assertEqual(state_machine.state, AS.AS_OFF)
            state_machine.receive_supervisor_status(ebss)
            self.assertEqual(state_machine.state, AS.AS_OFF)
            state_machine.receive_mission_selection(mission)
            ebss.status = EBSSupervisorStatus.CLOSED
            state_machine.receive_supervisor_status(ebss)
            self.assertEqual(state_machine.state, AS.AS_OFF)
            state_machine.receive_ts_on_signal(None)
            self.assertEqual(state_machine.state, AS.AS_READY)

            # check transition to AS_EMERGENCY
            ebss.status = EBSSupervisorStatus.OPEN
            state_machine.receive_supervisor_status(ebss)
            self.assertEqual(state_machine.state, AS.AS_EMERGENCY)

            # reset ASM
            ebss.status = EBSSupervisorStatus.CLOSED
            state_machine.state = AS.AS_READY
            state_machine.receive_supervisor_status(ebss)

            # check transition to AS_DRIVING
            state_machine.receive_res_go_signal(None)
            time.sleep(5)
            state_machine.receive_ts_on_signal(None)
            self.assertEqual(state_machine.state, AS.AS_DRIVING)

    def test_manual_driving(self):
        state_machine = ASM()

        # test transition to MANUAL_DRIVING
        ebss = EBSSupervisor()
        ebss.status = EBSSupervisorStatus.OPEN

        mission = MissionMsg()
        mission.mission = Mission.MANUAL_DRIVING

        self.assertEqual(state_machine.state, AS.AS_OFF)
        state_machine.receive_mission_selection(mission)

        state_machine.receive_ts_on_signal(None)
        self.assertEqual(state_machine.state, AS.MANUAL_DRIVING)


if __name__ == '__main__':
    unittest.main()

