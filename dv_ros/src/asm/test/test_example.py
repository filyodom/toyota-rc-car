#!/usr/bin/env python

import unittest
from asm.example import hello
# can import msgs

## A sample python unit test
class TestBareBones(unittest.TestCase):

    def test_one_equals_one(self):
        self.assertEquals(hello(), "hello")


if __name__ == '__main__':
    unittest.main()

# run as: $ NOSE_NOCAPTURE=1 catkin_make run_tests
# or: $ python -m unittest discover test/ 

# view test results: $ catkin_test_results build/test_results
