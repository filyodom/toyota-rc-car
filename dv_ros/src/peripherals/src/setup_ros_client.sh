#!/bin/sh

network_interface="$1";

if [ -z "$network_interface" ]
then
     echo "Must set network interface as parameter";
fi

echo "Setting up $network_interface";
echo $1

$(ip addr flush dev $1)
$(addr show dev $1)
$(ip addr add 10.5.5.1/24 dev $1)
$(ip link set "$netwrok_interface" up)
$(ip addr show dev "$netwrok_interface")

$(dnsmasq -C /dev/null -kd -F 10.5.5.50,10.5.5.100 -i "$netwrok_interface" --bind-dynamic;)


#sudo ip addr flish dev

