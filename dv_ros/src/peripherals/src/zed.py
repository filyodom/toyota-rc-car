#!/usr/bin/env python
import rospy
from dv_common.BaseNode import BaseNode
from sensor_msgs.msg import Image
import cv2 as cv
import cv_bridge as cvb
import numpy as np


class Zed(BaseNode):
    def __init__(self):
        super(Zed, self).__init__()
        self.image_left = None
        self.image_right = None
        self.bridge = cvb.CvBridge()

    def configure_ros(self):
        if DEBUG:
            rospy.init_node("Zed", log_level=rospy.DEBUG)
        else:
            rospy.init_node("Zed", log_level=rospy.ERROR)

    def subscribe_to_topics(self):
        rospy.Subscriber("/zed/zed_node/left/image_rect_color", Image, self.receive_image_left)
        rospy.Subscriber("/zed/zed_node/right/image_rect_color", Image, self.receive_image_right)

    def publish(self):
        pass

    def receive_image_left(self, img):
        # type: (Image) -> None
        cv_img = self.bridge.imgmsg_to_cv2(img, "bgr8")
        self.image_left = cv_img

    def receive_image_right(self, img):
        # type: (Image) -> None
        cv_img = self.bridge.imgmsg_to_cv2(img, "bgr8")
        self.image_right = cv_img

    def show_img(self):
        if self.image_left is not None and self.image_right is not None:
            cv.imshow("ZED", np.hstack((self.image_left, self.image_right)))
            cv.waitKey(1)


if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)

    zedClass = Zed()

    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        zedClass.show_img()
        rate.sleep()
