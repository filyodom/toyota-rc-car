#!/usr/bin/env python

import time
import cv_bridge as cvb
import cv2 as cv
import numpy as np

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image


class Tester(object):
    def __init__(self):
        self.WINDOW_NAME = rospy.get_namespace()  # "Realsense debug window"
        self.bridge = cvb.CvBridge()
        self.depth_img = None
        self.color_img = None

        rospy.Subscriber("/camera/color/image_raw", Image, self.handle_image)
        rospy.Subscriber("/camera/depth/image_rect_raw", Image, self.handle_depth_image)

    def handle_image(self, image):
        cv_img = self.bridge.imgmsg_to_cv2(image, "bgr8")
        self.color_img = cv_img

    def handle_depth_image(self, image):
        cv_img = self.bridge.imgmsg_to_cv2(image, "bgr8")
        self.depth_img = cv_img

    def show_debug_img(self):
        if self.depth_img is not None and self.color_img is not None:
            cv.imshow(self.WINDOW_NAME, np.hstack((self.color_img, self.depth_img)))
            cv.waitKey(1)


if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)
    if DEBUG:
        rospy.init_node('RealsenseTest', log_level=rospy.DEBUG)
    else:
        rospy.init_node('RealsenseTest', log_level=rospy.ERROR)

    tester = Tester()
    x = False
    while x:
        continue
    rate = rospy.Rate(5)
    while not rospy.is_shutdown():
        tester.show_debug_img()
        rate.sleep()
