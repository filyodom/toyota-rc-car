#!/usr/bin/env python
from __future__ import print_function

from enum import IntEnum
from Tkinter import *
import ttk

import rospy
from std_msgs.msg import Header

from dv_common.constants import EBSSupervisorStatus, Mission, CANMessageType, AS, MissionStatus
from dv_common.BaseNode import BaseNode
from can_dummy_input.msg import canMessage
from PIL import Image, ImageTk

DEBUG = True
PATH = ""


# noinspection PyTypeChecker
class CanGui(BaseNode):

    def __init__(self):
        # TODO: Add hooks for data
        super(CanGui, self).__init__()
        self.root = None
        self.in_vars = dict()
        self.motor_moments = dict()
        self.steering_angle = None
        self.breaking = None
        self.asm_status = None
        self.mission_status = None
        self.mission_selected = None

        self.root = Tk()
        self.buttons = dict()
        self.combo_boxes = dict()
        self.img = Image.open(PATH + "img/formula.png")
        self.image_formula = ImageTk.PhotoImage(image=self.img)
        self.imgLabel = None

        # Crate vars
        self.create_vars()

        # Last step build th gui
        self.build_gui()

        self.imgLabel["image"] = self.image_formula

    def subscribe_to_topics(self):
        rospy.Subscriber("/can_out", canMessage, self.receive_can_msg)

    def publish(self):
        pass

    def configure_ros(self):
        rospy.init_node('CAN_Dummy_Input_GUI', log_level=rospy.ERROR)
        self.publishers["can_in"] = rospy.Publisher("/can_in", canMessage, queue_size=1)

    def receive_can_msg(self, msg):
        actions = {
            CANMessageType.SB_BREAKING: self.receive_sb,
            CANMessageType.SA_ANGLE: self.receive_sa,
            CANMessageType.ASM_STATUS: self.receive_asm,
            CANMessageType.MOTOR_TORQUE: self.receive_mq,
        }
        actions[msg.type](msg)

    def receive_sb(self, sb_msg):
        self.breaking.set("True " if sb_msg.data == 1 else "False")

    def receive_sa(self, sa_msg):
        self.steering_angle.set(sa_msg.floatData)

    def receive_asm(self, asm_msg):
        self.asm_status.set(AS(asm_msg.data).name)

    def receive_mq(self, mt_msg):
        # type: (canMessage) -> None
        val = round(mt_msg.floatData, 4)
        if mt_msg.data == 0:
            self.motor_moments["LF"].set(val)
        elif mt_msg.data == 1:
            self.motor_moments["RF"].set(val)
        elif mt_msg.data == 2:
            self.motor_moments["LB"].set(val)
        elif mt_msg.data == 3:
            self.motor_moments["RB"].set(val)

    # region VARS

    def create_vars(self):
        self.motor_moments = {
            "LF": StringVar(),
            "RF": StringVar(),
            "LB": StringVar(),
            "RB": StringVar(),
        }
        [x.set("NaN") for x in self.motor_moments.values()]

        self.steering_angle = StringVar()
        self.steering_angle.set("NaN")

        self.breaking = StringVar()
        self.breaking.set(False)

        self.in_vars = {
            "mission": StringVar(),
            "ebss": StringVar(),
        }

        # Out vars
        self.asm_status = StringVar()
        self.mission_selected = StringVar()
        self.mission_status = StringVar()

        self.asm_status.set(AS(0).name)
        self.mission_selected.set(Mission.NOT_SELECTED.name)
        self.mission_status.set(MissionStatus.UNAVAILABLE.name)

    # endregion

    # region GUI
    def build_gui(self):
        self.root.title("CAN Dummy GUI")

        n = ttk.Notebook(self.root)
        f1 = ttk.Frame(n)  # first page, which would get widgets gridded into it
        f2 = ttk.Frame(n)  # second page
        n.add(f1, text='Overview')
        n.add(f2, text='Raw view')
        n.pack(expand=1, fill='both')

        self.build_input_frame(f1)

        # Separator
        ttk.Separator(f1, orient=VERTICAL).grid(column=2, row=0, rowspan=1, sticky=(N, S))

        self.build_output_frame(f1)

        for child in f1.winfo_children():
            child.grid_configure(padx=5, pady=5)

    def build_input_frame(self, root):
        in_frame = ttk.Frame(root, padding="3 3 12 12")
        in_frame.grid(column=0, row=0, sticky=(W, N))

        # Mission select region
        ttk.Label(in_frame, text="Mission:").grid(column=0, row=0, sticky=(W, E))
        self.combo_boxes["mission"] = ttk.Combobox(in_frame, textvariable=self.in_vars["mission"], state="readonly")
        self.combo_boxes["mission"].grid(column=1, row=0)
        self.combo_boxes["mission"]["values"] = ("ACCELERATION", "SKIDPAD", "AUTOCROSS", "TRACKDRIVE",
                                                 "EBS_TEST", "INSPECTION", "MANUAL_DRIVING")
        self.combo_boxes["mission"]["values"] = self.get_enum_names(Mission)
        self.combo_boxes["mission"].bind("<<ComboboxSelected>>", self.combo_change_mission)
        self.combo_boxes["mission"].current(0)
        ttk.Button(in_frame, text="Select", command=self.button_select_mission).grid(column=2, row=0, sticky=W)

        # EBSS select region
        ttk.Label(in_frame, text="EBSS:").grid(column=0, row=1, sticky=(W, E))
        self.combo_boxes["ebss"] = ttk.Combobox(in_frame, textvariable=self.in_vars["ebss"], state="readonly")
        self.combo_boxes["ebss"].grid(column=1, row=1)
        self.combo_boxes["ebss"]['values'] = self.get_enum_names(EBSSupervisorStatus)
        self.combo_boxes["ebss"].bind("<<ComboboxSelected>>", self.combo_change_ebss)
        self.combo_boxes["ebss"].current(0)
        ttk.Button(in_frame, text="Select", command=self.button_select_ebss).grid(column=2, row=1, sticky=W)

        # One time buttons
        ttk.Button(in_frame, text="TS On", command=self.button_ts_on).grid(column=0, row=3, sticky=(W, S))
        ttk.Button(in_frame, text="RES Go", command=self.button_res_go).grid(column=1, row=3, sticky=(E, S))
        ttk.Button(in_frame, text="RES Stop", command=self.button_res_stop).grid(column=2, row=3, sticky=(W, S))

        # State info
        ttk.Separator(in_frame, orient=HORIZONTAL).grid(column=0, row=4, columnspan=3, sticky=(W, E, N), pady=10)
        ttk.Label(in_frame, text="ASM status:").grid(column=0, row=5)
        ttk.Label(in_frame, textvariable=self.asm_status).grid(column=1, row=5)
        ttk.Label(in_frame, text="Mission selected:").grid(column=0, row=6)
        ttk.Label(in_frame, textvariable=self.mission_selected).grid(column=1, row=6)
        ttk.Label(in_frame, text="Mission status:").grid(column=0, row=7)
        ttk.Label(in_frame, textvariable=self.mission_status).grid(column=1, row=7)

    def build_output_frame(self, root):
        out_frame = ttk.Frame(root, padding="3 3 12 12")
        out_frame.grid(column=3, row=0, stick=(N, E, S))

        ttk.Label(out_frame, textvariable=self.motor_moments["LF"]).grid(row=1, column=0, sticky=(W, E))
        ttk.Label(out_frame, textvariable=self.motor_moments["RF"]).grid(row=1, column=3, sticky=(W, E))
        ttk.Label(out_frame, textvariable=self.motor_moments["LB"]).grid(row=2, column=0, sticky=(W, E))
        ttk.Label(out_frame, textvariable=self.motor_moments["RB"]).grid(row=2, column=3, sticky=(W, E))

        # Nice image of formula
        self.image_formula = ImageTk.PhotoImage(Image.open(PATH + "img/formula.png"))
        self.imgLabel = ttk.Label(out_frame, image=self.image_formula)
        self.imgLabel.image = self.image_formula
        self.imgLabel.grid(row=1, column=1, rowspan=2, columnspan=2, sticky=(N, W, S, E), padx=5, pady=5)
        out_frame.grid_columnconfigure(1, minsize=70)

        # New frame because of justifying
        number_frame = ttk.Frame(out_frame, padding="3 3 12 12")
        number_frame.grid(row=4, column=0, columnspan=4, sticky=(N, W, S, E))

        # Steering angle
        ttk.Label(number_frame, text="Steering angle (deg):").grid(row=0, column=0, columnspan=2, sticky=(N, W, S))
        ttk.Label(number_frame, textvariable=self.steering_angle).grid(row=0, column=2, columnspan=1, sticky=(N, W, S))

        # Breaking
        ttk.Label(number_frame, text="Breaking:").grid(row=1, column=0, columnspan=2, sticky=(N, W, S))
        ttk.Label(number_frame, textvariable=self.breaking).grid(row=1, column=2, columnspan=1, sticky=(N, W, S))

    # endregion

    def loop(self):
        self.root.mainloop()

    # region Callback functions
    def combo_change_mission(self, event):
        val = self.combo_boxes["mission"].current()
        if DEBUG:
	    print(self.combo_boxes["mission"]["values"][val])

    def combo_change_ebss(self, event):
        val = self.combo_boxes["ebss"].current()
	if DEBUG:
            print(self.combo_boxes["ebss"]["values"][val])

    def button_select_mission(self):
        msg = self.init_message()
        val = self.combo_boxes["mission"].current()
        # parse input from combo box
        msg.type = CANMessageType.MISSION_SELECT
        msg.data = val

        self.publishers["can_in"].publish(msg)

        if DEBUG:
            print("DEBUG: Mission select, value: {}".format(val))

    def button_select_ebss(self):
        msg = self.init_message()
        val = self.combo_boxes["ebss"].current()
        # parse input from combo box
        msg.type = CANMessageType.EBS_SUPERVISOR_STATUS

        msg.data = val
        self.publishers["can_in"].publish(msg)

        if DEBUG:
            print("DEBUG: EBSS select, value: {}".format(val))

    def button_res_go(self):
        msg = self.init_message()
        msg.type = CANMessageType.RES_GO
        msg.data = 255
        self.publishers["can_in"].publish(msg)
        print("RES GO sent")

    def button_res_stop(self):
        msg = self.init_message()
        msg.type = CANMessageType.RES_STOP
        msg.data = 255
        self.publishers["can_in"].publish(msg)
        if DEBUG:
            print("DEBUG: RES STOP sent")

    def button_ts_on(self):
        msg = self.init_message()
        msg.type = CANMessageType.TS_ON
        msg.data = 255
        self.publishers["can_in"].publish(msg)

        if DEBUG:
            print("DEBUG: TS ON sent")

    def update_motor_moments(self, motor_moments):
        """motor_moments is an dict similar to self.motor_moments"""
        [self.motor_moments[x].set(motor_moments[x]) for x in self.motor_moments.keys()]

    def update_breaking(self, value):
        """Value is an Boolean"""
        self.breaking.set(value)

    def update_steering(self, value):
        self.steering_angle.set(value)

    # endregion

    # region Helpers methods
    @staticmethod
    def get_enum_names(enum):
        ret = []
        for i in range(len(enum)):
            ret.append(enum(i).name)
        return ret

    @staticmethod
    def init_message():
        msg = canMessage()
        msg.header = Header()
        return msg
    # endregion


if __name__ == '__main__':
    PATH = rospy.get_param("path", "")
    DEBUG = rospy.get_param("DEBUG", False)

    gui = CanGui()
    gui.loop()
