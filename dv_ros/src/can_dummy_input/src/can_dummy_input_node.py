#!/usr/bin/env python
from __future__ import print_function
import rospy
from can_dummy_input.msg import canMessage
from std_msgs.msg import Header

from can_dummy_gui import CanGui
from dv_common.constants import *


def init_message():
    msg = canMessage()
    msg.header = Header()
    return msg


class CANTerminal(object):
    def __init__(self):
        self.message_keys = {
            "g": CANMessageType.RES_GO,
            "s": CANMessageType.RES_STOP,
            "t": CANMessageType.TS_ON,
            "m": CANMessageType.MISSION_SELECT,
            "e": CANMessageType.EBS_SUPERVISOR_STATUS
        }
        self.handlers = {
            "g": self.handle_res_go,
            "s": self.handle_res_stop,
            "t": self.handle_ts_on,
            "m": self.handle_mission_select,
            "e": self.handle_ebs_supervisor_status
        }
        self.publisher = rospy.Publisher("/can_in", canMessage)
        rospy.Subscriber("/can_out", canMessage, self.receive_can_msg)

    def help(self):
        print("No help :(")

    def parse_console_input(self):
        print("Type of message:")

        msg_type = str(raw_input())
        if len(msg_type) > 1 or msg_type not in self.message_keys.keys():
            print("Invalid type")
            return

        return self.handlers[msg_type]()

    def handle_res_go(self):
        msg = init_message()
        msg.type = CANMessageType.RES_GO
        msg.data = 255
        self.publisher.publish(msg)
        print("RES GO sent")

    def handle_res_stop(self):
        msg = init_message()
        msg.type = CANMessageType.RES_STOP
        msg.data = 255
        self.publisher.publish(msg)
        print("RES STOP sent")

    def handle_ts_on(self):
        msg = init_message()
        msg.type = CANMessageType.TS_ON
        msg.data = 255
        self.publisher.publish(msg)
        print("TS ON sent")

    def handle_mission_select(self):
        available_missions = {
            "ma": Mission.MANUAL_DRIVING,
            "ac": Mission.ACCELERATION,
            "sk": Mission.SKIDPAD,
            "eb": Mission.EBS_TEST,
            "au": Mission.AUTOCROSS,
            "in": Mission.INSPECTION,
        }
        print("Mission (first two letters):")
        mission = raw_input()
        if mission not in available_missions.keys():
            print("Invalid mission")
            return

        msg = init_message()
        msg.type = CANMessageType.MISSION_SELECT
        msg.data = available_missions[mission]
        self.publisher.publish(msg)
        print("Mission sent")

    def handle_ebs_supervisor_status(self):
        statuses = {
            "o": EBSSupervisorStatus.OPEN,
            "c": EBSSupervisorStatus.CLOSED,
            "off": EBSSupervisorStatus.OFF,
        }

        print("States (c - closed / o - open / off):")
        status_code = raw_input()
        if status_code not in ["c", "o", "off"]:
            print("Invalid status")

        msg = init_message()
        msg.type = CANMessageType.MISSION_SELECT
        msg.data = statuses[status_code]
        self.publisher.publish(msg)
        print("EBS supervisor status sent")

    def receive_can_msg(self):
        pass


if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)

    if DEBUG:
        rospy.init_node('CAN_Dummy_Input_Terminal', log_level=rospy.DEBUG)
    else:
        rospy.init_node('CAN_Dummy_Input_Terminal', log_level=rospy.ERROR)

    terminal = CANTerminal()

    gui = CanGui()

    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        terminal.parse_console_input()
        rate.sleep()
