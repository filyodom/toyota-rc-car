#!/usr/bin/env python
from enum import Enum
import json
import rospy
from std_msgs.msg import UInt8, Header, Bool
from dv_common.constants import Mission as MissionEnum, CANMessageType, EBSSupervisorStatus
from dv_common.BaseNode import BaseNode
from can_dummy_input.msg import canMessage
from asm import asm_class
from asm.msg import CarState, EBSSupervisor, Mission
from motion_planning.msg import Breaking, Steering, MotorTorques

DEBUG = False

def make_car_state():
    car_state = CarState()
    car_state.header = Header()
    car_state.mission = MissionEnum.NOT_SELECTED
    car_state.TS = False
    car_state.RES_Go = False
    car_state.RES_Stop = False

    return car_state


def make_mission(mission):
    msg = Mission()
    msg.header = Header()
    msg.mission = mission
    return msg


def make_ebs_supervisor(status):
    msg = EBSSupervisor()
    msg.header = Header()
    msg.status = status
    return msg


class CANDummy(object):
    def __init__(self):
        self.publishers = dict()
        self.subscribers = dict()
        self.init_publishers()
        self.init_subscribers()
        self.car_state = make_car_state()
        self.car_state_publisher = rospy.Publisher('car_state', CarState, queue_size=1)

    def init_subscribers(self):
        self.subscribers["can_in"] = rospy.Subscriber("/can_in", canMessage, self.receive_can_message)
        self.subscribers["as"] = rospy.Subscriber("as", UInt8, self.receive_as)
        self.subscribers["steering"] = rospy.Subscriber("steering", Steering, self.receive_steering)
        self.subscribers["breaking"] = rospy.Subscriber("breaking", Breaking, self.receive_breaking)
        self.subscribers["motor_torques"] = rospy.Subscriber("motor_torques", MotorTorques, self.receive_motor_torques)

    def init_publishers(self):
        self.publishers["can_out"] = rospy.Publisher("/can_out", canMessage)
        self.publishers[CANMessageType.RES_GO] = rospy.Publisher("can/res_go", Bool, queue_size=1)
        self.publishers[CANMessageType.RES_STOP] = rospy.Publisher("can/res_stop", Bool, queue_size=1)
        self.publishers[CANMessageType.TS_ON] = rospy.Publisher("can/ts_on", Bool, queue_size=1)
        self.publishers[CANMessageType.MISSION_SELECT] = rospy.Publisher("can/mission", Mission, queue_size=1)
        self.publishers[CANMessageType.EBS_SUPERVISOR_STATUS] = rospy.Publisher("can/supervisor"
                                                                                , EBSSupervisor, queue_size=1)

    def publish(self):
        self.car_state_publisher.publish(self.car_state)

    # region IN Messages
    def receive_can_message(self, msg):
        # type: (canMessage) -> None

        possible_inputs = {
            CANMessageType.RES_GO: self.receive_res_go,
            CANMessageType.RES_STOP: self.receive_res_stop,
            CANMessageType.TS_ON: self.receive_ts_on,
            CANMessageType.MISSION_SELECT: self.receive_mission_select,
            CANMessageType.EBS_SUPERVISOR_STATUS: self.receive_ebs_supervisor_status,
        }

        possible_inputs[msg.type](msg)

    def receive_res_go(self, _):
        # type: (canMessage) -> None
        self.car_state.RES_Go = True
        # Publish msg
        self.publishers[CANMessageType.RES_GO].publish(True)

    def receive_res_stop(self, _):
        # type: (canMessage) -> None
        self.car_state.RES_Stop = True
        # Publish msg
        self.publishers[CANMessageType.RES_STOP].publish(True)

    def receive_ts_on(self, _):
        # type: (canMessage) -> None
        self.car_state.TS = True
        # Publish msg
        self.publishers[CANMessageType.TS_ON].publish(True)

    def receive_mission_select(self, msg):
        # type: (canMessage) -> None
        self.car_state.mission = msg.data
        # Publish msg
        self.publishers[CANMessageType.MISSION_SELECT].publish(make_mission(msg.data))

    def receive_ebs_supervisor_status(self, msg):
        # type: (canMessage) -> None
        self.car_state.EBS_supervisor = msg.data
        # Publish msg
        self.publishers[CANMessageType.EBS_SUPERVISOR_STATUS].publish(make_ebs_supervisor(msg.data))
    # endregion

    def receive_as(self, status):
        # type: (UInt8) -> None
        msg = canMessage()
        msg.header = Header()
        msg.type = CANMessageType.ASM_STATUS
        msg.data = status.data

        self.publishers["can_out"].publish(msg)

    def receive_steering(self, steering_msg):
        # type: (Steering) -> None
        steering_angle = steering_msg.steeringAngle

        msg = canMessage()
        msg.header = Header()
        msg.type = CANMessageType.SA_ANGLE
        msg.floatData = steering_angle

        self.publishers["can_out"].publish(msg)

    def receive_breaking(self, breaking_msg):
        # type: (Breaking) -> None
        breaking = breaking_msg.breaking

        msg = canMessage()
        msg.header = Header()
        msg.type = CANMessageType.SB_BREAKING
        msg.data = breaking

        self.publishers["can_out"].publish(msg)

    def receive_motor_torques(self, motor_msg):
        # type: (MotorTorques) -> None
        msg = canMessage()
        msg.header = Header()
        msg.type = CANMessageType.MOTOR_TORQUE
        msg.data = 0    # TODO: Redo this...
        msg.floatData = motor_msg.LeftFront

        self.publishers["can_out"].publish(msg)

        msg = canMessage()
        msg.header = Header()
        msg.type = CANMessageType.MOTOR_TORQUE
        msg.data = 1  # TODO: Redo this...
        msg.floatData = motor_msg.RightFront
        self.publishers["can_out"].publish(msg)

        msg = canMessage()
        msg.header = Header()
        msg.type = CANMessageType.MOTOR_TORQUE
        msg.data = 2  # TODO: Redo this...
        msg.floatData = motor_msg.LeftBack
        self.publishers["can_out"].publish(msg)

        msg = canMessage()
        msg.header = Header()
        msg.type = CANMessageType.MOTOR_TORQUE
        msg.data = 3  # TODO: Redo this...
        msg.floatData = motor_msg.RightBack
        self.publishers["can_out"].publish(msg)


if __name__ == '__main__':
    DEBUG = rospy.get_param("debug", False)
    if DEBUG:
        rospy.init_node('CAN_Dummy', log_level=rospy.DEBUG)
    else:
        rospy.init_node("CAN_Dummy", log_level=rospy.ERROR)
    can_dummy = CANDummy()

    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        can_dummy.publish()
        rate.sleep()
