#!/usr/bin/env python
from __future__ import print_function
import time
from threading import Lock
from dv_common.constants import AS, MissionStatus, EBSSupervisorStatus, SubSystem, Mission as EMission, GO_SIGNAL_DELAY
from dv_common.car_info import CarInfo


class ASM(object):
    """
    Implementation of the autonomous state machine
    """

    def __init__(self):
        # System critical signals
        self.state = AS.AS_OFF
        self.mission = EMission.NOT_SELECTED
        self.mission_status = MissionStatus.UNAVAILABLE
        self.go_signal_received = 0
        self.stop_signal_received = 0
        self.ts_status = SubSystem.OFF
        self.ebs_supervisor = EBSSupervisorStatus.OFF
        # Additional info about car, like speed, acceleration, etc.
        self.car_info = CarInfo()

        self.update_lock = Lock()

    # region Receive signals
    def receive_mission_status(self, status):
        # type: (MissionStatus) -> None
        self.update_lock.acquire()
        self.mission_status = MissionStatus[status.data]
        self.update_as_state()
        self.update_lock.release()

    def receive_res_go_signal(self, _):
        self.update_lock.acquire()
        self.go_signal_received = time.time()
        self.update_as_state()
        self.update_lock.release()

    def receive_res_stop_signal(self, _):
        self.update_lock.acquire()
        self.stop_signal_received = 1
        self.update_as_state()
        self.update_lock.release()

    def receive_ts_on_signal(self, _):
        self.update_lock.acquire()
        self.ts_status = SubSystem.ON
        self.update_as_state()
        self.update_lock.release()

    def receive_mission_selection(self, msg):
        # type: (Mission)-> None
        self.update_lock.acquire()
        if self.state == AS.AS_OFF:
            self.mission = EMission(msg.mission)  # Don't have to check AS state because, it won't change
        self.update_lock.release()

    def receive_supervisor_status(self, msg):
        # type: (EBSSupervisor) -> None
        self.update_lock.acquire()
        self.ebs_supervisor = EBSSupervisorStatus(msg.status)
        self.update_as_state()
        self.update_lock.release()

    def check_as_state(self, msg):
        pass  # TODO: implement

    # endregion

    # region Autonomous State Machine
    def update_as_state(self):
        handlers = {
            AS.AS_OFF: self.handle_as_off,
            AS.MANUAL_DRIVING: self.handle_manual_driving,
            AS.AS_READY: self.handle_as_ready,
            AS.AS_DRIVING: self.handle_as_driving,
            AS.AS_FINISHED: self.handle_as_finished,
            AS.AS_EMERGENCY: self.handle_as_emergency,
        }
        handlers[self.state]()

    def handle_as_off(self):
        if self.mission == EMission.MANUAL_DRIVING and self.ebs_supervisor == EBSSupervisorStatus.OFF and \
                self.state == AS.AS_OFF and self.ts_status == SubSystem.ON:

            self.state = AS.MANUAL_DRIVING

        elif self.mission != EMission.MANUAL_DRIVING and self.mission != EMission.NOT_SELECTED and \
                self.ebs_supervisor == EBSSupervisorStatus.CLOSED and self.ts_status == SubSystem.ON:
            self.state = AS.AS_READY
            self.go_signal_received = -1  # Be sure that you won't start

    def handle_manual_driving(self):
        pass  # There is no way in software back from AS_MANUAL_DRIVING

    def handle_as_ready(self):
        curr_time = time.time()
        if self.go_signal_received != -1 and (curr_time - self.go_signal_received) >= GO_SIGNAL_DELAY:
            self.state = AS.AS_DRIVING

        elif self.ebs_supervisor == EBSSupervisorStatus.OPEN or self.stop_signal_received == 1:
            self.state = AS.AS_EMERGENCY

    def handle_as_driving(self):
        if self.stop_signal_received == 1:
            self.state = AS.AS_EMERGENCY
        elif self.mission_status == MissionStatus.FINISHED and self.car_info.speed == 0:
            self.state = AS.AS_FINISHED
        elif self.ebs_supervisor == EBSSupervisorStatus.OPEN or self.stop_signal_received == 1:
            self.state = AS.AS_EMERGENCY

    def handle_as_finished(self):
        if self.ebs_supervisor == EBSSupervisorStatus.OPEN or self.stop_signal_received == 1:
            self.state = AS.AS_EMERGENCY

    def handle_as_emergency(self):
        pass  # final state

    # endregion
