// Auto-generated. Do not edit!

// (in-package asm.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class CarState {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.mission = null;
      this.TS = null;
      this.EBS_supervisor = null;
      this.RES_Stop = null;
      this.RES_Go = null;
      this.speed = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('mission')) {
        this.mission = initObj.mission
      }
      else {
        this.mission = 0;
      }
      if (initObj.hasOwnProperty('TS')) {
        this.TS = initObj.TS
      }
      else {
        this.TS = false;
      }
      if (initObj.hasOwnProperty('EBS_supervisor')) {
        this.EBS_supervisor = initObj.EBS_supervisor
      }
      else {
        this.EBS_supervisor = 0;
      }
      if (initObj.hasOwnProperty('RES_Stop')) {
        this.RES_Stop = initObj.RES_Stop
      }
      else {
        this.RES_Stop = false;
      }
      if (initObj.hasOwnProperty('RES_Go')) {
        this.RES_Go = initObj.RES_Go
      }
      else {
        this.RES_Go = false;
      }
      if (initObj.hasOwnProperty('speed')) {
        this.speed = initObj.speed
      }
      else {
        this.speed = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type CarState
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [mission]
    bufferOffset = _serializer.uint8(obj.mission, buffer, bufferOffset);
    // Serialize message field [TS]
    bufferOffset = _serializer.bool(obj.TS, buffer, bufferOffset);
    // Serialize message field [EBS_supervisor]
    bufferOffset = _serializer.uint8(obj.EBS_supervisor, buffer, bufferOffset);
    // Serialize message field [RES_Stop]
    bufferOffset = _serializer.bool(obj.RES_Stop, buffer, bufferOffset);
    // Serialize message field [RES_Go]
    bufferOffset = _serializer.bool(obj.RES_Go, buffer, bufferOffset);
    // Serialize message field [speed]
    bufferOffset = _serializer.float64(obj.speed, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type CarState
    let len;
    let data = new CarState(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [mission]
    data.mission = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [TS]
    data.TS = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [EBS_supervisor]
    data.EBS_supervisor = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [RES_Stop]
    data.RES_Stop = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [RES_Go]
    data.RES_Go = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [speed]
    data.speed = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 13;
  }

  static datatype() {
    // Returns string type for a message object
    return 'asm/CarState';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ed1d98ad4090e2c54dae8c25435bf0e9';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    uint8 mission
    bool TS
    uint8 EBS_supervisor
    bool RES_Stop
    bool RES_Go
    float64 speed
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new CarState(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.mission !== undefined) {
      resolved.mission = msg.mission;
    }
    else {
      resolved.mission = 0
    }

    if (msg.TS !== undefined) {
      resolved.TS = msg.TS;
    }
    else {
      resolved.TS = false
    }

    if (msg.EBS_supervisor !== undefined) {
      resolved.EBS_supervisor = msg.EBS_supervisor;
    }
    else {
      resolved.EBS_supervisor = 0
    }

    if (msg.RES_Stop !== undefined) {
      resolved.RES_Stop = msg.RES_Stop;
    }
    else {
      resolved.RES_Stop = false
    }

    if (msg.RES_Go !== undefined) {
      resolved.RES_Go = msg.RES_Go;
    }
    else {
      resolved.RES_Go = false
    }

    if (msg.speed !== undefined) {
      resolved.speed = msg.speed;
    }
    else {
      resolved.speed = 0.0
    }

    return resolved;
    }
};

module.exports = CarState;
