// Auto-generated. Do not edit!

// (in-package motion_planning.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class MotorTorques {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.LeftFront = null;
      this.RightFront = null;
      this.LeftBack = null;
      this.RightBack = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('LeftFront')) {
        this.LeftFront = initObj.LeftFront
      }
      else {
        this.LeftFront = 0.0;
      }
      if (initObj.hasOwnProperty('RightFront')) {
        this.RightFront = initObj.RightFront
      }
      else {
        this.RightFront = 0.0;
      }
      if (initObj.hasOwnProperty('LeftBack')) {
        this.LeftBack = initObj.LeftBack
      }
      else {
        this.LeftBack = 0.0;
      }
      if (initObj.hasOwnProperty('RightBack')) {
        this.RightBack = initObj.RightBack
      }
      else {
        this.RightBack = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MotorTorques
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [LeftFront]
    bufferOffset = _serializer.float64(obj.LeftFront, buffer, bufferOffset);
    // Serialize message field [RightFront]
    bufferOffset = _serializer.float64(obj.RightFront, buffer, bufferOffset);
    // Serialize message field [LeftBack]
    bufferOffset = _serializer.float64(obj.LeftBack, buffer, bufferOffset);
    // Serialize message field [RightBack]
    bufferOffset = _serializer.float64(obj.RightBack, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MotorTorques
    let len;
    let data = new MotorTorques(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [LeftFront]
    data.LeftFront = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [RightFront]
    data.RightFront = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [LeftBack]
    data.LeftBack = _deserializer.float64(buffer, bufferOffset);
    // Deserialize message field [RightBack]
    data.RightBack = _deserializer.float64(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 32;
  }

  static datatype() {
    // Returns string type for a message object
    return 'motion_planning/MotorTorques';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '2bab8d054ec4aa480ead66ab590540f5';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Header header
    float64 LeftFront
    float64 RightFront
    float64 LeftBack
    float64 RightBack
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MotorTorques(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.LeftFront !== undefined) {
      resolved.LeftFront = msg.LeftFront;
    }
    else {
      resolved.LeftFront = 0.0
    }

    if (msg.RightFront !== undefined) {
      resolved.RightFront = msg.RightFront;
    }
    else {
      resolved.RightFront = 0.0
    }

    if (msg.LeftBack !== undefined) {
      resolved.LeftBack = msg.LeftBack;
    }
    else {
      resolved.LeftBack = 0.0
    }

    if (msg.RightBack !== undefined) {
      resolved.RightBack = msg.RightBack;
    }
    else {
      resolved.RightBack = 0.0
    }

    return resolved;
    }
};

module.exports = MotorTorques;
