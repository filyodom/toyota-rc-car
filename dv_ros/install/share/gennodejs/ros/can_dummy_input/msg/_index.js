
"use strict";

let canMessage = require('./canMessage.js');

module.exports = {
  canMessage: canMessage,
};
