
(cl:in-package :asdf)

(defsystem "motion_planning-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Breaking" :depends-on ("_package_Breaking"))
    (:file "_package_Breaking" :depends-on ("_package"))
    (:file "MotorTorques" :depends-on ("_package_MotorTorques"))
    (:file "_package_MotorTorques" :depends-on ("_package"))
    (:file "Steering" :depends-on ("_package_Steering"))
    (:file "_package_Steering" :depends-on ("_package"))
  ))