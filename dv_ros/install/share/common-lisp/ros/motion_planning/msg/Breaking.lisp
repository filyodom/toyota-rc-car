; Auto-generated. Do not edit!


(cl:in-package motion_planning-msg)


;//! \htmlinclude Breaking.msg.html

(cl:defclass <Breaking> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (breaking
    :reader breaking
    :initarg :breaking
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass Breaking (<Breaking>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Breaking>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Breaking)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name motion_planning-msg:<Breaking> is deprecated: use motion_planning-msg:Breaking instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <Breaking>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader motion_planning-msg:header-val is deprecated.  Use motion_planning-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'breaking-val :lambda-list '(m))
(cl:defmethod breaking-val ((m <Breaking>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader motion_planning-msg:breaking-val is deprecated.  Use motion_planning-msg:breaking instead.")
  (breaking m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Breaking>) ostream)
  "Serializes a message object of type '<Breaking>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'breaking) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Breaking>) istream)
  "Deserializes a message object of type '<Breaking>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:slot-value msg 'breaking) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Breaking>)))
  "Returns string type for a message object of type '<Breaking>"
  "motion_planning/Breaking")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Breaking)))
  "Returns string type for a message object of type 'Breaking"
  "motion_planning/Breaking")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Breaking>)))
  "Returns md5sum for a message object of type '<Breaking>"
  "9dd6dc81938db977e9ed62ea1c2ad7b8")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Breaking)))
  "Returns md5sum for a message object of type 'Breaking"
  "9dd6dc81938db977e9ed62ea1c2ad7b8")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Breaking>)))
  "Returns full string definition for message of type '<Breaking>"
  (cl:format cl:nil "Header header~%bool breaking~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Breaking)))
  "Returns full string definition for message of type 'Breaking"
  (cl:format cl:nil "Header header~%bool breaking~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Breaking>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Breaking>))
  "Converts a ROS message object to a list"
  (cl:list 'Breaking
    (cl:cons ':header (header msg))
    (cl:cons ':breaking (breaking msg))
))
