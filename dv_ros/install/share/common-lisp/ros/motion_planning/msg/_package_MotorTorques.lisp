(cl:in-package motion_planning-msg)
(cl:export '(HEADER-VAL
          HEADER
          LEFTFRONT-VAL
          LEFTFRONT
          RIGHTFRONT-VAL
          RIGHTFRONT
          LEFTBACK-VAL
          LEFTBACK
          RIGHTBACK-VAL
          RIGHTBACK
))