
(cl:in-package :asdf)

(defsystem "can_dummy_input-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "canMessage" :depends-on ("_package_canMessage"))
    (:file "_package_canMessage" :depends-on ("_package"))
  ))