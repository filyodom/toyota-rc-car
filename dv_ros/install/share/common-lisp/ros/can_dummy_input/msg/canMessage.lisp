; Auto-generated. Do not edit!


(cl:in-package can_dummy_input-msg)


;//! \htmlinclude canMessage.msg.html

(cl:defclass <canMessage> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (type
    :reader type
    :initarg :type
    :type cl:fixnum
    :initform 0)
   (data
    :reader data
    :initarg :data
    :type cl:fixnum
    :initform 0)
   (floatData
    :reader floatData
    :initarg :floatData
    :type cl:float
    :initform 0.0))
)

(cl:defclass canMessage (<canMessage>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <canMessage>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'canMessage)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name can_dummy_input-msg:<canMessage> is deprecated: use can_dummy_input-msg:canMessage instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <canMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader can_dummy_input-msg:header-val is deprecated.  Use can_dummy_input-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'type-val :lambda-list '(m))
(cl:defmethod type-val ((m <canMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader can_dummy_input-msg:type-val is deprecated.  Use can_dummy_input-msg:type instead.")
  (type m))

(cl:ensure-generic-function 'data-val :lambda-list '(m))
(cl:defmethod data-val ((m <canMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader can_dummy_input-msg:data-val is deprecated.  Use can_dummy_input-msg:data instead.")
  (data m))

(cl:ensure-generic-function 'floatData-val :lambda-list '(m))
(cl:defmethod floatData-val ((m <canMessage>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader can_dummy_input-msg:floatData-val is deprecated.  Use can_dummy_input-msg:floatData instead.")
  (floatData m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <canMessage>) ostream)
  "Serializes a message object of type '<canMessage>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'type)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data)) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'floatData))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <canMessage>) istream)
  "Deserializes a message object of type '<canMessage>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'type)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'data)) (cl:read-byte istream))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'floatData) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<canMessage>)))
  "Returns string type for a message object of type '<canMessage>"
  "can_dummy_input/canMessage")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'canMessage)))
  "Returns string type for a message object of type 'canMessage"
  "can_dummy_input/canMessage")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<canMessage>)))
  "Returns md5sum for a message object of type '<canMessage>"
  "42dcc0e4bf73a018ff05078421a8384c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'canMessage)))
  "Returns md5sum for a message object of type 'canMessage"
  "42dcc0e4bf73a018ff05078421a8384c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<canMessage>)))
  "Returns full string definition for message of type '<canMessage>"
  (cl:format cl:nil "Header header~%uint8 type~%uint8 data~%float32 floatData~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'canMessage)))
  "Returns full string definition for message of type 'canMessage"
  (cl:format cl:nil "Header header~%uint8 type~%uint8 data~%float32 floatData~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <canMessage>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     1
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <canMessage>))
  "Converts a ROS message object to a list"
  (cl:list 'canMessage
    (cl:cons ':header (header msg))
    (cl:cons ':type (type msg))
    (cl:cons ':data (data msg))
    (cl:cons ':floatData (floatData msg))
))
