(cl:in-package asm-msg)
(cl:export '(HEADER-VAL
          HEADER
          MISSION-VAL
          MISSION
          TS-VAL
          TS
          EBS_SUPERVISOR-VAL
          EBS_SUPERVISOR
          RES_STOP-VAL
          RES_STOP
          RES_GO-VAL
          RES_GO
          SPEED-VAL
          SPEED
))