; Auto-generated. Do not edit!


(cl:in-package asm-msg)


;//! \htmlinclude EBSSupervisor.msg.html

(cl:defclass <EBSSupervisor> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (status
    :reader status
    :initarg :status
    :type cl:fixnum
    :initform 0))
)

(cl:defclass EBSSupervisor (<EBSSupervisor>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <EBSSupervisor>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'EBSSupervisor)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name asm-msg:<EBSSupervisor> is deprecated: use asm-msg:EBSSupervisor instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <EBSSupervisor>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader asm-msg:header-val is deprecated.  Use asm-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'status-val :lambda-list '(m))
(cl:defmethod status-val ((m <EBSSupervisor>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader asm-msg:status-val is deprecated.  Use asm-msg:status instead.")
  (status m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <EBSSupervisor>) ostream)
  "Serializes a message object of type '<EBSSupervisor>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <EBSSupervisor>) istream)
  "Deserializes a message object of type '<EBSSupervisor>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'status)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<EBSSupervisor>)))
  "Returns string type for a message object of type '<EBSSupervisor>"
  "asm/EBSSupervisor")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'EBSSupervisor)))
  "Returns string type for a message object of type 'EBSSupervisor"
  "asm/EBSSupervisor")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<EBSSupervisor>)))
  "Returns md5sum for a message object of type '<EBSSupervisor>"
  "ba980b25d9f16ce730407be078c41a8d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'EBSSupervisor)))
  "Returns md5sum for a message object of type 'EBSSupervisor"
  "ba980b25d9f16ce730407be078c41a8d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<EBSSupervisor>)))
  "Returns full string definition for message of type '<EBSSupervisor>"
  (cl:format cl:nil "Header header~%uint8 status #IntEnum from dv_common/constants.py~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'EBSSupervisor)))
  "Returns full string definition for message of type 'EBSSupervisor"
  (cl:format cl:nil "Header header~%uint8 status #IntEnum from dv_common/constants.py~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <EBSSupervisor>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <EBSSupervisor>))
  "Converts a ROS message object to a list"
  (cl:list 'EBSSupervisor
    (cl:cons ':header (header msg))
    (cl:cons ':status (status msg))
))
