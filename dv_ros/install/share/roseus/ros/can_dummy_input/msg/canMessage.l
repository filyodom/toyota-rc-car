;; Auto-generated. Do not edit!


(when (boundp 'can_dummy_input::canMessage)
  (if (not (find-package "CAN_DUMMY_INPUT"))
    (make-package "CAN_DUMMY_INPUT"))
  (shadow 'canMessage (find-package "CAN_DUMMY_INPUT")))
(unless (find-package "CAN_DUMMY_INPUT::CANMESSAGE")
  (make-package "CAN_DUMMY_INPUT::CANMESSAGE"))

(in-package "ROS")
;;//! \htmlinclude canMessage.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass can_dummy_input::canMessage
  :super ros::object
  :slots (_header _type _data _floatData ))

(defmethod can_dummy_input::canMessage
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:type __type) 0)
    ((:data __data) 0)
    ((:floatData __floatData) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _type (round __type))
   (setq _data (round __data))
   (setq _floatData (float __floatData))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:type
   (&optional __type)
   (if __type (setq _type __type)) _type)
  (:data
   (&optional __data)
   (if __data (setq _data __data)) _data)
  (:floatData
   (&optional __floatData)
   (if __floatData (setq _floatData __floatData)) _floatData)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8 _type
    1
    ;; uint8 _data
    1
    ;; float32 _floatData
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8 _type
       (write-byte _type s)
     ;; uint8 _data
       (write-byte _data s)
     ;; float32 _floatData
       (sys::poke _floatData (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8 _type
     (setq _type (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _data
     (setq _data (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; float32 _floatData
     (setq _floatData (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get can_dummy_input::canMessage :md5sum-) "42dcc0e4bf73a018ff05078421a8384c")
(setf (get can_dummy_input::canMessage :datatype-) "can_dummy_input/canMessage")
(setf (get can_dummy_input::canMessage :definition-)
      "Header header
uint8 type
uint8 data
float32 floatData

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :can_dummy_input/canMessage "42dcc0e4bf73a018ff05078421a8384c")


