;; Auto-generated. Do not edit!


(when (boundp 'asm::Mission)
  (if (not (find-package "ASM"))
    (make-package "ASM"))
  (shadow 'Mission (find-package "ASM")))
(unless (find-package "ASM::MISSION")
  (make-package "ASM::MISSION"))

(in-package "ROS")
;;//! \htmlinclude Mission.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass asm::Mission
  :super ros::object
  :slots (_header _mission ))

(defmethod asm::Mission
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:mission __mission) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _mission (round __mission))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:mission
   (&optional __mission)
   (if __mission (setq _mission __mission)) _mission)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8 _mission
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8 _mission
       (write-byte _mission s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8 _mission
     (setq _mission (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get asm::Mission :md5sum-) "55a54e954dcb4d9978176e5d2c076ab0")
(setf (get asm::Mission :datatype-) "asm/Mission")
(setf (get asm::Mission :definition-)
      "Header header
uint8 mission #IntEnum from dv_common/constants.py
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :asm/Mission "55a54e954dcb4d9978176e5d2c076ab0")


