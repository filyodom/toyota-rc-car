;; Auto-generated. Do not edit!


(when (boundp 'asm::EBSSupervisor)
  (if (not (find-package "ASM"))
    (make-package "ASM"))
  (shadow 'EBSSupervisor (find-package "ASM")))
(unless (find-package "ASM::EBSSUPERVISOR")
  (make-package "ASM::EBSSUPERVISOR"))

(in-package "ROS")
;;//! \htmlinclude EBSSupervisor.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass asm::EBSSupervisor
  :super ros::object
  :slots (_header _status ))

(defmethod asm::EBSSupervisor
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:status __status) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _status (round __status))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8 _status
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8 _status
       (write-byte _status s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8 _status
     (setq _status (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get asm::EBSSupervisor :md5sum-) "ba980b25d9f16ce730407be078c41a8d")
(setf (get asm::EBSSupervisor :datatype-) "asm/EBSSupervisor")
(setf (get asm::EBSSupervisor :definition-)
      "Header header
uint8 status #IntEnum from dv_common/constants.py
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :asm/EBSSupervisor "ba980b25d9f16ce730407be078c41a8d")


