;; Auto-generated. Do not edit!


(when (boundp 'asm::CarState)
  (if (not (find-package "ASM"))
    (make-package "ASM"))
  (shadow 'CarState (find-package "ASM")))
(unless (find-package "ASM::CARSTATE")
  (make-package "ASM::CARSTATE"))

(in-package "ROS")
;;//! \htmlinclude CarState.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass asm::CarState
  :super ros::object
  :slots (_header _mission _TS _EBS_supervisor _RES_Stop _RES_Go _speed ))

(defmethod asm::CarState
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:mission __mission) 0)
    ((:TS __TS) nil)
    ((:EBS_supervisor __EBS_supervisor) 0)
    ((:RES_Stop __RES_Stop) nil)
    ((:RES_Go __RES_Go) nil)
    ((:speed __speed) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _mission (round __mission))
   (setq _TS __TS)
   (setq _EBS_supervisor (round __EBS_supervisor))
   (setq _RES_Stop __RES_Stop)
   (setq _RES_Go __RES_Go)
   (setq _speed (float __speed))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:mission
   (&optional __mission)
   (if __mission (setq _mission __mission)) _mission)
  (:TS
   (&optional __TS)
   (if __TS (setq _TS __TS)) _TS)
  (:EBS_supervisor
   (&optional __EBS_supervisor)
   (if __EBS_supervisor (setq _EBS_supervisor __EBS_supervisor)) _EBS_supervisor)
  (:RES_Stop
   (&optional __RES_Stop)
   (if __RES_Stop (setq _RES_Stop __RES_Stop)) _RES_Stop)
  (:RES_Go
   (&optional __RES_Go)
   (if __RES_Go (setq _RES_Go __RES_Go)) _RES_Go)
  (:speed
   (&optional __speed)
   (if __speed (setq _speed __speed)) _speed)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8 _mission
    1
    ;; bool _TS
    1
    ;; uint8 _EBS_supervisor
    1
    ;; bool _RES_Stop
    1
    ;; bool _RES_Go
    1
    ;; float64 _speed
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8 _mission
       (write-byte _mission s)
     ;; bool _TS
       (if _TS (write-byte -1 s) (write-byte 0 s))
     ;; uint8 _EBS_supervisor
       (write-byte _EBS_supervisor s)
     ;; bool _RES_Stop
       (if _RES_Stop (write-byte -1 s) (write-byte 0 s))
     ;; bool _RES_Go
       (if _RES_Go (write-byte -1 s) (write-byte 0 s))
     ;; float64 _speed
       (sys::poke _speed (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8 _mission
     (setq _mission (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; bool _TS
     (setq _TS (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; uint8 _EBS_supervisor
     (setq _EBS_supervisor (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; bool _RES_Stop
     (setq _RES_Stop (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _RES_Go
     (setq _RES_Go (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; float64 _speed
     (setq _speed (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get asm::CarState :md5sum-) "ed1d98ad4090e2c54dae8c25435bf0e9")
(setf (get asm::CarState :datatype-) "asm/CarState")
(setf (get asm::CarState :definition-)
      "Header header
uint8 mission
bool TS
uint8 EBS_supervisor
bool RES_Stop
bool RES_Go
float64 speed
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :asm/CarState "ed1d98ad4090e2c54dae8c25435bf0e9")


