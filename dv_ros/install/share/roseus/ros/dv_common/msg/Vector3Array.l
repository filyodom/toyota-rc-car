;; Auto-generated. Do not edit!


(when (boundp 'dv_common::Vector3Array)
  (if (not (find-package "DV_COMMON"))
    (make-package "DV_COMMON"))
  (shadow 'Vector3Array (find-package "DV_COMMON")))
(unless (find-package "DV_COMMON::VECTOR3ARRAY")
  (make-package "DV_COMMON::VECTOR3ARRAY"))

(in-package "ROS")
;;//! \htmlinclude Vector3Array.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass dv_common::Vector3Array
  :super ros::object
  :slots (_header _vectors ))

(defmethod dv_common::Vector3Array
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:vectors __vectors) (let (r) (dotimes (i 0) (push (instance geometry_msgs::Vector3 :init) r)) r))
    )
   (send-super :init)
   (setq _header __header)
   (setq _vectors __vectors)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:vectors
   (&rest __vectors)
   (if (keywordp (car __vectors))
       (send* _vectors __vectors)
     (progn
       (if __vectors (setq _vectors (car __vectors)))
       _vectors)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Vector3[] _vectors
    (apply #'+ (send-all _vectors :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Vector3[] _vectors
     (write-long (length _vectors) s)
     (dolist (elem _vectors)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Vector3[] _vectors
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _vectors (let (r) (dotimes (i n) (push (instance geometry_msgs::Vector3 :init) r)) r))
     (dolist (elem- _vectors)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get dv_common::Vector3Array :md5sum-) "936f339da6b58e2df7955dbec9c7f03f")
(setf (get dv_common::Vector3Array :datatype-) "dv_common/Vector3Array")
(setf (get dv_common::Vector3Array :definition-)
      "Header header
geometry_msgs/Vector3[] vectors
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: geometry_msgs/Vector3
# This represents a vector in free space. 
# It is only meant to represent a direction. Therefore, it does not
# make sense to apply a translation to it (e.g., when applying a 
# generic rigid transformation to a Vector3, tf2 will only apply the
# rotation). If you want your data to be translatable too, use the
# geometry_msgs/Point message instead.

float64 x
float64 y
float64 z
")



(provide :dv_common/Vector3Array "936f339da6b58e2df7955dbec9c7f03f")


