;; Auto-generated. Do not edit!


(when (boundp 'motion_planning::Steering)
  (if (not (find-package "MOTION_PLANNING"))
    (make-package "MOTION_PLANNING"))
  (shadow 'Steering (find-package "MOTION_PLANNING")))
(unless (find-package "MOTION_PLANNING::STEERING")
  (make-package "MOTION_PLANNING::STEERING"))

(in-package "ROS")
;;//! \htmlinclude Steering.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass motion_planning::Steering
  :super ros::object
  :slots (_header _steeringAngle ))

(defmethod motion_planning::Steering
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:steeringAngle __steeringAngle) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _steeringAngle (float __steeringAngle))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:steeringAngle
   (&optional __steeringAngle)
   (if __steeringAngle (setq _steeringAngle __steeringAngle)) _steeringAngle)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float64 _steeringAngle
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float64 _steeringAngle
       (sys::poke _steeringAngle (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float64 _steeringAngle
     (setq _steeringAngle (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get motion_planning::Steering :md5sum-) "99afc1058f255e8ba96584f33e3be5d0")
(setf (get motion_planning::Steering :datatype-) "motion_planning/Steering")
(setf (get motion_planning::Steering :definition-)
      "Header header
float64 steeringAngle
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :motion_planning/Steering "99afc1058f255e8ba96584f33e3be5d0")


