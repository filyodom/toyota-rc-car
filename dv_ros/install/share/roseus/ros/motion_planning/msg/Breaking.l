;; Auto-generated. Do not edit!


(when (boundp 'motion_planning::Breaking)
  (if (not (find-package "MOTION_PLANNING"))
    (make-package "MOTION_PLANNING"))
  (shadow 'Breaking (find-package "MOTION_PLANNING")))
(unless (find-package "MOTION_PLANNING::BREAKING")
  (make-package "MOTION_PLANNING::BREAKING"))

(in-package "ROS")
;;//! \htmlinclude Breaking.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass motion_planning::Breaking
  :super ros::object
  :slots (_header _breaking ))

(defmethod motion_planning::Breaking
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:breaking __breaking) nil)
    )
   (send-super :init)
   (setq _header __header)
   (setq _breaking __breaking)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:breaking
   (&optional __breaking)
   (if __breaking (setq _breaking __breaking)) _breaking)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; bool _breaking
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; bool _breaking
       (if _breaking (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; bool _breaking
     (setq _breaking (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get motion_planning::Breaking :md5sum-) "9dd6dc81938db977e9ed62ea1c2ad7b8")
(setf (get motion_planning::Breaking :datatype-) "motion_planning/Breaking")
(setf (get motion_planning::Breaking :definition-)
      "Header header
bool breaking
================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :motion_planning/Breaking "9dd6dc81938db977e9ed62ea1c2ad7b8")


