;; Auto-generated. Do not edit!


(when (boundp 'motion_planning::MotorTorques)
  (if (not (find-package "MOTION_PLANNING"))
    (make-package "MOTION_PLANNING"))
  (shadow 'MotorTorques (find-package "MOTION_PLANNING")))
(unless (find-package "MOTION_PLANNING::MOTORTORQUES")
  (make-package "MOTION_PLANNING::MOTORTORQUES"))

(in-package "ROS")
;;//! \htmlinclude MotorTorques.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass motion_planning::MotorTorques
  :super ros::object
  :slots (_header _LeftFront _RightFront _LeftBack _RightBack ))

(defmethod motion_planning::MotorTorques
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:LeftFront __LeftFront) 0.0)
    ((:RightFront __RightFront) 0.0)
    ((:LeftBack __LeftBack) 0.0)
    ((:RightBack __RightBack) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _LeftFront (float __LeftFront))
   (setq _RightFront (float __RightFront))
   (setq _LeftBack (float __LeftBack))
   (setq _RightBack (float __RightBack))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:LeftFront
   (&optional __LeftFront)
   (if __LeftFront (setq _LeftFront __LeftFront)) _LeftFront)
  (:RightFront
   (&optional __RightFront)
   (if __RightFront (setq _RightFront __RightFront)) _RightFront)
  (:LeftBack
   (&optional __LeftBack)
   (if __LeftBack (setq _LeftBack __LeftBack)) _LeftBack)
  (:RightBack
   (&optional __RightBack)
   (if __RightBack (setq _RightBack __RightBack)) _RightBack)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float64 _LeftFront
    8
    ;; float64 _RightFront
    8
    ;; float64 _LeftBack
    8
    ;; float64 _RightBack
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float64 _LeftFront
       (sys::poke _LeftFront (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _RightFront
       (sys::poke _RightFront (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _LeftBack
       (sys::poke _LeftBack (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _RightBack
       (sys::poke _RightBack (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float64 _LeftFront
     (setq _LeftFront (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _RightFront
     (setq _RightFront (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _LeftBack
     (setq _LeftBack (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _RightBack
     (setq _RightBack (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get motion_planning::MotorTorques :md5sum-) "2bab8d054ec4aa480ead66ab590540f5")
(setf (get motion_planning::MotorTorques :datatype-) "motion_planning/MotorTorques")
(setf (get motion_planning::MotorTorques :definition-)
      "Header header
float64 LeftFront
float64 RightFront
float64 LeftBack
float64 RightBack

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

")



(provide :motion_planning/MotorTorques "2bab8d054ec4aa480ead66ab590540f5")


