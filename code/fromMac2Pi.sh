#!/bin/bash

if [ -z "$1" ]
then
	echo AP
	scp -o 'ProxyCommand ssh ctu@10.42.0.1 nc %h %p' Raspberry/data_acquisition.py pi@192.168.0.10:"/home/pi/Raspberry/data_acquisition.py"
else
	echo eduroam
	scp -o 'ProxyCommand ssh ctu@"$1" nc %h %p' Raspberry/data_acquisition.py pi@192.168.0.10:"/home/pi/Raspberry/data_acquisition.py"
fi