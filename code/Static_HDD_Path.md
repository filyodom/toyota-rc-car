# Static path of a mounted HDD ReadMe File

## Setting static path of a mounted HDD on Linux
Settings must be done on *Jetson* device for every new external HDD.

0) Open config file for editting, i.e.
    ```
    sudo nano /etc/fstab
    ```
1) Add a new line like the following:
    ```
    UUID=UUID_OF_DISK	MOUNT_DST_PATH		PartitionType		defaults		0 0
    ```
    Recently used configuration:
    ```
    UUID=882C51BA2C51A3CA   /media/ctu/ADATASE730H     ntfs    defaults     0 0
    ```
    `UUID_OF_DISK` - UUID is a unique identificator of the disk you want to be mounted (found in application "disks")

    `MOUNT_DST_PATH` - Path where the disk should be mounted

    `PartitionType` - Type of the partition

2) Save changes by pressing *ctrl+O* and exit the editor by pressing *ctrl+X*.

3) Restart computer for configuration to take effect.

## FAQ
* **I am replacing my old HDD with a new one. Should I only add a new line or should I edit the existing one?**

    Adding a new line will work just fine. There will be only extra line for the old HDD which will not be used if you don't want to use the old HDD anymore. Therefore the line for the old HDD should be removed.