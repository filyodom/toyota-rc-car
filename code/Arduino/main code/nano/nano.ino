
// https://github.com/xkam1x/Arduino-PWM-Reader

#include "src/pwm/PWM.hpp"

#define LED_PIN 13
#define THROTTLE_OUTPUT_PIN 9
#define THROTTLE_INPUT_PIN 2
#define STEER_OUTPUT_PIN 10
#define STEER_INPUT_PIN 3

bool blinkState = false;

PWM throttle(THROTTLE_INPUT_PIN);
PWM steer(STEER_INPUT_PIN);

uint16_t pwm_steer, pwm_throttle;

void setup() {
    Serial.begin(115200);
    
    pinMode(LED_PIN, OUTPUT);

    TCCR1B = TCCR2B & B11111000 | B00001100; // timer for steer and throttle
    //TCCR1B = TCCR2B & B11111000 | B00000100;
    pinMode(THROTTLE_OUTPUT_PIN, OUTPUT);
    pinMode(STEER_OUTPUT_PIN, OUTPUT);
    
    throttle.begin(true);
    steer.begin(true);
}


int action_code = 0;      // first byte of every message, to determine action
int incoming_ride[3];
int last_speed = 124;     //holds info about last requested speed 
int incoming_steer;
bool ride_input_done = 0;
bool steer_input_done = 0;
int s;
void loop() {
    pwm_throttle = throttle.getValue();
    pwm_steer = steer.getValue();
    
    analogWrite(STEER_OUTPUT_PIN, round(0.0516 * pwm_steer + 13.658)); // 71 - 107 (left to right, 90 is middle)
    analogWrite(THROTTLE_OUTPUT_PIN, round(0.2627 * pwm_throttle - 262.44)); // 31 - 237 (124 is stationary)


   //wait for action_code to determine if steering or ride input is on its way
    /*bool loop_exit = 0;
    while(1){
       while(Serial.available()){
          action_code = Serial.read();
          loop_exit = 1;
       }
       if(loop_exit == 1){
        break; 
       }
    }

    decide_action(action_code);*/

    blinkState = !blinkState;
    digitalWrite(LED_PIN, blinkState);
}

int ride(int ride_time, int ride_speed, int accel) {
    unsigned long time_now = millis();
    unsigned long loop_time = 0;

    if(ride_speed >= last_speed){                       //accelerate
      for(int i = last_speed; i <= ride_speed; i++) {
        analogWrite(THROTTLE_OUTPUT_PIN, i);
        if(check_for_input() == 23){
          break;
        }
        delay(accel);
        last_speed = i;
      }
    }
    else if(ride_speed < last_speed){                   //de-accelerate
      for(int i = last_speed; i >= ride_speed; i--) {
        analogWrite(THROTTLE_OUTPUT_PIN, i);
        if(check_for_input() == 23){
          break;
        }
        delay(accel);
        last_speed = i;
      }  
    }

    while(true) {
        check_for_input();
        unsigned long time_now2 = millis();
        loop_time = time_now2 - time_now;
        if(loop_time > (unsigned long) 1000*ride_time) {
            break;
        }
    }
}

void steer_car(int steer_pwm){
    analogWrite(STEER_OUTPUT_PIN, steer_pwm);
    Serial.println("Done");  
}

void stop_car(){
  analogWrite(THROTTLE_OUTPUT_PIN, 124);
  Serial.println("Done");
}

int check_for_input(){              //check for new input
  if(Serial.available()){
    action_code = Serial.read();
    decide_action(action_code);
    if(action_code == 23){ return 23; }
  }
  return 0;
}

void decide_action(int action_code){            // read action code and initiate correct action
    if(action_code == 21){                      // 21 - code for ride
      while(1){
          while(Serial.available() == 3){       // wait for ride inputs
            for (int i = 0; i < 3; i++){
              incoming_ride[i] = Serial.read();
              ride_input_done = 1;
            }
          }
          if(ride_input_done){
            ride_input_done = 0;
            incoming_ride[1] = (int) (124 + round(incoming_ride[1] * 1.13));      // incoming[1] is represented as % 
            incoming_ride[2] = (int) (500 - round(incoming_ride[2] * 4.5));       // 500 at zero percent, 50 at 100 percent
            Serial.println(incoming_ride[0]);
            Serial.println(incoming_ride[1]);
            Serial.println(incoming_ride[2]);
            ride(incoming_ride[0], incoming_ride[1], incoming_ride[2]);
            break;
          }
      }
    }

    else if(action_code == 22){             // 22 - code for steer
      while(1){
        while(Serial.available() == 1){       // wait for steer input
          incoming_steer = Serial.read();
          Serial.println(incoming_steer);
          steer_input_done = 1;
        }
        if(steer_input_done){
          steer_input_done = 0;
          steer_car(incoming_steer);
          break;
        }
      }
    }

    else if(action_code == 23){           // 22 - code for stop
      stop_car();
    }
}
