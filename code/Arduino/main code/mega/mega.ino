// https://github.com/xkam1x/Arduino-PWM-Reader

#include "src/pwm/PWM.hpp"

#define LED_PIN 13
#define STEER_PIN 18
#define THROTTLE_PIN 19
#define HALL_PIN A0

PWM steer(STEER_PIN);
PWM throttle(THROTTLE_PIN);

uint16_t pwm_steer, pwm_throttle;
int hall, old_hall;
bool blinkState = false;
unsigned long time1 = 0;
unsigned long time2 = 0;
float quick_rpm = 0;
float precise_rpm = 0;
bool spike = false;

void setup() {
    Serial.begin(1000000,SERIAL_8E2);  // output, even parity, 2 stop bits
    while (!Serial) {;}

    pinMode(LED_PIN, OUTPUT);

    steer.begin(true);
    throttle.begin(true);
}

void loop() {
    hall = analogRead(HALL_PIN);

    // positive spike
    if(hall > 550 && old_hall > hall && !spike) {
        quick_rpm = (((1000000*60*(50/13)) / (micros() - time1)) / 10);
        time1 = micros();
        spike = true;
    }

    // negative spike
    if(hall < 450 && old_hall < hall && !spike) {
        quick_rpm = (((1000000*60*(50/13)) / (micros() - time1)) / 10);
        precise_rpm = ((1000000*60*(50/13)) / (micros() - time2));
        time1 = micros();
        time2 = micros();
        spike = true;
    }

    // reset
    if(hall < 550 && hall > 450) {
        spike = false;
    }

    if(micros() - time1 > 30000) { //100000 - nuly
        quick_rpm = 0;
    }

    //Serial.println(quick_rpm);

    old_hall = hall;
             
    if(Serial.available()) {
        int handshake = 0;
        handshake = Serial.read();           //send data after request comes from raspberry
        if(handshake) {
             pwm_steer = steer.getValue();
             pwm_throttle = throttle.getValue();
             
             sendInt16Uart(pwm_steer);
             sendInt16Uart(pwm_throttle);
             sendInt32Uart((int)quick_rpm);
             sendInt32Uart((int)precise_rpm);
        }
    }
    blinkState = !blinkState;
    digitalWrite(LED_PIN, blinkState);
}

void sendInt16Uart(int16_t data) {
    Serial.write(0);
    Serial.write(0);
    Serial.write((uint8_t)(data >> 8));
    Serial.write((uint8_t)(data & 0xFF));
}

void sendInt32Uart(int32_t data) {
    Serial.write((uint8_t)(data >> 24));
    Serial.write((uint8_t)(data >> 16));
    Serial.write((uint8_t)(data >> 8));
    Serial.write((uint8_t)(data & 0xFF));
}

void sendFloatUart(float data) {
    uint8_t* b = (uint8_t*) &data;
    Serial.write(b[3]);
    Serial.write(b[2]);
    Serial.write(b[1]);
    Serial.write(b[0]);
}
