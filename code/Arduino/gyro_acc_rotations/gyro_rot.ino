// Arduino gyroskop a akcelerometr 1
// připojení knihovny Wire
#include "Wire.h"
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"
// inicializace proměnné pro určení adresy senzoru
// 0x68 nebo 0x69, dle připojení AD0
const int MPU_addr=0x68;
MPU6050 mpu;
// inicializace proměnných, do kterých se uloží data
float AcX,AcY,AcZ;
float rotace[3];        // rotace kolem os x,y,z
Quaternion q;           // [w, x, y, z] kvaternion
VectorFloat gravity;    // [x, y, z] vektor setrvačnosti
bool dmpReady = false;
uint8_t mpuIntStatus;
uint8_t devStatus;
uint16_t packetSize;
uint16_t fifoCount;
uint8_t fifoBuffer[64];
volatile bool mpuInterrupt = false;
void dmpINT() {
  mpuInterrupt = true;
}
void setup()
{
  // komunikace přes I2C sběrnici
  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
  mpu.initialize();
  // komunikace přes sériovou linku rychlostí 115200 baud
  Serial.begin(115200);
    devStatus = mpu.dmpInitialize();
  // kontrola funkčnosti DMP
  if (devStatus == 0) {
      // zapnutí DMP
     // Serial.println(F("Povoleni DMP..."));
      mpu.setDMPEnabled(true);
      // nastavení pinu INT jako přerušovacího, interrupt 0 odpovídá pinu 2
      attachInterrupt(0, dmpINT, RISING);
      mpuIntStatus = mpu.getIntStatus();
      //Serial.println(F("DMP pripraveno, cekam na prvni preruseni.."));
      dmpReady = true;
      // načtení velikosti zpráv, které bude DMP posílat
      packetSize = mpu.dmpGetFIFOPacketSize();
  }
  else {
      // V případě chyby:
      // 1 : selhání připojení k DMP
      // 2 : selhání při nastavení DMP
     // Serial.print(F("DMP inicializace selhala (kod "));
     // Serial.print(devStatus);
      //Serial.println(F(")"));
  }
}
void loop()
{
  // zapnutí přenosu
  Wire.beginTransmission(MPU_addr);
  // zápis do registru ACCEL_XOUT_H
  Wire.write(0x3B);
  Wire.endTransmission(false);
  // vyzvednutí dat z 14 registrů
  Wire.requestFrom(MPU_addr,14,true);
  AcX=Wire.read()<<8|Wire.read();    
  AcY=Wire.read()<<8|Wire.read();
  AcZ=Wire.read()<<8|Wire.read();
if (!dmpReady) return;
// tato podmínka čeká na příjem přerušení a můžeme v ní provádět
// ostatní operace
while (!mpuInterrupt && fifoCount < packetSize) {
    // místo pro ostatní operace
    // ..
}
// získání informace o statusu DSP
mpuInterrupt = false;
mpuIntStatus = mpu.getIntStatus();
// získání velikosti zásobníku dat
fifoCount = mpu.getFIFOCount();
// kontrola přetečení zásobníku dat
if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
    mpu.resetFIFO();
    //Serial.println(F("Preteceni zasobniku dat!"));
    // v případě přetečení zásobníku je nutné
    // častěji vyčítat data
}
else if (mpuIntStatus & 0x02) {
    // kontrola délky dat
    while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
    // čtení paketu ze zásobníku
    mpu.getFIFOBytes(fifoBuffer, packetSize);
    // při větším množství paketů snížíme počítadlo
    fifoCount -= packetSize;
    // přečtení dat z DSP a uložení do proměnných
    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetYawPitchRoll(rotace, &q, &gravity);
   
   
}
  
  // výpis surových dat z proměnných na sériovou linku
  Serial.print(""); Serial.print(AcX/16384);
  Serial.print(";"); Serial.print(AcY/16384);
  Serial.print(";"); Serial.print(AcZ/16384);
  Serial.print(";"); Serial.print(rotace[2] * 180/M_PI);
  Serial.print(";"); Serial.print(rotace[1] * 180/M_PI);
  Serial.print(";"); Serial.println(rotace[0] * 180/M_PI);
  delay(100);
 
}