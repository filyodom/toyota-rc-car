# Configuration of ssh server

## Changing configuration of ssh server for connecting with password on Linux
Settings must be done on *Jetson* device.

0) Open config file for editting, i.e.
    ```
    sudo nano /etc/ssh/sshd_config
    ```
1) Change line beginning with `PermitRootLogin` to:
    ```
    PermitRootLogin yes 
    ```
2) Change line beginning with `PasswordAuthentication` to:
    ```
    PasswordAuthentication yes
    ```
3) Save changes by pressing *ctrl+O* and exit the editor by pressing *ctrl+X*.

4) Restart computer or use the next command for configuration to take effect.
    ```
    sudo service ssh restart
    ```