# Configuration and connecting to vnc server on Jetson

## Configuration of vnc server on Linux
Settings must be done on *Jetson* device.

0) Install X11VNC:
    ```
    sudo apt-get install x11vnc
    ```
1) Change password for connecting through vnc in cmd:
    ```
    x11vnc -storepasswd

    Enter VNC password: *********
    Verify password: *********  
    Write password to /home/rahul/.vnc/passwd?  [y]/n y
    Password written to: /home/rahul/.vnc/passwd
    ```
2) Starting vnc server after startup:
    
    Open application `Startup Applications` and add a new command after startup (or write to */etc/rc.local*):
    ```
    x11vnc -loop -forever -usepw
    ```
3) Restart computer for configuration to take effect.

4) Add new Access Point, see example:
    ````
    nmcli con add type wifi ifname wlan0 mode ap con-name WIFI_AP ssid MY_AP
    nmcli con modify WIFI_AP 802-11-wireless.band bg
    nmcli con modify WIFI_AP 802-11-wireless.channel 1
    nmcli con modify WIFI_AP 802-11-wireless-security.key-mgmt wpa-psk
    nmcli con modify WIFI_AP 802-11-wireless-security.proto rsn
    nmcli con modify WIFI_AP 802-11-wireless-security.group ccmp
    nmcli con modify WIFI_AP 802-11-wireless-security.pairwise ccmp
    nmcli con modify WIFI_AP 802-11-wireless-security.psk 11223344
    nmcli con modify WIFI_AP ipv4.method shared
    nmcli con up WIFI_AP

    ````

5) It is reccomended to configure SSH server to avoid some problems with connection.

## Connecting to vnc server

0) Install any vnc viewer for example from this website:
    ```
    https://www.realvnc.com/en/connect/download/viewer/
    ```
1) Get i.p. address of the jetson nano by  running `ifconfig` in cmd and choose i.p. address of the wifi adapter, which should be `wlan0`.
    * I.p. address can be for example `147.32.219.49` when connected to `eduroam`.

    * When using `AP mode` (Access Point mode) the i.p. address should be `10.42.0.1`.

2) Run installed VNC Viewer on your computer and connect to Jetson's i.p. address with port 5900. For example `147.32.219.49:5900`.

3) Password for connection was configured at poin 1) in configuration part. Recently used password is `CTU`.

## FAQ
* **I have encoutered really bad resolution when connected through vnc viewer. How can i fix this?**

    Start up jetson with pluged monitor, the resolution will be then normal. When started without a real monitor pluged in, the resolution is then really low and we don't have a quick fix for this at the time. The only solution is to buy HDMI plug which simulates connected monitor.

