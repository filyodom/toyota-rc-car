# sudo crontab -e
# @reboot python3 /home/pi/Raspberry/data_acquisition.py

import spidev
import time
import argparse 
import sys
import serial
import struct
import time
import math
import signal
import logging

from multiprocessing import Process, Value, Array, Manager
from multiprocessing.managers import BaseManager, SyncManager

import navio.mpu9250
import navio.lsm9ds1
import navio.util
import navio.ublox
import navio.pwm
import navio.util
import navio.leds

class Navio:
    def __init__(self, arduino_port, jetson_port, throttle_pwm_port, steer_pwm_port, pwm_freq, gps_buffer_size, arduino_buffer_size, throttle_boundary, steer_boundary, throttle_center, steer_center, 
        full_brake, fail_time, abs_ramp_time, abs_breaking_level, abs_acc_threshold, abs_rpm_threshold):

        # parameters
        self.__ARDUINO_BUFFER_SIZE = arduino_buffer_size        # arduino buffer size in bytes (2x RPM INT + 2x REF PWM INT = 16B)
        self.__THROTTLE_CENTER = throttle_center                # center value of throttle pwm in ms (when the car is on neutral)
        self.__STEER_CENTER = steer_center                      # center value of steer pwm in ms (wheels are perpendicular to the car)
        self.__THROTTLE_BOUNDARY = throttle_boundary            # maximum value in ms that can be added to throttle_center,
                                                                # if this value is exceeded, throttle pwm is saturated and the car is at maximum speed
        self.__STEER_BOUNDARY = steer_boundary                  # maximum value in ms that can be added/subtracted to steer_center,
                                                                # if this value is exceeded, steer pwm is saturated and the wheels are turned to the right/left maximally
        self.__FULL_BRAKE = full_brake                          # value in ms that is subtracted from throttle_center for maximum braking,
        self.__FAIL_TIME = fail_time                            # if Raspberry does not receive new command within this time, the car stops
        self.__ABS_RAMP_TIME = abs_ramp_time                    # parameter that specifies slope of the braking ramp in ABS
                                                                # the greater the abs_ramp_time, the lower the braking speed
        self.__ABS_BREAKING_LEVEL = abs_breaking_level          # not used (abs_k is used)
        self.__ABS_ACC_THRESHOLD = abs_acc_threshold            # if the absolute value of car acceleration is below abs_acc_threshold, ABS turns off (it is assumed that the car is stationary)
        self.__ABS_RPM_THRESHOLD = abs_rpm_threshold            # if the motor speed is below abs_rpm_threshold, the wheels are blocked and ABS starts releasing braking force 

        # shared variables
        self.__gps_buffer = Array('i', range(gps_buffer_size))
        self.__new_gps = Value('i', 0)
        self.__arduino_buffer = Array('B', range(arduino_buffer_size))
        self.__new_arduino = Value('i', 0)
        self.__quick_rpm = Value('i', 0)
        self.__precise_rpm = Value('i', 0)
        self.__steer_reference_arduino = Value('f', self.__STEER_CENTER)
        self.__throttle_reference_arduino = Value('f', self.__THROTTLE_CENTER)
        self.__steer_reference = Value('f', self.__STEER_CENTER)
        self.__throttle_reference = Value('f', self.__THROTTLE_CENTER)
        self.__real_steer = Value('f', 0)
        self.__real_throttle = Value('f', 0)
        self.__last_command_time = Value('d', 0)
        self.__flags = Array('B', [0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0]) # 1. bit - new GPS values
                                                                            # 2. bit - new arduino values
                                                                            # 3. bit - new throttle reference from Jetson
                                                                            # 4. bit - new steer reference from Jetson
                                                                            # 5. bit - ABS breaking
        self.__brake_command = Value('i', 0)
        self.__accY = Value('f', 0)
        self.__abs_k = Value('f', 1)

        # variables
        self.__imu_buffer = []
        self.__last_gps_buffer = []
        self.__last_arduino_buffer = []
        self.__prev_rpm = 0

        # initializations
        self.__initIMU()
        self.__initGPS()
        self.__initPWM(throttle_pwm_port, steer_pwm_port, pwm_freq)
        self.__initSerial(arduino_port, jetson_port)
        navio.util.check_apm()
        led = navio.leds.Led()
        led.setColor('Green')

    # Start all threads depending on parameters:
    #                                                           imu: True if imu data are gathered
    #                                                           gps: True if gps data are gathered
    #                                                           arduino: True if arduino data (user references, quick rpm, precise rpm) are gathered
    #                                                           send: True if the data are sent to Jetson
    #                                                           pwm: True if pwm outputs are generated
    #                                                           direct_throttle: True throttle is controlled by remote controlller, False if it is controlled by Jetson
    #                                                           direct_steer: True streer is controlled by remote controlller, False if it is controlled by Jetson
    #                                                           abs_on: True if ABS is enabled
    def start(self, imu=True, gps=True, arduino=True, send=True, pwm=True, direct_throttle=True, direct_steer=True, abs_on=True):
        # processes init
        processes = []
        manager = SyncManager()
        manager.start(self.__mgr_init)

        # GPS thread
        if gps:
            gps_p = Process(target=self.__readGPS, args=(self.__gps_buffer, self.__new_gps, self.__flags))
            processes.append(gps_p)
            gps_p.start()

        # arduino thread
        if arduino:
            arduino_p = Process(target=self.__readArduino, args=(self.__arduino_buffer, self.__new_arduino, self.__flags, self.__last_command_time))
            processes.append(arduino_p)
            arduino_p.start()

        if pwm:
            # throttle PWM thread
            throttle_p = Process(target=self.__setThrottle, args=(self.__real_throttle, direct_steer, abs_on))
            processes.append(throttle_p)
            throttle_p.start()

            # steer PWM thread
            steer_p = Process(target=self.__setSteer, args=(self.__real_steer, direct_steer))
            processes.append(steer_p)
            steer_p.start()

            # Jetson listener thread
            jetson_p = Process(target=self.__jetsonCommands, args=(self.__throttle_reference, self.__steer_reference, self.__last_command_time, self.__flags, self.__brake_command, self.__abs_k))
            processes.append(jetson_p)
            jetson_p.start()

        # sending thread
        if send:
            send_p = Process(target=self.__sendBuffer, args=(imu, gps, arduino, self.__accY))
            processes.append(send_p)
            send_p.start()

        # sigint handler
        try:
            for process in processes:
                process.join()
        except KeyboardInterrupt:
            self.__arduino_serial.close()
            self.__jetson_serial.close()
            print("Main thread exit")

    def __mgr_sig_handler(self, signal, frame):
        return None

    def __mgr_init(self):
        signal.signal(signal.SIGINT, self.__mgr_sig_handler)

    ################## init functions ##################

    def __initIMU(self):
        self.__imu1 = navio.mpu9250.MPU9250()
        self.__imu2 = navio.lsm9ds1.LSM9DS1()

        if self.__imu1.testConnection():
            print("MPU9250 INIT OK")
        else:
            sys.exit("MPU9250 INIT ERROR")
            
        if self.__imu2.testConnection():
            print("LSM9DS1 INIT OK")
        else:
            sys.exit("LSM9DS1 INIT ERROR")

        self.__imu1.initialize()
        self.__imu2.initialize()

    def __initGPS(self):
        self.__ubl = navio.ublox.UBlox("spi:0.0", baudrate=5000000, timeout=2)

        self.__ubl.configure_poll_port()
        self.__ubl.configure_poll(navio.ublox.CLASS_CFG, navio.ublox.MSG_CFG_USB)
        #self.__ubl.configure_poll(navio.ublox.CLASS_MON, navio.ublox.MSG_MON_HW)

        self.__ubl.configure_port(port=navio.ublox.PORT_SERIAL1, inMask=1, outMask=0)
        self.__ubl.configure_port(port=navio.ublox.PORT_USB, inMask=1, outMask=1)
        self.__ubl.configure_port(port=navio.ublox.PORT_SERIAL2, inMask=1, outMask=0)
        self.__ubl.configure_poll_port()
        self.__ubl.configure_poll_port(navio.ublox.PORT_SERIAL1)
        self.__ubl.configure_poll_port(navio.ublox.PORT_SERIAL2)
        self.__ubl.configure_poll_port(navio.ublox.PORT_USB)
        self.__ubl.configure_solution_rate(rate_ms=100)

        self.__ubl.set_preferred_dynamic_model(None)
        self.__ubl.set_preferred_usePPP(None)

        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_POSLLH, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_PVT, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_STATUS, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_SOL, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_VELNED, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_SVINFO, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_VELECEF, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_POSECEF, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_RXM, navio.ublox.MSG_RXM_RAW, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_RXM, navio.ublox.MSG_RXM_SFRB, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_RXM, navio.ublox.MSG_RXM_SVSI, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_RXM, navio.ublox.MSG_RXM_ALM, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_RXM, navio.ublox.MSG_RXM_EPH, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_TIMEGPS, 1)
        self.__ubl.configure_message_rate(navio.ublox.CLASS_NAV, navio.ublox.MSG_NAV_CLOCK, 1)

        print("GPS INIT OK")

    def __initSerial(self, arduino_port, jetson_port):
        try:
            self.__arduino_serial = serial.Serial(arduino_port, baudrate=1000000, parity=serial.PARITY_EVEN, stopbits=serial.STOPBITS_TWO)
            self.__jetson_serial = serial.Serial(jetson_port, baudrate=1000000, parity=serial.PARITY_EVEN, stopbits=serial.STOPBITS_TWO)
            print("SERIAL INIT OK")
        except Exception as e:
            self.__arduino_serial = None
            self.__jetson_serial = None
            print(e)
            sys.exit("SERIAL INIT FAIL")

    def __initPWM(self, throttle_port, steer_port, frequency):
            self.__throttle_pwm = navio.pwm.PWM(throttle_port)
            self.__throttle_pwm.initialize()
            self.__throttle_pwm.set_period(frequency)
            self.__throttle_pwm.enable()
            self.__throttle_pwm.set_duty_cycle(self.__THROTTLE_CENTER)

            self.__steer_pwm = navio.pwm.PWM(steer_port)
            self.__steer_pwm.initialize()
            self.__steer_pwm.set_period(frequency)
            self.__steer_pwm.enable()
            self.__steer_pwm.set_duty_cycle(self.__STEER_CENTER)

    # Read IMU units, save it to shared IMU buffer and return acceleration in Y axis
    def __readIMU(self):
        ac1, gy1, ma1 = self.__imu1.getMotion9()
        ac2, gy2, ma2 = self.__imu2.getMotion9()

        ac11b = bytearray(struct.pack('f', ac1[0]))
        ac12b = bytearray(struct.pack('f', ac1[1]))
        ac13b = bytearray(struct.pack('f', ac1[2]))
        ac21b = bytearray(struct.pack('f', ac2[0]))
        ac22b = bytearray(struct.pack('f', ac2[1]))
        ac23b = bytearray(struct.pack('f', ac2[2]))

        gy11b = bytearray(struct.pack('f', gy1[0]))
        gy12b = bytearray(struct.pack('f', gy1[1]))
        gy13b = bytearray(struct.pack('f', gy1[2]))
        gy21b = bytearray(struct.pack('f', gy2[0]))
        gy22b = bytearray(struct.pack('f', gy2[1]))
        gy23b = bytearray(struct.pack('f', gy2[2]))

        ma11b = bytearray(struct.pack('f', ma1[0]))
        ma12b = bytearray(struct.pack('f', ma1[1]))
        ma13b = bytearray(struct.pack('f', ma1[2]))
        ma21b = bytearray(struct.pack('f', ma2[0]))
        ma22b = bytearray(struct.pack('f', ma2[1]))
        ma23b = bytearray(struct.pack('f', ma2[2]))

        self.__imu_buffer = []

        self.__imu_buffer.append(bytes(ac11b))
        self.__imu_buffer.append(bytes(ac12b))
        self.__imu_buffer.append(bytes(ac13b))
        self.__imu_buffer.append(bytes(ac21b))
        self.__imu_buffer.append(bytes(ac22b))
        self.__imu_buffer.append(bytes(ac23b))

        self.__imu_buffer.append(bytes(gy11b))
        self.__imu_buffer.append(bytes(gy12b))
        self.__imu_buffer.append(bytes(gy13b))
        self.__imu_buffer.append(bytes(gy21b))
        self.__imu_buffer.append(bytes(gy22b))
        self.__imu_buffer.append(bytes(gy23b))

        self.__imu_buffer.append(bytes(ma11b))
        self.__imu_buffer.append(bytes(ma12b))
        self.__imu_buffer.append(bytes(ma13b))
        self.__imu_buffer.append(bytes(ma21b))
        self.__imu_buffer.append(bytes(ma22b))
        self.__imu_buffer.append(bytes(ma23b))

        return ac2[1]

    # Read GPS (NAV_PVT message) and save it to shared GPS buffer
    def __readGPS(self, buffer, new_gps, flags):
        try:
            while True:
                try:
                    msg = self.__ubl.receive_message()
                except navio.ublox.UBloxError:
                    continue
                except OSError:
                    continue 
                if msg is None:
                    if opts.reopen:
                        ubl.close()
                        ubl = navio.ublox.UBlox("spi:0.0", baudrate=5000000, timeout=2)
                        continue
                    print(empty)
                    break

                if msg.name() == "NAV_PVT":
                    outstr = str(msg).split(",")[1:]

                    buffer[0] = int(outstr[13][5:])  # Longitude [deg / 1e-7]
                    buffer[1] = int(outstr[14][5:])  # Latitude [deg / 1e-7]
                    buffer[2] = int(outstr[17][6:])  # Horizontal Accuracy Estimate [mm]
                    buffer[3] = int(outstr[18][6:])  # Vertical Accuracy Estimate [mm]
                    buffer[4] = int(outstr[19][6:])  # NED north velocity [mm/s]
                    buffer[5] = int(outstr[20][6:])  # NED east velocity [mm/s]
                    buffer[6] = int(outstr[21][6:])  # NED down velocity [mm/s]
                    buffer[7] = int(outstr[22][8:])  # Ground Speed (2-D) [mm/s]
                    buffer[8] = int(outstr[23][9:])  # Heading of motion 2-D [deg / 1e-5]
                    buffer[9] = int(outstr[24][6:])  # Speed Accuracy Estimate [mm/s]
                    buffer[10] = int(outstr[25][9:]) # Heading Accuracy Estimate (both motion & vehicle) [deg / 1e-5]

                    flags[0] = 0x1
                    new_gps.value = 1
        except KeyboardInterrupt:
            print("Thread 1 exit")

    # Process the GPS buffer and prepare it for sending
    def __GPStoBytes(self):
        lon = bytearray(struct.pack('i', self.__gps_buffer[0]))
        lat = bytearray(struct.pack('i', self.__gps_buffer[1]))
        hAcc = bytearray(struct.pack('i', self.__gps_buffer[2]))
        vAcc = bytearray(struct.pack('i', self.__gps_buffer[3]))
        velN = bytearray(struct.pack('i', self.__gps_buffer[4]))
        velE = bytearray(struct.pack('i', self.__gps_buffer[5]))
        velD = bytearray(struct.pack('i', self.__gps_buffer[6]))
        gSpeed = bytearray(struct.pack('i', self.__gps_buffer[7]))
        heading = bytearray(struct.pack('i', self.__gps_buffer[8]))
        sAcc = bytearray(struct.pack('i', self.__gps_buffer[9]))
        headAcc = bytearray(struct.pack('i', self.__gps_buffer[10]))

        gps_buffer = []
        gps_buffer.append(bytes(lon))
        gps_buffer.append(bytes(lat))
        gps_buffer.append(bytes(hAcc))
        gps_buffer.append(bytes(vAcc))
        gps_buffer.append(bytes(velN))
        gps_buffer.append(bytes(velE))
        gps_buffer.append(bytes(velD))
        gps_buffer.append(bytes(gSpeed))
        gps_buffer.append(bytes(heading))
        gps_buffer.append(bytes(sAcc))
        gps_buffer.append(bytes(headAcc))

        return gps_buffer

    # Read arduino values via serial line and save it to shared arduino buffer
    def __readArduino(self, buffer, new_arduino, flags, last_command_time):
        try:
            while True:
                if not new_arduino.value:
                    buffer1 = []
                    self.__arduino_serial.write(struct.pack('>B', 1)) # data request to arduino
                    for i in range(self.__ARDUINO_BUFFER_SIZE):
                        buffer1.append(self.__arduino_serial.read(1))
                    for i in range(len(buffer1)):
                        buffer[i] = int.from_bytes(buffer1[i], "big", signed=False)

                    flags[1] = 0x1
                    new_arduino.value = 1
                    last_command_time.value = time.time()
        except KeyboardInterrupt:
            print("Thread 2 exit")

    # Process the arduino buffer and prepare it for sending
    # Save all values to shared variables for ABS controll
    def __arduinotoBytes(self):
        arduino_buffer = []

        for i in range(self.__ARDUINO_BUFFER_SIZE // 4):
            b = bytearray()
            b.append(self.__arduino_buffer[4*i+3])
            b.append(self.__arduino_buffer[4*i+2])
            b.append(self.__arduino_buffer[4*i+1])
            b.append(self.__arduino_buffer[4*i])
            arduino_buffer.append(bytes(b))

            if i == 0:
                self.__steer_reference_arduino.value = struct.unpack('<i', b)[0] / 1000
            if i == 1:
                temp_throttle_reference = struct.unpack('<i', b)[0] / 1000
                if temp_throttle_reference < 1 or temp_throttle_reference > 2:
                    self.__throttle_reference_arduino.value = self.__THROTTLE_CENTER
                else:
                    self.__throttle_reference_arduino.value = temp_throttle_reference
            if i == 2:
                self.__quick_rpm.value = struct.unpack('<i', b)[0]
            if i == 3:
                self.__precise_rpm.value = struct.unpack('<i', b)[0]

        return arduino_buffer

    # Send chosen buffers + real throttle and steer + flags to Jetson
    def __sendBuffer(self, imu, gps, arduino, accY):
        try:
            while True:
                # send start bytes
                self.__jetson_serial.write(struct.pack('BBBB', 0xAA, 0xAA, 0xAA, 0xAA))

                # send IMU
                if imu:
                    accY.value = self.__readIMU()
                    for val in range(len(self.__imu_buffer)):
                        self.__jetson_serial.write(self.__imu_buffer[val])

                # send GPS
                if gps:
                    if self.__new_gps.value:
                        self.__last_gps_buffer = self.__GPStoBytes()
                    for val in range(len(self.__last_gps_buffer)):
                        self.__jetson_serial.write(self.__last_gps_buffer[val])
                    self.__new_gps.value = 0 # opravit, musí být sdílená

                # send Arduino (references + rpm)
                if arduino:
                    if self.__new_arduino.value:
                        self.__last_arduino_buffer = self.__arduinotoBytes()
                    self.__jetson_serial.write(struct.pack('<i', int(self.__steer_reference.value * 1000)))
                    self.__jetson_serial.write(struct.pack('<i', int(self.__throttle_reference.value * 1000)))
                    self.__jetson_serial.write(struct.pack('<i', self.__quick_rpm.value))
                    self.__jetson_serial.write(struct.pack('<i', self.__precise_rpm.value))
                    self.__new_arduino.value = 0

                # send real PWMs
                self.__jetson_serial.write(struct.pack('<i', int(self.__real_steer.value * 1000)))
                self.__jetson_serial.write(struct.pack('<i', int(self.__real_throttle.value * 1000)))

                # send abs_k
                self.__jetson_serial.write(struct.pack('<f', self.__abs_k.value))

                # send parameters
                self.__jetson_serial.write(struct.pack('<f', self.__THROTTLE_CENTER))
                self.__jetson_serial.write(struct.pack('<f', self.__STEER_CENTER))
                self.__jetson_serial.write(struct.pack('<f', self.__THROTTLE_BOUNDARY))
                self.__jetson_serial.write(struct.pack('<f', self.__STEER_BOUNDARY))
                self.__jetson_serial.write(struct.pack('<f', self.__FULL_BRAKE))
                self.__jetson_serial.write(struct.pack('<f', self.__FAIL_TIME))
                self.__jetson_serial.write(struct.pack('<f', self.__ABS_RAMP_TIME))
                self.__jetson_serial.write(struct.pack('<f', self.__ABS_ACC_THRESHOLD))
                self.__jetson_serial.write(struct.pack('<f', self.__ABS_RPM_THRESHOLD))

                # send flags
                self.__jetson_serial.write(struct.pack('B', self.__flags[0] | 2 * self.__flags[1] | 4 * self.__flags[2] | 8 * self.__flags[3] |
                                                        16 * self.__flags[4] | 32 * self.__flags[5] | 64 * self.__flags[6] | 128 * self.__flags[7]))
                for i in range(len(self.__flags)):
                    self.__flags[i] = 0x0
        except KeyboardInterrupt:
            print("Thread 6 exit")

    # Receive different types of commands from Jeston and set shared varaibles depending on the command
    # 0 for speed setpoint, 1 for steer setpoint, 2 for hard stop, 3 for ABS stop, 4 for change of abs_k
    def __jetsonCommands(self, throttle_reference, steer_reference, last_command_time, flags, brake_command, abs_k):
        try:
            while True:
                command = struct.unpack('>B', self.__jetson_serial.read(1))[0]
                if command == 0:
                    flags[2] = 0x1
                    #last_command_time.value = time.time()
                    throttle_reference.value = struct.unpack('i', self.__jetson_serial.read(4))[0] / 1000
                if command == 1:
                    flags[3] = 0x1
                    steer_reference.value = struct.unpack('i', self.__jetson_serial.read(4))[0] / 1000
                if command == 2:
                    brake_command.value = 1
                    flags[2] = 0x1
                if command == 3:
                    brake_command.value = 2
                    flags[2] = 0x1
                if command == 4:
                    abs_k.value = struct.unpack('f', self.__jetson_serial.read(4))[0]
        except KeyboardInterrupt:
            print("Thread 5 exit")

     # Set throttle PWM by reading the desired value (from remote controller or Jetson) and processing it (limits)
     # Also control braking (full brake or ABS)
    def __setThrottle(self, real_throttle, direct_throttle, abs_on):
        try:
            print("THROTTLE CONTROL OK")
            time.sleep(1)
            abs_counter = 0
            while True:
                '''for i in range(200):
                    real_throttle.value = self.__THROTTLE_CENTER + i / 1000
                    self.__throttle_pwm.set_duty_cycle(real_throttle.value)
                    time.sleep(0.05)
                real_throttle.value = self.__THROTTLE_CENTER
                self.__throttle_pwm.set_duty_cycle(real_throttle.value)
                break'''

                if time.time() - self.__last_command_time.value > self.__FAIL_TIME:
                    print("STOP")
                    if self.__quick_rpm.value > 0:
                        self.__throttle_pwm.set_duty_cycle(self.__THROTTLE_CENTER - self.__FULL_BRAKE)
                    else:
                        self.__throttle_pwm.set_duty_cycle(self.__THROTTLE_CENTER)
                    while True:
                        pass

                if direct_throttle:
                    self.__throttle_reference.value = self.__throttle_reference_arduino.value

                # set PWM
                if self.__throttle_reference.value > self.__THROTTLE_CENTER + self.__THROTTLE_BOUNDARY:
                    real_throttle.value = self.__THROTTLE_CENTER + self.__THROTTLE_BOUNDARY
                else:
                    real_throttle.value = self.__throttle_reference.value

                self.__throttle_pwm.set_duty_cycle(real_throttle.value)

                #print("throttle reference: ", self.__throttle_reference.value, "\treal throttle:", real_throttle.value, "\trpm:", self.__quick_rpm.value)

                # max brake
                if self.__brake_command.value == 1:
                    self.__throttle_reference.value = self.__THROTTLE_CENTER - self.__FULL_BRAKE
                    self.__throttle_pwm.set_duty_cycle(self.__throttle_reference.value)
                    time.sleep(5)
                    self.__brake_command.value = 0
                    self.__throttle_reference.value = self.__THROTTLE_CENTER

                # ABS
                if (self.__brake_command.value == 2 or self.__throttle_reference.value < self.__THROTTLE_CENTER - 0.2) and abs_on:
                    abs_counter += 1
                    if abs_counter > 4:
                        print("ABS on")
                        abs_max = self.__THROTTLE_CENTER - self.__FULL_BRAKE
                        real_throttle.value = self.__THROTTLE_CENTER
                        abs_counter = 0
                        abs_quit_counter = 0
                        while True:
                            self.__flags[4] = 0x1
                            #print(real_throttle.value, self.__quick_rpm.value)
                            if real_throttle.value > abs_max:
                                real_throttle.value -=  self.__ABS_RAMP_TIME
                                self.__throttle_pwm.set_duty_cycle(real_throttle.value)
                            if self.__quick_rpm.value <= self.__ABS_RPM_THRESHOLD:
                                #print("nula, acc: ", self.__accY.value)
                                if math.fabs(self.__accY.value) < self.__ABS_ACC_THRESHOLD:
                                    abs_quit_counter += 1
                                    if abs_quit_counter > 4:
                                        print("Car stationary")
                                        break
                                else:
                                    abs_quit_counter = 0
                                real_throttle.value = self.__THROTTLE_CENTER - (self.__THROTTLE_CENTER - real_throttle.value) * self.__abs_k.value
                                self.__throttle_pwm.set_duty_cycle(real_throttle.value)
                    self.__brake_command.value = 0

        except KeyboardInterrupt:
            self.__throttle_pwm.set_duty_cycle(self.__THROTTLE_CENTER)
            print("Thread 3 exit")

    # Set steer PWM by reading the desired value (from remote controller or Jetson) and processing it
    def __setSteer(self, real_steer, direct_steer):
        try:
            print("STEER CONTROL OK")
            time.sleep(1)
            while True:
                if direct_steer:
                    self.__steer_reference.value = self.__steer_reference_arduino.value
                #print("steer reference: ", self.__throttle_reference.value)

                if math.fabs(self.__steer_reference.value - self.__STEER_CENTER) < self.__STEER_BOUNDARY:
                    real_steer.value = self.__steer_reference.value
                elif self.__steer_reference.value < self.__STEER_CENTER - self.__STEER_BOUNDARY:
                    real_steer.value = self.__STEER_CENTER - self.__STEER_BOUNDARY
                else:
                    real_steer.value = self.__STEER_CENTER + self.__STEER_BOUNDARY

                self.__steer_pwm.set_duty_cycle(real_steer.value)

        except KeyboardInterrupt:
            self.__steer_pwm.set_duty_cycle(self.__STEER_CENTER)
            print("Thread 4 exit")
        
if __name__ == "__main__":
    navio1 = Navio(
        arduino_port        = "/dev/serial/by-id/usb-Silicon_Labs_CP2104_USB_to_UART_Bridge_Controller_01B8FA0B-if00-port0",
        jetson_port         = "/dev/serial/by-id/usb-Silicon_Labs_CP2104_USB_to_UART_Bridge_Controller_01B8FB67-if00-port0",
        throttle_pwm_port   = 1,
        steer_pwm_port      = 0,
        pwm_freq            = 90,
        gps_buffer_size     = 11,
        arduino_buffer_size = 16,
        throttle_center     = 1.52,
        steer_center        = 1.42,
        throttle_boundary   = 0.15,
        steer_boundary      = 0.4,
        full_brake          = 0.4,
        fail_time           = 0.5,
        abs_ramp_time       = 0.02,
        abs_breaking_level  = 0.2,
        abs_acc_threshold   = 0.1,
        abs_rpm_threshold   = 0)

    navio1.start(imu=True, gps=True, arduino=True, send=True, pwm=True, direct_throttle=True, direct_steer=True, abs_on=False)