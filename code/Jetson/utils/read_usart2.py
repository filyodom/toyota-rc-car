#!/usr/bin/env python3

import sys
import time
import serial
import logging
import struct
import csv
import numpy as np
import RPi.GPIO as GPIO
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import message_data


# Pin Definitions
arduino_mega_reset_pin = 29  # arduino MEGA reset
arduino_nano_reset_pin = 31  # arduino NANO reset

# Constants
MESSAGE_SIZE_BYTES = message_data.DATA_SIZE  # Message data size in bytes (w/o start bytes)
MESSAGE_START_FRAME = message_data.DATA_START_FRAME

# Data types definition
DATA_STRUCT = message_data.DATA_STRUCT  # Data types list compatible with struct.unpack
DATA_NAMES = message_data.DATA_NAMES

# Logging
logger = logging.getLogger('usart_logger')
logger.setLevel(logging.DEBUG)

sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.INFO)

fh = logging.FileHandler('usart.log')
fh.setLevel(logging.DEBUG)

file_formatter = logging.Formatter(
    '[%(asctime)s - -%(name)s - %(levelname)s] %(message)s')
fh.setFormatter(file_formatter)
prompt_formatter = logging.Formatter('[%(levelname)s] %(message)s')
sh.setFormatter(prompt_formatter)

logger.addHandler(fh)
logger.addHandler(sh)


def main_loop():
    """
    """
    msg_cntr = 0
    logger.info("Reading from serial started.")

    baud_timer = time.time()

    size = 100
    x_vec = np.linspace(0, 1, size+1)[0:-1]
    y_vec = np.random.randn(len(x_vec))
    line1 = []

    disp_counter = 0
    average_disp = 0
    # waiting counter adds up if no data is being recieved, working as a safe stop in case of arduino failure
    while True:
        # Read serial, if connected
        if serial.isOpen():
            if not serial.in_waiting:
                continue
            end = 0
            byte = None
            last_byte = None
            reading = b''
            while not end:
                start_timer = time.time()                       # check if the message reading doesnt take too long, in that case break
                byte = serial.read(1)
                if [last_byte, byte] == MESSAGE_START_FRAME:
                    timestamp = time.time()
                    reading = serial.read(MESSAGE_SIZE_BYTES)
                    end = 1

                last_byte = byte
                end_timer = time.time()
                if end_timer - start_timer > 1:
                    print("Delay between recieved bytes too long")
                    break
            # reading = serial.read_until(terminator='\r', size = MESSAGE_SIZE_BYTES)
        else:
            logger.warning("Serial port disconnected!")
            break

        if len(reading) < MESSAGE_SIZE_BYTES:
            print("No or corrupted data recieved. "
                         "Data length is {}".format(len(reading)))
            continue

        msg_cntr += len(reading)

        disp_data, display_title = process_message(reading, timestamp)

        # update the live plot every X cycles (high refresh rate slows down samples/s)
        disp_counter += 1
        if disp_counter == 50:
            average_disp = average_disp / disp_counter
            y_vec[-1] = average_disp
            line1 = live_plotter(x_vec, y_vec, line1, display_title)
            y_vec = np.append(y_vec[1:], 0.0)
            disp_counter = 0
            average_disp = 0
        else:
            average_disp += disp_data
        
        # Count FPS
        if (time.time() - baud_timer) > 1:
            baud_timer = time.time()
            logger.info(f"Current baudrate: {msg_cntr*8} ({msg_cntr*8/MESSAGE_SIZE_BYTES} samples/s)")
            msg_cntr = 0
        

def process_message(msg, timestamp):
    """ Decodes message and saves values to file.
    """
    logger.debug("Data recieved:\t {}".format(''.join(r'\x'+hex(letter)[2:] for letter in msg)))
    fmt = DATA_STRUCT
    data = struct.unpack(fmt, msg)
    logger.debug("Data unpacked:\t {}".format(data))
    with open('data/meas_data.txt', 'a') as data_file:
        writer = csv.writer(data_file, delimiter=',')
        templist = list(data)
        templist.insert(0, timestamp)

        writer.writerow(templist)

    display_output = 12                 # index of output list
    return templist[display_output], display_output


# use ggplot style for more sophisticated visuals
plt.style.use('ggplot')


def live_plotter(x_vec, y1_data, line1, identifier='XX', pause_time=0.01):
    if line1 == []:
        # this is the call to matplotlib that allows dynamic plotting
        plt.ion()
        fig = plt.figure(figsize=(13, 6))
        ax = fig.add_subplot(111)
        # create a variable for the line so we can later update it
        line1, = ax.plot(x_vec, y1_data, '-o', alpha=0.8)        
        # update plot label/title
        plt.ylabel('Y Label')
        plt.title('Title: {}'.format(identifier))
        plt.show()
    25
    # after the figure, axis, and line are created, we only need to update the y-data
    line1.set_ydata(y1_data)
    # adjust limits if new data goes beyond bounds
    if np.min(y1_data) <= line1.axes.get_ylim()[0] or np.max(y1_data) >= line1.axes.get_ylim()[1]:
        plt.ylim([np.min(y1_data)-np.std(y1_data), np.max(y1_data)+np.std(y1_data)])
    # this pauses the data so the figure/axis can catch up - the amount of pause can be altered above
    plt.pause(pause_time)
    
    # return line so we can update it again in the next iteration
    return line1
 

def arduino_reset(reset_pin):
        GPIO.setmode(GPIO.BOARD)
        # set pin as an output pin with initial state of LOW -> HIGH resets the arduino
        GPIO.setup(reset_pin, GPIO.OUT, initial=GPIO.LOW)
        GPIO.output(reset_pin, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(reset_pin, GPIO.LOW)
        GPIO.cleanup()
        print("Arduino restarted")


if __name__ == "__main__":
    serial_port = "/dev/ttyTHS1"
    baudrate = 1000000
    parity = serial.PARITY_EVEN
    stopbits = 2

    # Try to open serial
    try:
        serial = serial.Serial(
            port=serial_port,
            baudrate=baudrate,
            parity=parity,
            stopbits=stopbits,
            timeout=0.5)
        time.sleep(0.1)
    except:
        logger.warning("Cannot connect to serial '{}'".format(serial_port))
        logger.info("Reading and synchronization stopped.")
        sys.exit()

    # Write first row of data file
    with open('data/meas_data.txt', 'w') as data_file:
        data_file.write("Time,{}\n".format(','.join(DATA_NAMES)))

    try:
        main_loop()
        arduino_reset(arduino_mega_reset_pin)

    except KeyboardInterrupt:
        if serial.isOpen():
            serial.close()
            GPIO.cleanup()
        logger.info("Keyboard interrupt, reading stopped.")
    
