import sys
import pyzed.sl as sl
from time import sleep


def main():

    cam = sl.Camera()
    init = sl.InitParameters()
    init.depth_mode = sl.DEPTH_MODE.DEPTH_MODE_ULTRA
    init.coordinate_units = sl.UNIT.UNIT_METER
    init.coordinate_system = sl.COORDINATE_SYSTEM.COORDINATE_SYSTEM_RIGHT_HANDED_Z_UP
    


    cam.open(init)
   
    runtime = sl.RuntimeParameters()
    runtime.sensing_mode = sl.SENSING_MODE.SENSING_MODE_STANDARD
    runtime.measure3D_reference_frame = sl.REFERENCE_FRAME.REFERENCE_FRAME_WORLD
    spatial = sl.SpatialMappingParameters()

    transform = sl.Transform()
    
    tracking = sl.TrackingParameters(transform)
    cam.enable_tracking(tracking)

    pymesh = sl.Mesh()
    pyplane = sl.Plane()
    reset_tracking_floor_frame = sl.Transform()
    found = 0
    print("Processing...")
    i = 0


    while i < 1000:
        if cam.grab(runtime) == sl.ERROR_CODE.SUCCESS :
            err = cam.find_floor_plane(pyplane, reset_tracking_floor_frame)
            
            if i > 700 and err == sl.ERROR_CODE.SUCCESS:
                found = 1
                print('Floor found!')

               
          
                Normal_Vector = pyplane.get_normal()
                print('Normal Vector')
                print(Normal_Vector)
            
                break


            i+=1

    
    cam.disable_tracking()
    
    cam.close()
    print("\nFINISH")


if __name__ == "__main__":
    main()
