#!/usr/bin/env python3

import os
import sys
import csv
import logging

import message_data

# Logging
logger = logging.getLogger('sync_logger')
logger.setLevel(logging.DEBUG)

sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.DEBUG)

fh = logging.FileHandler('sync.log')
fh.setLevel(logging.DEBUG)

file_formatter = logging.Formatter(
    '[%(asctime)s - -%(name)s - %(levelname)s] %(message)s')
fh.setFormatter(file_formatter)
prompt_formatter = logging.Formatter('[%(levelname)s] %(message)s')
sh.setFormatter(prompt_formatter)

logger.addHandler(fh)
logger.addHandler(sh)


class DataSynchronizer():
    """ Data synchronizer class providing synchronization of image data and
    measurements.

        Data:
            Data are expected to be in specific format. Measurement (telemetry)
            data are to be stored in .csv file in format 'time_stamp_s, (data1,
            data2,...)'.
            Images data are expected to be stored in csv. file in format
            'grab_time_stamp, save_time_stamp'.
            Both timestamps shall be consistent with same reference.

        Synchronization:
            Synchronization is done by simple algorithm:
                1) Choose image to synchronize and estimate its time of taking
                2) Iterate through measurements (with adjusted timestamp) and
                   find closest measurement
                3) Create vector of image timestamp (estimated time of
                   exposition), measured data, image with estemated time sync.
                   differce and estimated time since last image
                4) Repeat from step 1) for next image
            Synchronization quality is highly dependent on parameters, which
            should partially eliminate some inprecisions (communication delays,
            exposition, time, ...).

        Synchronization parameters:
            epsilon_t: ... [s]
            meas_data_offset: ... [s]
            image_data_offset: ... [s]
    """
    def __init__(self, meas_data_file, image_data_file, params=None):
        """ Initializes data synchronizer.

            Args:
                meas_data_file: path to measurement data file (.csv format
                expected)
                image_dir: path to directory with images
                params: synchronization parameters (optional)
        """
        # Initialize synchronization parameters
        self._epsilon_t = 0.02
        self._meas_data_offset = 0
        self._image_data_offset = 0

        self._sync_data = []
        self.synchronized = False

        # Initialize variables for statistics
        self._num_img_synced = 0
        self._num_img_unsynced = 0
        self._sum_time_diffs = 0
        self._abs_sum_time_diffs = 0
        self._max_tdiff = 0  # Absolut value
        self._min_tdiff = 999  # Absolut value

        if os.path.exists(meas_data_file):
            self._meas_data_file = meas_data_file
        else:
            raise ValueError(f"{meas_data_file}: Not a file!")

        if os.path.exists(image_data_file):
            self._image_data_file = image_data_file
        else:
            raise ValueError(f"{image_data_file}: Not a file!")

        self.set_params(params)

    def set_params(self, params):
        """ Set parameters of a synchronizer.
        """
        if params:
            for key, value in params.items():
                if "epsilon_t" == key:
                    self._epsilon_t = value
                if "meas_data_offset" == key:
                    self._meas_data_offset = value
                if "image_data_offset" == key:
                    self._image_data_offset = value

    def start_sync(self):
        """ Starts synchronization of all data.
        """
        with open(self._meas_data_file, newline='\n') as meas_csv, \
                open(self._image_data_file, newline='\n') as image_csv:

            # Skip first line - header
            meas_csv.readline()
            image_csv.readline()

            meas_reader = csv.reader(
                meas_csv, delimiter=',', skipinitialspace=True)
            image_reader = csv.reader(
                image_csv, delimiter=',', skipinitialspace=True)

            for image in image_reader:
                self._sync_image(image, meas_reader)

            self.synchronized = True

    def _sync_image(self, image, meas_reader):
        """ Synchronize image with measurement.
        """

        if self.synchronized:
            return False

        time_diff = 999
        end = False
        synced = False

        [grab_timestamp, save_timestamp] = image

        img_timestamp = self._estimate_img_timestamp(image)

        while not end:

            try:
                meas = next(meas_reader)
            except StopIteration:
                end = True
                continue

            meas_timestamp = self._estimate_meas_timestamp(meas)

            last_time_diff = time_diff
            time_diff = img_timestamp - meas_timestamp

            # If measurement time is sufficiently close to image exposition
            # time, consider them to be synced
            if abs(time_diff) < self._epsilon_t:

                synced_data = (
                    grab_timestamp,
                    img_timestamp,
                    time_diff,
                    *meas[1:])

                synced = True

            # When time difference starts to grow, stop
            if abs(time_diff) > abs(last_time_diff):
                end = True

        if synced:
            self._sync_data.append(synced_data)
            self._num_img_synced += 1
            self._abs_sum_time_diffs += abs(time_diff)
            self._sum_time_diffs += time_diff
            self._eval_stat_tdiff_min_max(time_diff)
            # logger.info(f"Synchronized data: {synced_data}")
        else:
            self._num_img_unsynced += 1
            logger.info(
                f"Image with timestamp {grab_timestamp} not "
                f"synchronized. Time difference: {time_diff} s")

        return synced

    def _eval_stat_tdiff_min_max(self, tdiff):
        """ Evaluate, if time difference value is minimal or maximal.
            If so, write it.
        """
        tdiff = abs(tdiff)
        if tdiff > self._max_tdiff:
            self._max_tdiff = tdiff
        if tdiff < self._min_tdiff:
            self._min_tdiff = tdiff

    def _estimate_img_timestamp(self, image):
        """ Estimate image exposition timestamp.

            TODO: describe!!!
        """
        estimated_timestamp = float(image[1]) + self._image_data_offset

        return estimated_timestamp

    def _estimate_meas_timestamp(self, meas):
        """ Estimate measurement timestamp.
        """
        estimated_timestamp = float(meas[0]) + self._meas_data_offset

        return estimated_timestamp

    def get_synchronized_data(self):
        """ If available, get synchronized data.
        """
        if self.synchronized:
            return self._sync_data
        else:
            return None

    def save_data_to_csv(self, file=None):
        """ Save sync data to csv.
        """
        if self.synchronized:
            with open('output.csv', 'w', newline='') as f:
                writer = csv.writer(f, delimiter=',')
                header = (
                    "grab_timestamp",
                    "estim_timestamp",
                    "time_diff",
                    *message_data.DATA_NAMES)
                writer.writerow(header)
                writer.writerows(self._sync_data)

    def print_statistics(self):
        """ Print statistical information about synchronization.
        """
        statistics = "Synchronization statistics\n"

        num_images = self._num_img_synced + self._num_img_unsynced
        statistics += "{:<40}".format("\t- Total number of images:")
        statistics += f"{num_images}\n"

        dropped_imgs = self._num_img_unsynced
        statistics += "{:<40}".format("\t- Number of unsynchronized images:")
        statistics += f"{dropped_imgs}\n"

        avg_time_diff = self._abs_sum_time_diffs / self._num_img_synced
        statistics += "{:<40}".format("\t- Avarage absolute time difference:")
        statistics += f"{avg_time_diff} s\n"

        statistics += "{:<40}".format("\t- Min time difference:")
        statistics += f"{self._min_tdiff} s\n"
        statistics += "{:<40}".format("\t- Max time difference:")
        statistics += f"{self._max_tdiff} s\n"

        logger.info(statistics)

        print("Mean time diff: ", self._sum_time_diffs/self._num_img_synced)


if __name__ == "__main__":

    # Obtain parameters
    if len(sys.argv) != 3:
        logger.info(
            "Incorrect number of arguments!"
            "Expected: path_to_meas_data path_to_image_data")
        sys.exit()
    logger.info("Synchronization from command line started.")

    synchronizer = DataSynchronizer(sys.argv[1], sys.argv[2])

    # Set parameters and start synchronization
    params = None
    synchronizer.set_params(params)
    synchronizer.start_sync()
    logger.info("Synchronization finished.")

    # Save data to file, print statistics and exit
    synchronizer.save_data_to_csv()
    logger.info("Data saved to file.")
    synchronizer.print_statistics()
    sys.exit()
