#!/usr/bin/env python3

import sys
import time
import serial
import logging
import struct
import csv

import message_data

# Constants
MESSAGE_SIZE_BYTES = message_data.DATA_SIZE  # Message data size in bytes (w/o start bytes)
MESSAGE_START_FRAME = message_data.DATA_START_FRAME

# Data types definition
DATA_STRUCT = message_data.DATA_STRUCT  # Data types list compatible with struct.unpack
DATA_NAMES = message_data.DATA_NAMES

# Logging
logger = logging.getLogger('usart_logger')
logger.setLevel(logging.DEBUG)

sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.INFO)

fh = logging.FileHandler('usart.log')
fh.setLevel(logging.DEBUG)

file_formatter = logging.Formatter(
    '[%(asctime)s - -%(name)s - %(levelname)s] %(message)s')
fh.setFormatter(file_formatter)
prompt_formatter = logging.Formatter('[%(levelname)s] %(message)s')
sh.setFormatter(prompt_formatter)

logger.addHandler(fh)
logger.addHandler(sh)


def main_loop():
    """
    """
    msg_cntr = 0
    logger.info("Reading from serial started.")

    baud_timer = time.time()

    while True:
        # Read serial, if connected
        if serial.isOpen():
            if not serial.in_waiting:
                continue
            end = 0
            byte = None
            last_byte = None
            reading = b''
            while not end:
                byte = serial.read(1)
                if [last_byte, byte] == MESSAGE_START_FRAME:
                    timestamp = time.time()
                    reading = serial.read(MESSAGE_SIZE_BYTES)
                    end = 1

                last_byte = byte

            # reading = serial.read_until(terminator='\r', size=MESSAGE_SIZE_BYTES)
        else:
            logger.warning("Serial port disconnected!")
            break

        if len(reading) < MESSAGE_SIZE_BYTES:
            logging.info("No or corrupted data recieved. "
                         "Data length is {}".format(len(reading)))
            continue

        msg_cntr += len(reading)

        process_message(reading, timestamp)

        # Count FPS
        if (time.time() - baud_timer) > 1:
            baud_timer = time.time()
            logger.info(f"Current baudrate: {msg_cntr*8} ({msg_cntr*8/72} samples/s)")
            msg_cntr = 0


def process_message(msg, timestamp):
    """ Decodes message and saves values to file.
    """
    logger.debug("Data recieved:\t {}".format(''.join(r'\x'+hex(letter)[2:] for letter in msg)))
    fmt = DATA_STRUCT
    data = struct.unpack(fmt, msg)
    logger.debug("Data unpacked:\t {}".format(data))
    with open('data/meas_data.txt', 'a') as data_file:
        writer = csv.writer(data_file, delimiter=',')
        templist = list(data)
        templist.insert(0, timestamp)
        writer.writerow(templist)


if __name__ == "__main__":

    serial_port = "/dev/ttyTHS1"
    baudrate = 1000000
    parity = serial.PARITY_EVEN
    stopbits = 2

    # Try to open serial
    try:
        serial = serial.Serial(
            port=serial_port,
            baudrate=baudrate,
            parity=parity,
            stopbits=stopbits,
            timeout=0.5)
        time.sleep(0.1)
    except:
        logger.warning("Cannot connect to serial '{}'".format(serial_port))
        logger.info("Reading and synchronization stopped.")
        sys.exit()

    # Write first row of data file
    with open('data/meas_data.txt', 'w') as data_file:
        data_file.write("Time,{}\n".format(','.join(DATA_NAMES)))

    try:
        main_loop()
    except KeyboardInterrupt:
        if serial.isOpen():
            serial.close()
        logger.info("Keyboard interrupt, reading stopped.")
