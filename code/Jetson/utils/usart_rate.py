import sys
import serial
import time


if __name__ == "__main__":

    if len(sys.argv) == 3:
        com_port = sys.argv[1]
        speed = sys.argv[2]
    else:
        com_port = 'COM3'
        speed = 115200

    ser = serial.Serial(com_port, speed, timeout=0.5)

    cntr = 0

    time1 = time.time()
    while(True):
        line = ser.read_until(terminator='\n', size=13)
        cntr += 1

        if (time.time() - time1) > 1:
            time1 = time.time()
            print(f"{cntr} samples/sec")
            cntr = 0
