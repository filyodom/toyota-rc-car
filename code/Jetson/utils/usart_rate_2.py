import sys
import serial
import time

import message_data


if __name__ == "__main__":

    if len(sys.argv) == 3:
        com_port = sys.argv[1]
        speed = sys.argv[2]
    else:
        com_port = '/dev/ttyTHS1'
        speed = 1000000

    ser = serial.Serial(
        com_port,
        speed,
        timeout=0.5,
        stopbits=2,
        parity=serial.PARITY_EVEN)

    cntr = 0

    SAMPLE_SIZE = message_data.DATA_SIZE

    time1 = time.time()
    while(True):
        line = ser.read(size=SAMPLE_SIZE)
        size = len(line)
        cntr += size

        if (time.time() - time1) > 1:
            time1 = time.time()
            print(f"{cntr} bytes/sec ({cntr*8} baud), {cntr*8/SAMPLE_SIZE} samples/s")
            cntr = 0
