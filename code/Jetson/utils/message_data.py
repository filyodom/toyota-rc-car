#!/usr/bin/env python3
""" Message data.
"""

import struct

DATA_START_FRAME = [b'\xff', b'\xff']
DATA_ENDIANESS = '>'

DATA = {
    "ax": 'h',
    "ay": 'h',
    "az": 'h',
    "gx": 'h',
    "gy": 'h',
    "gz": 'h',
    "steer_pwm": 'H',
    "throttle_pwm": 'H',
    "avg_motor_current": 'f',
    "avg_input_current": 'f',
    "duty_cycle": 'f',
    "rmp": 'i',
    "in_voltage": 'f',
    "amphours": 'f',
    "amphours_charged": 'f',
    "tachometer": 'i',
    "tachometer_abs": 'i',
    "gps_latit": 'f',
    "gps_long": 'f',
    "gps_course": 'f',
    "gps_speed": 'f'
    }


DATA_NAMES = DATA.keys()
DATA_STRUCT = DATA_ENDIANESS + "".join([DATA[var] for var in DATA_NAMES])

DATA_SIZE = struct.Struct(DATA_STRUCT).size

