
import serial
import struct
import time
import RPi.GPIO as GPIO

# Pin Definitions
arduino_mega_reset_pin = 29  # arduino MEGA reset
arduino_nano_reset_pin = 31  # arduino NANO reset


def arduino_reset(reset_pin):
        GPIO.setmode(GPIO.BOARD)
        # set pin as an output pin with initial state of LOW -> HIGH resets the arduino
        GPIO.setup(reset_pin, GPIO.OUT, initial=GPIO.LOW)
        GPIO.output(reset_pin, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(reset_pin, GPIO.LOW)
        GPIO.cleanup()
        print("Arduino restarted")


# seconds, %, %
def ride(ride_time, speed, acceleration=50):           
    if speed > 100 or acceleration > 100:
        print("Speed or acceleration surpassed 100%!")
        return None
    arduino.write(struct.pack('>B', 21))            # numerical code for arduino to distinquish ride and steering inputs
    time.sleep(0.01)
    arduino.write(struct.pack('>BBB', ride_time, speed, acceleration))
    i = 0
    info = []
    while i < 3:
        cc = str(arduino.readline())
        info.append(cc[2:][:-5])
        i += 1
    print("Riding with ride_time, speed, accel:", info)           


def steer(steer_input):
    if steer_input < 71 or steer_input > 107:
        print("Steer input out of bounds!")
        return None
    arduino.write(struct.pack('>B', 22))
    time.sleep(0.01)
    arduino.write(struct.pack('>B', steer_input))
    cc = str(arduino.readline())
    cc = cc[2:][:-5]
    print("Steering with input:", cc)
    while 1:    
        cc = str(arduino.readline())                # wait for arduino confirmation on finished task
        if cc[2:][:-5] == "Done":
            print("Steer done")
            break


def car_stop():
    arduino.write(struct.pack('>B', 23))            # code for car stop
    while 1:    
        cc = str(arduino.readline())                # wait for arduino confirmnation on finished task
        if cc[2:][:-5] == "Done":
            print("Stop initiated")
            break


if __name__ == "__main__":
    nano_port = "/dev/ttyUSB0"
    try:    
        arduino = serial.Serial(nano_port, 115200)
        time.sleep(2)
        print("Connection to " + nano_port + " established succesfully!\n")
    except Exception as e:
        print(e)

    try:
        steer(90)
        ride(1, 80, 80)
        time.sleep(5)
        car_stop()
        arduino.close() 

    except KeyboardInterrupt:
        if arduino.isOpen():
            arduino.close()
            GPIO.cleanup()
        print("Keyboard interrupt!")