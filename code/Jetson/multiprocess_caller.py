#!/usr/bin/env python3

""" This script calls two scripts as separate processes and creates shared
resources for forwarding data.
"""

import multiprocessing as mp
import time

import raspberry_control
import zed_data_and_imgs


def main():

    abs_k = mp.Value('f')
    throttle_SP = mp.Value('i', 1519)
    steer_SP = mp.Value('i', 1419)
    # TODO: create structure for sharing all data from Raspberry

    # Acces value with construction:
    # > with abs_k.get_lock():
    # >     abs_k.value = new_value

    autopilot = True

    rpi_process = mp.Process(
        target=raspberry_control.main,
        args=(abs_k, throttle_SP, steer_SP, autopilot)
        )

    depth = False
    jets_process = mp.Process(
        target=zed_data_and_imgs.main,
        args=(abs_k, depth, throttle_SP, steer_SP))

    rpi_process.start()
    time.sleep(5)
    jets_process.start()

    jets_process.join()
    rpi_process.join()

    return True


if __name__ == "__main__":
    main()
