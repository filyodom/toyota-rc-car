import sys
import csv
import matplotlib.pyplot as plt
import numpy as np

data = []
with open(str(sys.argv[1]), newline='') as inputfile:
    for row in csv.reader(inputfile):
        data.append(row)

data_np = np.array(data)
data_np = np.delete(data_np, (0), axis=0) # delete the first row
data_np = data_np.astype(np.float64) # convert to float array
data_np[:,0] = (data_np[:,0] - data_np[0,0]) * 1000 # convert to relative timestamps and to milis
data_np[:,31] = (data_np[:,31] - 1520) * 30

plt.ylim(-10, 5000)
plt.plot(data_np[:,0], data_np[:,32])
plt.plot(data_np[:,0], data_np[:,31])
plt.show()