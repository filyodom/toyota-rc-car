import cv2
import numpy as np
import sys
import os
from pathlib import Path
from PIL import Image
import numpy

# https://nikolasent.github.io/opencv/2017/05/07/Bird's-Eye-View-Transformation.html


def initParameters(img_shape, Minverse=False):
    percentalBorder = 0.35
    cut = round(img_shape[0]*0.417)
    IMAGE_H_crop = img_shape[0]-cut
    IMAGE_W = int(img_shape[1]/2)
    IMAGE_H_crop_deform = round(IMAGE_W*(3/2))

    p01 = [0.4485*IMAGE_W, 0.455*IMAGE_H_crop]
    p02 = [0.584*IMAGE_W, 0.455*IMAGE_H_crop]
    p03 = [0.195*IMAGE_W, 1*IMAGE_H_crop]
    p04 = [0.876*IMAGE_W, 0.99*IMAGE_H_crop]

    p11 = [IMAGE_W*percentalBorder, 0]
    p12 = [IMAGE_W-(IMAGE_W*percentalBorder), 0]
    p13 = [IMAGE_W*percentalBorder, IMAGE_H_crop_deform]
    p14 = [IMAGE_W-(IMAGE_W*percentalBorder), IMAGE_H_crop_deform]

    src = np.float32([p01, p02, p03, p04])
    dst = np.float32([p11, p12, p13, p14])
    if Minverse:
        M = cv2.getPerspectiveTransform(dst, src)
    else:
        M = cv2.getPerspectiveTransform(src, dst)
    return IMAGE_H_crop, IMAGE_H_crop_deform, IMAGE_W, cut, M


def transform(img, params=None):
    """ Transforms image to bird's eye perspective.

    Arguments:
        img: numpy array like object
    """
    # FIXME: initParameters shall not be called everytime transform is called
    if not params:
        IMAGE_H_crop, IMAGE_H_crop_deform, IMAGE_W, cut, M = initParameters(img.shape, False)
    else:
        try:
            IMAGE_H_crop, IMAGE_H_crop_deform, IMAGE_W, cut, M = params
        except TypeError:
            IMAGE_H_crop, IMAGE_H_crop_deform, IMAGE_W, cut, M = initParameters(img.shape, False)
    img = img[cut:(IMAGE_H_crop+cut), 0:IMAGE_W]
    warped_img = cv2.warpPerspective(img, M, (IMAGE_W, IMAGE_H_crop_deform))
    resizedImg = cv2.resize(warped_img, (640, 960))

    return resizedImg


if __name__ == "__main__":

    if len(sys.argv) == 2:

        target_path = Path(sys.argv[1])

        if os.path.exists(target_path):
            # Target is single file
            if os.path.isfile(target_path):
                files = [target_path]
                os.mkdir(target_path.parent / "bird_eye")
            # Target is directory of files
            if os.path.isdir(target_path):
                files = os.listdir(target_path)
                files = [target_path / f for f in files]
                os.mkdir(target_path / "bird_eye")

        init_params = None
        for file_ in files:
            try:
                img = Image.open(file_)
            except IOError:
                print(f"File {file_} cannot be read.")
            else:
                img = numpy.array(img)

                if not init_params:
                    img_shape = img.shape
                    init_params = initParameters(img_shape, False)

                t_img = transform(img)
                t_img = Image.fromarray(t_img)
                new_name = file_.parent / "bird_eye" / file_.name
                t_img.save(new_name)
                print(f"Image {file_.name} processed and saved.")

    sys.exit()
