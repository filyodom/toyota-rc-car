import pandas
import numpy as np
import sys
import os
import time
import pathlib

fn = pathlib.Path(__file__).parent / 'cam_imu_synchro.py'
path=str(fn.parent)+"/images/"
inputImageTimeStamps=os.listdir(path)
if not(os.path.isfile('zed_recording.csv') and os.path.isfile('meas_data.txt') and len(inputImageTimeStamps)>0):
    print("Import file error")
    exit(0)
zed_recording = pandas.read_csv('zed_recording.csv')
meas_data = pandas.read_csv('meas_data.txt')

idx=[0]*len(zed_recording.Timestamp)
for i in range(0, len(zed_recording.Timestamp)):
    idx[i]=abs(meas_data['Time']-zed_recording.Timestamp[i]).idxmin()

carIMUData=meas_data.iloc[idx,:]

separator = ','
leftHeader=separator.join(carIMUData.columns.values)
rightHeader=separator.join(zed_recording.columns.values)
outputHeader=leftHeader+','+rightHeader

leftPart=np.matrix(carIMUData.values)
rightPart=np.matrix(zed_recording.values)
output=np.hstack((leftPart,rightPart))

##pictures

for i in range(0, len(inputImageTimeStamps)):
    
    inputImageTimeStamps[i]=inputImageTimeStamps[i][4:]
    inputImageTimeStamps[i]=inputImageTimeStamps[i][:-4]

inputImageTimeStamps = [float(j) for j in inputImageTimeStamps] 
inputImageTimeStamps=np.array(inputImageTimeStamps)
idx2=[0]*len(zed_recording.Timestamp)
for i in range(0, len(zed_recording.Timestamp)):
    idx2[i]=np.argmin(abs(zed_recording.Timestamp[i]-inputImageTimeStamps))
print(idx2)
outputHeader=outputHeader+",ImageTimeStamp"
outputImageTimeStamps=[0.0]*len(zed_recording.Timestamp)
for i in range(0,len(outputImageTimeStamps)):
    outputImageTimeStamps[i]=inputImageTimeStamps[idx2[i]]
outputImageTimeStamps=np.matrix(outputImageTimeStamps).transpose()
output=np.hstack((output,outputImageTimeStamps))

##pictures
if len(sys.argv)==1:
    name="Merge_"+time.strftime("%m_%d_%Y-%H:%M:%S", time.gmtime())+".csv"
else: name=sys.argv[1]+".csv"
np.savetxt(name, output, delimiter=",",header=outputHeader,fmt='%f')
print("Merge_END")
    
