#!/usr/bin/env python3
""" Message data.
"""

import struct

DATA_START_FRAME = [b'\xaa', b'\xaa', b'\xaa', b'\xaa']
DATA_ENDIANESS = '<'

DATA = {
    # IMU (0-17)
    "ax1": 'f',
    "ay1": 'f',
    "az1": 'f',
    "ax2": 'f',
    "ay2": 'f',
    "az2": 'f',
    "gx1": 'f',
    "gy1": 'f',
    "gz1": 'f',
    "gx2": 'f',
    "gy2": 'f',
    "gz2": 'f',
    "magx1": 'f',
    "magy1": 'f',
    "magz1": 'f',
    "magx2": 'f',
    "magy2": 'f',
    "magz2": 'f',
    # GPS (18-28)
    "lon": 'i',
    "lat": 'i',
    "hAcc": 'i',
    "vAcc": 'i',
    "velN": 'i',
    "velE": 'i',
    "velD": 'i',
    "gSpeed": 'i',
    "heading": 'i',
    "sAcc": 'i',
    "headAcc": 'i',
    # PWM REFERENCES (29-30)
    "PWM_steer_ref": 'i',
    "PWM_throttle_ref": 'i',
    # RPM (31-32)
    "RPM-quick": 'i',
    "RPM-precise": 'i',
    # PWM ON MOTORS (33-34)
    "PWM_steer": 'i',
    "PWM_throttle": 'i',
    # ABS BRAKE FORCE (35)
    "abs_k": 'f',
    # PARAMETERS (36-44)
    "throttle center": 'f',
    "steer_center": 'f',
    "throttle boundary": 'f',
    "steer boundary": 'f',
    "full brake": 'f',
    "fail time": 'f',
    "abs_ramp_time": 'f',
    "abs_acc_threshold": 'f',
    "abs_rpm_threshold": 'f',
    # FLAGS (45)
    "FLAG": "B"
    }


DATA_NAMES = DATA.keys()
DATA_STRUCT = DATA_ENDIANESS + "".join([DATA[var] for var in DATA_NAMES])

DATA_SIZE = struct.Struct(DATA_STRUCT).size


"""    "AVG.motor.current": 'f',
    "AVG.input.current": 'f',
    "Duty_cycle": 'f',
    "RPM": 'i',
    "Input_voltage": 'f',
    "Amphours": 'f',
    "Amphours_charged": 'f',
    "Tachometer": 'i',
    "Tachometer_abs": 'i',
    "STEER_PWM": 'i',
    "THROTTLE_PWM": 'i' """
