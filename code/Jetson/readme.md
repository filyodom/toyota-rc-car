# Jetson data collection and synchronization

## Data collection
TODO: update this section

There are two scripts for data collection. They can run simultainously in two terminals.
```
sudo python3 read_usart.py
```
and
```
sudo python3 read_ZED.py
```
Important note: There have to exist two directories in same directory as scripts, *data* and *images*, otherwise srcipts edn up with error.

For examining usart speed, simple utility is prepared:
```
sudo python3 usart_rate.py
```

## Data synchronization
TODO: update this section

Data synchronization is done by another script. Two arguments with data files are required.
```
python3 synchronizer.py "data/meas_data.txt" "data/img_data.txt"
```

## FAQ and troubleshooting
### Cannot open arduino serial
Try to
* install Arduino IDE or
* grant permission by ``sudo chmod a+rw /dev/ttyACM0``

### USART is not working or is slow
* run script with `sudo`
* run `sudo python3 usart_rate.py` to test speed
* check set baudrate for Serial connection on both sides
* check ports ('/dev/ttyXXX')

### Cannot connect to ZED camera
* Run script as a root (`sudo python3 script.py`).
* Do not use hub nor extension cords (or try another one).

### Cannot reach desired FPS
In ZED SDK, you can find
`"NVIDIA Jetson X1 only supports RESOLUTION_HD1080@15, RESOLUTION_HD720@30/15, and RESOLUTION_VGA@60/30/15."`. Also check Jetson power settings and 'jetson_clock.sh'.

# Hardware

## Jetson Nano
### Power jack barrel
2.1x5.5mm

## Jetson Xavier
### Power jack barrel
2.1x5.5mm

# General
## FAQ
### Jetson suddenly shutted down.
Power supply probably provide not enough power.
### Jetson doesn't switch on.
* Check power supply. Not all power adaptors provide enough power, even if they say it.
* Check, if wire jack for choosing power source is plugged correctly.
### Jetson is turned on (LED is on), but screen is black.
Try to use DHMI instead of DiplayPort.