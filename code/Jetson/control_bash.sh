#!/bin/bash

saveDir=0
timeStamp=$(date "+-%Y_%m_%d-%H_%M_%S")

if [ -z $1 ]
then
	saveDir=$(date "+RCtest-%Y_%m_%d-%H_%M_%S")
else
	saveDir=$1
fi

trap 'cleanup' INT

cleanup()
{	

	echo "Bash Cleanup"
	if [ -d "/media/ctuxavier/ADATASE730H/AcquiredData/$saveDir" ]
	then
		saveDir=$saveDir$timeStamp
		mkdir -p "/media/ctuxavier/ADATASE730H/AcquiredData/$saveDir"		
	else
		mkdir -p "/media/ctuxavier/ADATASE730H/AcquiredData/$saveDir"
	fi
	
	if [ -f "/media/ctuxavier/ADATASE730H/img_data.txt" ]; then
   		mv "/media/ctuxavier/ADATASE730H/img_data.txt" "/media/ctuxavier/ADATASE730H/AcquiredData/$saveDir/img_data.txt"
	fi
	if [ -f "/media/ctuxavier/ADATASE730H/meas_data.txt" ]; then
   		mv "/media/ctuxavier/ADATASE730H/meas_data.txt" "/media/ctuxavier/ADATASE730H/AcquiredData/$saveDir/meas_data.txt"
	fi
	if [ -f "/media/ctuxavier/ADATASE730H/zed_recording.csv" ]; then
   		mv "/media/ctuxavier/ADATASE730H/zed_recording.csv" "/media/ctuxavier/ADATASE730H/AcquiredData/$saveDir/zed_recording.csv"
	fi
	if [ -d "/media/ctuxavier/ADATASE730H/images" ]; then
   		mv "/media/ctuxavier/ADATASE730H/images" "/media/ctuxavier/ADATASE730H/AcquiredData/$saveDir/images"
	else
	    echo "Images not moved from /images"
	fi

	mkdir -p "/media/ctuxavier/ADATASE730H/images"
	echo "Bash Cleanup Completed"
	exit 0	
}

(python3 multiprocess_caller.py ; kill -INT $$)

echo "Bash started"

wait
