
import pyzed.sl as sl
import csv
import time


### Getting Rotatation and Translation Vector and save them to csv file ### 


def init_zed():


	init = sl.InitParameters(camera_resolution=sl.RESOLUTION.RESOLUTION_HD720,
                                 depth_mode=sl.DEPTH_MODE.DEPTH_MODE_PERFORMANCE,
                                 coordinate_units=sl.UNIT.UNIT_METER,
                                 coordinate_system=sl.COORDINATE_SYSTEM.COORDINATE_SYSTEM_RIGHT_HANDED_Z_UP,
                                 sdk_verbose=True)

	
	return init

	
def track():
    # Create a Camera object
    zed = sl.Camera()

    # Initilialize Camera Parameters
    init_params = init_zed()

    # Open the camera
    zed_cam = zed.open(init_params)
    
    if zed_cam != sl.ERROR_CODE.SUCCESS:
        print("Error: Zed Camera could not be opened.")
        exit()
    
    print("")    
    print("Zed Camera succesfully opened.")
    
    # Enable positional tracking 
    
    transform = sl.Transform()  
    tracking_parameters = sl.TrackingParameters(init_pos=transform)
    tracking = zed.enable_tracking(tracking_parameters)
    
    if tracking != sl.ERROR_CODE.SUCCESS:
        exit()
    
    print("")    
    print("Tracking succesfully enabled.")
  
    runtime_parameters = sl.RuntimeParameters()
    zed_pose = sl.Pose() # Getting Pose
    
    with open('/media/ctu/ADATA SE730H/zed_recording.csv', mode='w') as csv_file: # Open csv file
    
        print("")
        print("Start Recording")
        
        fieldnames = ['Tx', 'Ty', 'Tz', 'Pitch', 'Roll', 'Yaw', 'Timestamp']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        
        while True:

            if zed.grab(runtime_parameters) == sl.ERROR_CODE.SUCCESS:
                                                        
                zed.get_position(zed_pose, sl.REFERENCE_FRAME.REFERENCE_FRAME_WORLD) # Updating Pose
                
                # Translation
                translation = sl.Translation()
                tx = round(zed_pose.get_translation(translation).get()[0], 3)
                ty = round(zed_pose.get_translation(translation).get()[1], 3)
                tz = round(zed_pose.get_translation(translation).get()[2], 3)
                                               
                # Rotation
                rotation = zed_pose.get_rotation_vector()
                rx = round(rotation[0], 2)
                ry = round(rotation[1], 2)
                rz = round(rotation[2], 2)
                
                
                writer.writerow({'Tx': ty, 'Ty': tx, 'Tz': tz, 'Pitch': rx, 'Roll': ry, 'Yaw': rz, 'Timestamp': time.time()})
                
             

           
    # Close the camera
    #zed.close()

if __name__ == "__main__":
    
    track()
