# Static I.P.Address ReadMe File

## Setting static I.P.Address on Linux
Settings must be done on *Raspberry* and *Jetson* devices manually

0) Open config file for editting, i.e.
    ```
    sudo nano /etc/network/interfaces
    ```
1) For `Raspberry` add or change configuration of `eth0` to the following:
    ```
    auto eth0
    iface eth0 inet static
        address 192.168.0.10
        netmask 255.255.255.0
        gateway 192.168.0.11
        broadcast 192.168.0.255
    ```
2) For `Jetson` add or change configuration of `eth0` to the following:
    ```
    auto eth0
    iface eth0 inet static
        address 192.168.0.11
        netmask 255.255.255.0
        gateway 192.168.0.10
        broadcast 192.168.0.255
    ```
3) Save changes by pressing *ctrl+O* and exit the editor by pressing *ctrl+X*.

4) Use the next command or restart computer for configuration to take effect.
    ```
    sudo service networking restart
    ```

## FAQ

**Ethernet connection is unstable**
Make sure */etc/NetworkManager/NetworkManager.config* contains line `managed=true`
