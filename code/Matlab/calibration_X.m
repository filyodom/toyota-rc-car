function [Rx, tx] = calibration_X(tb, u, Ra_2d, ta_2d, tb_2d)


%%
% tb - translacni vector ktery dostaneme z kamery. 
% u - gravitacni vector z kamery
% test: Ujet urcitou primkou.
% R_a, ta 2d - rotacni/translacni matice auta.
% tb 2d - translacni matice camera
% test - ujet urcitou traektorii. ( napr. ujet zatacku 90 stupnu).
%
%%
Tb = tb/norm(tb);

vec = GramSchmidt([u.',Tb.']);

Rx = [vec(:,2), cross(vec(:,1),vec(:,2)), vec(:,1)];

Rx

tx = (inv(Ra_2d - eye(2)))*(Rx(1:2,1:2,1)*tb_2d.' - ta_2d.');


tx



end