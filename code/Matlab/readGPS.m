function [lon, lat, gSpeed, time] = readGPS(dat)

data = dat.data;
 
 
for i=1:length(data(:,1))
    

    lon(i,:) = data(i,20);

    lat(i,:) = data(i,21);
    
    gSpeed(i,:) =  data(i,27);

end


time = data(:,1) - data(1,1);

end