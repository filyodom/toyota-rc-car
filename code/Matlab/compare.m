clc;
clear all;
close all;

circle = importdata('circle/final.csv');

gps_1 = importdata('gps_test1/final.csv');

gps_2 = importdata('gps_test2/final.csv');

left = importdata('left/final.csv');

right = importdata('right/final.csv');

str_1 = importdata('straight1/final.csv');

str_2 = importdata('straight2/final.csv');

[b, a] = butter (1, 0.18, 'low');

%%
[acc_l, gyr_l, mag_l, time_l] = readIMU(left,150);
[lon_l, lat_l, speed_l] = readGPS(left);
gyrP_l = zeros(length(gyr_l(:,1)),3);
[T_l, R_l] = readCAM(left);
gyr_l = filter(b,a,gyr_l);

for i = 2:length(gyr_l(:,3))
    gyrP_l(i,:) = gyrP_l(i-1,:) + gyr_l(i,:) * (time_l(i)-time_l(i-1));
   
end

figure
subplot(2,1,1)
hold on;
grid;
plot(gyrP_l(:,1),'r')
plot(gyrP_l(:,2),'g')
plot(gyrP_l(:,3),'b')
legend('X', 'Y', 'Z');
xlabel('Sample');
ylabel('Degrees (o)');
title('Orientation GYRO');
subplot(2,1,2)
hold on;
grid;
plot(R_l(:,1), 'r')
plot(R_l(:,2), 'g')
plot(R_l(:,3), 'b')
legend('X', 'Y', 'Z');
xlabel('Sample');
ylabel('Degrees (o)');
title('Orientation CAM');

figure
hold on;
grid;
plot(lon_l,lat_l);
plot(lon_l(1),lat_l(1),'r*')
plot(lon_l(length(lon_l)),lat_l(length(lon_l)),'b*')
xlabel('Longtitude');
ylabel('Latitude (o)');
title('GPS position');

figure
hold on;
grid;
plot(T_l(:,1),T_l(:,2));
plot(T_l(1,1),T_l(1,2),'r*')
plot(T_l(length(T_l(:,1)),1),T_l(length(T_l(:,2)),2),'b*')
xlabel('X (m)');
ylabel('Y (m)');
title('Camera');


%%


[acc_r, gyr_r, mag_r, time_r] = readIMU(right,31);
[lon_r, lat_r, speed_r] = readGPS(right);
gyrP_r = zeros(length(gyr_r(:,1)),3);
[T_r, R_r] = readCAM(right);
gyr_r = filter(b,a,gyr_r);


for i = 2:length(gyr_r(:,3))
    gyrP_r(i,:) = gyrP_r(i-1,:) + gyr_r(i,:) * (time_r(i)-time_r(i-1));
   
end

figure
subplot(2,1,1)
hold on;
grid;
plot(gyrP_r(:,1),'r')
plot(gyrP_r(:,2),'g')
plot(gyrP_r(:,3),'b')
legend('X', 'Y', 'Z');
xlabel('Sample');
ylabel('Degrees (o)');
title('Orientation GYRO');
subplot(2,1,2)
hold on;
grid;
plot(R_r(:,1), 'r')
plot(R_r(:,2), 'g')
plot(R_r(:,3), 'b')
legend('X', 'Y', 'Z');
xlabel('Sample');
ylabel('Degrees (o)');
title('Orientation CAM');

figure
hold on;
grid;
plot(lon_r,lat_r);
plot(lon_r(1),lat_r(1),'r*')
plot(lon_r(length(lon_r)),lat_r(length(lon_r)),'b*')
xlabel('Longtitude');
ylabel('Latitude (o)');
title('GPS position');



figure
hold on;
grid;
plot(T_r(:,1),T_r(:,2));
plot(T_r(1,1),T_r(1,2),'r*')
plot(T_r(length(T_r(:,1)),1),T_r(length(T_r(:,2)),2),'b*')
xlabel('X (m)');
ylabel('Y (m)');
title('Camera');
%%

[acc_c, gyr_c, mag_c, time_c] = readIMU(circle,31);
[lon_c, lat_c, speed_c] = readGPS(circle);
gyrP_c = zeros(length(gyr_c(:,1)),3);
[T_c, R_c] = readCAM(circle);
gyr_c = filter(b,a,gyr_c);


for i = 2:length(gyr_c(:,3))
    gyrP_c(i,:) = gyrP_c(i-1,:) + gyr_c(i,:) * (time_c(i)-time_c(i-1));
   
end

figure
subplot(2,1,1)
hold on;
grid;
plot(gyrP_c(:,1),'r')
plot(gyrP_c(:,2),'g')
plot(gyrP_c(:,3),'b')
legend('X', 'Y', 'Z');
xlabel('Sample');
ylabel('Degrees (o)');
title('Orientation GYRO');
subplot(2,1,2)
hold on;
grid;
plot(R_c(:,1), 'r')
plot(R_c(:,2), 'g')
plot(R_c(:,3), 'b')
legend('X', 'Y', 'Z');
xlabel('Sample');
ylabel('Degrees (o)');
title('Orientation CAM');

figure
hold on;
grid;
plot(lon_c,lat_c);
plot(lon_c(1),lat_c(1),'r*')
plot(lon_c(length(lon_c)),lat_c(length(lon_c)),'b*')
xlabel('Longtitude');
ylabel('Latitude (o)');
title('GPS position');


figure
hold on;
grid;
plot(T_c(:,1),T_c(:,2));
plot(T_c(1,1),T_c(1,2),'r*')
plot(T_c(length(T_c(:,1)),1),T_c(length(T_c(:,2)),2),'b*')
xlabel('X (m)');
ylabel('Y (m)');
title('Camera');

%%

[acc_g1, gyr_g1, mag_g1, time_g1] = readIMU(gps_1,31);
[lon_g1, lat_g1, speed_g1] = readGPS(gps_1);
gyrP_g1 = zeros(length(gyr_g1(:,1)),3);
[T_g1, R_g1] = readCAM(gps_1);
gyr_g1 = filter(b,a,gyr_g1);


for i = 2:length(gyr_g1(:,3))
    gyrP_g1(i,:) = gyrP_g1(i-1,:) + gyr_g1(i,:) * (time_g1(i)-time_g1(i-1));
   
end

figure
subplot(2,1,1)
hold on;
grid;
plot(gyrP_g1(:,1),'r')
plot(gyrP_g1(:,2),'g')
plot(gyrP_g1(:,3),'b')
legend('X', 'Y', 'Z');
xlabel('Sample');
ylabel('Degrees (o)');
title('Orientation GYRO');
subplot(2,1,2)
hold on;
grid;
plot(R_g1(:,1), 'r')
plot(R_g1(:,2), 'g')
plot(R_g1(:,3), 'b')
legend('X', 'Y', 'Z');
xlabel('Sample');
ylabel('Degrees (o)');
title('Orientation CAM');

figure
hold on;
grid;
plot(lon_g1,lat_g1);
plot(lon_g1(1),lat_g1(1),'r*')
plot(lon_g1(length(lon_g1)),lat_g1(length(lon_g1)),'b*')
xlabel('Longtitude');
ylabel('Latitude (o)');
title('GPS position');


figure
hold on;
grid;
plot(T_g1(:,1),T_g1(:,2));
plot(T_g1(1,1),T_g1(1,2),'r*')
plot(T_g1(length(T_g1(:,1)),1),T_g1(length(T_g1(:,2)),2),'b*')
xlabel('X (m)');
ylabel('Y (m)');
title('Camera');
