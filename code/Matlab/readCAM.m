function [T, R] = readCAM(dat)
% read tanslation and rotation matrix from data



data = dat.data;
 
 
for i=1:length(data(:,1))
    

    T(i,1) = data(i,41);
    T(i,2) = data(i,42);
    T(i,3) = data(i,43);
    
    R(i,1) = data(i,44);
    R(i,2) = data(i,45);
    R(i,3) = data(i,46);

end

R = R * 180/pi;

end