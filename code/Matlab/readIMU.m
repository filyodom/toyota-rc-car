function [acc, gyr, mag, time] = readIMU(dat,i_st,i_im)

%% Function read IMU data from file dat
% i_st - number of stable samples
% i_im - if 1 read data from MPU9050.
% Output: acc in m/s/s without offsets.
% gyr in deg/s without offsets
% mag without offsets





data = dat.data;


for i=1:length(data(:,1))
    if i_im == 1
        acc(i,:) = [ data(i,2), ...
            data(i,3),  ...
            data(i,4)];
        
        gyr(i,:) = [ data(i,8), ...
            data(i,9), ...
            data(i,10)];
        mag(i,:) = [ data(i,14), ...
            data(i,15), ...
            data(i,16)];
    else
        acc(i,:) = [ data(i,5), ...
            data(i,6),  ...
            data(i,7)];
        
        gyr(i,:) = [ data(i,11), ...
            data(i,12), ...
            data(i,13)];
        mag(i,:) = [ data(i,17), ...
            data(i,18), ...
            data(i,19)];
        
    end
    
end
acc = acc - [mean(acc(1:i_st,1)), mean(acc(1:i_st,2)), mean(acc(1:i_st,3))];
gyr = gyr - [mean(gyr(1:i_st,1)), mean(gyr(1:i_st,2)), mean(gyr(1:i_st,3))];
gyr = gyr*180/pi;
time = data(:,1) - data(1,1);

end