#!/usr/bin/env python3

import sys
import os
from pathlib import Path

import cv2
import numpy as np
from matplotlib import pyplot as plt

""" This script converts annotation PNG output from PixelAnnotationTool to
project segmentation format.
"""


def tool_to_project(in_annot_fpath, out_annot_dpath, shades_dict=None):
    """ Converts annotation image from tool format to prohect format.

    All shades are found in image from tool and mapped directly or by
    provided dictionary.

    Arguments:
        in_annot_path: path to annotation tool output file
        out_annot_fpath: path to directory for converter output
        shades_dict: mapping dictionary from input shade to output shades
    """
    in_annot_fpath = Path(in_annot_fpath)
    print(in_annot_fpath)
    out_annot_dpath = Path(out_annot_dpath)
    print(out_annot_dpath)

    annot_img = cv2.imread(str(in_annot_fpath), cv2.IMREAD_GRAYSCALE)

    remove_border(annot_img)

    if annot_img is None:
        print(f"Cannot open file {in_annot_fpath}.")
        return False

    shades = get_shades(annot_img)

    if not shades_dict:
        print("No dictionary given.")
        shades_dict = dict(zip(shades, shades))

    for shade in shades:
        if shades_dict.get(shade) is None:
            print(f"Shade dictionary does not contain {shade}!")
            return False

    for shade in shades:
        annot_img[annot_img == shade] = shades_dict.get(shade)

    # plt.imshow(annot_img, 'gray')
    # plt.show()

    cv2.imwrite(str(out_annot_dpath / in_annot_fpath.name), annot_img)
    return True


def get_shades(img):
    """ Returns array of all shades of gray in image.
    """
    hist = cv2.calcHist([img], [0], None, [256], [0, 256])
    shades = np.nonzero(hist)[0]
    return shades


def remove_border(img):

    img[0, :] = img[1, :]
    img[-1, :] = img[-2, :]
    img[:, 0] = img[:, 1]
    img[:, -1] = img[:, -2]

    img[0, 0] = img[1, 1]
    img[0, -1] = img[1, -2]
    img[-1, 0] = img[-2, 1]
    img[-1, -1] = img[-2, -2]

    return img


if __name__ == "__main__":

    if len(sys.argv) != 3:
        print("Wrong number of arguments!")
        sys.exit()

    in_dir = Path(sys.argv[1])
    out_dir = sys.argv[2]

    shades_dict = {}
    for i in range(8):
        shades_dict[i+1] = i

    for f in os.listdir(in_dir):
        tool_to_project(str(in_dir / f), out_dir, shades_dict)
