from PIL import Image, ImageFilter
import matplotlib.pyplot as plt

image=Image.open("bird.JPG")
plt.figure()
plt.imshow(image)

im_sharp = image.filter( ImageFilter.SHARPEN )
plt.figure()
plt.imshow(im_sharp)
plt.show()