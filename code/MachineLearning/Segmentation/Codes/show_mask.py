#!/usr/bin/env python3
""" This script shows all validation images with its annotations and
segmentations, if available.
"""
import os
import sys
from sys import argv
import matplotlib.pyplot as plt
from pathlib import Path
from PIL import Image
import numpy as np
import mpldatacursor
from matplotlib.widgets import MultiCursor


if __name__ == "__main__":

    if len(argv) == 1:
        dataset_dpath = Path(".")
        print("Current directory chosen.")
    else:
        dataset_dpath = Path(argv[1])
        print(f"Dataset directory {dataset_dpath} chosen.")

    valid_images_dpath = dataset_dpath / "validation/images"
    valid_annots_dpath = dataset_dpath / "validation/annotations"
    valid_segm_dpath = dataset_dpath / "_segmentation/validation/masks"

    if not valid_images_dpath.exists():
        print(f"Path {valid_images_dpath} does not exist!")
        sys.exit()

    if not valid_annots_dpath.exists():
        print(f"Path {valid_annots_dpath} does not exist!")
        sys.exit()

    if not valid_segm_dpath.exists():
        print(f"Path {valid_segm_dpath} does not exist!")
        valid_segm_dpath = None

    for img_name in os.listdir(valid_images_dpath):

        id_name = Path(img_name).stem
        # _show_comparsion()

        image = Image.open(str(valid_images_dpath / img_name))
        img_arr = np.asarray(image, dtype=np.uint8)

        mask = Image.open(
            str(valid_annots_dpath / id_name) + ".png").convert("L")
        mask_arr = np.asarray(mask, dtype=np.uint8)

        if valid_segm_dpath:
            segm = Image.open(
                str(valid_segm_dpath / id_name) + ".png").convert("L")
            segm_arr = np.asarray(segm, dtype=np.uint8)

        fig = plt.figure()
        plt.title(img_name)
        mng = plt.get_current_fig_manager()
        mng.window.state('zoomed')

        if valid_segm_dpath:
            vmax = np.max([np.max(mask_arr), np.max(segm_arr)])
        else:
            vmax = np.max([np.max(mask_arr)])

        ax1 = fig.add_subplot(131)
        plt.imshow(img_arr)

        ax2 = fig.add_subplot(132, sharex=ax1, sharey=ax1)
        plt.imshow(mask_arr, cmap='gist_rainbow', vmin=1, vmax=vmax)

        ax3 = fig.add_subplot(133, sharex=ax1, sharey=ax1)
        if valid_segm_dpath:
            plt.imshow(segm_arr, cmap='gist_rainbow', vmin=1, vmax=vmax)

        mpldatacursor.datacursor(hover=True, bbox=dict(alpha=1, fc='w'),
                                 formatter='{z}'.format, display='multiple')

        multi = MultiCursor(
            fig.canvas, (ax1, ax2, ax3), color='r', lw=1, horizOn=True)

        if not plt.waitforbuttonpress(0):
            sys.exit(0)
        plt.close(fig)
