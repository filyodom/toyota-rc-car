#!/bin/bash -i
# Run this script with 'source keras_seg_init.sh'

module load 'scikit-learn' && echo sklearn loaded || echo Failed to load sklearn
module load 'Python' && echo Python loaded || echo Failed to load Python
module load 'OpenCV' && echo OpenCV loaded || echo Failed to load OpenCV
module load 'TensorFlow' && echo Tensorflow loaded || echo Failed to load Tensorflow
module load 'Keras' && echo Keras loaded || echo Failed to load Keras
module load 'h5py' && echo h5py loaded || echo Failed to load h5py
module load 'tqdm' && echo tqdm loaded || echo Failed to load tqdm
module load 'matplotlib' && echo matplotlib loaded || echo Failed to load matplotlib
module load 'Pillow' && echo Pillow loaded || echo Failed to load Pillow

if [ "$#" == 1 ]; then
	export CUDA_VISIBLE_DEVICES="$1"
    echo "Graphic card $1 selected."
else
	export CUDA_VISIBLE_DEVICES=0
    echo "Graphic card 0 selected."
fi
