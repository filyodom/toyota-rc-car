#!/usr/bin/env python3
""" This script performs data augmentation on images and annotation masks in
directories 'images' and annotations'.
"""

import os
from PIL import Image
from pathlib import Path
import numpy as np


if __name__ == "__main__":

    img_names = os.listdir("./images") 

    for img_name in img_names:

        img_file = Path("./images/") / img_name
        mask_file = Path("./annotations/") / Path(img_name).with_suffix(".png")

        if not img_file.exists() or not img_file.exists():
            print("Image or mask {img_name} does not exist.")

        # Flip image
        img = Image.open(img_file)
        img = np.array(img)
        flipped_img = np.fliplr(img)
        img = Image.fromarray(flipped_img)

        # Flip image
        img_mask = Image.open(mask_file)
        img_mask = np.array(img_mask)
        flipped_mask = np.fliplr(img_mask)
        img_mask = Image.fromarray(flipped_mask, 'L')

        img_file = str(img_file.with_suffix("")) + "_flipped" + img_file.suffix
        mask_file = str(mask_file.with_suffix("")) + "_flipped" + mask_file.suffix
        img.save(img_file)
        img_mask.save(mask_file, "PNG")
        print(img_file)
