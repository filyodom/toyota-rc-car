import os
import sys
import shutil
from pathlib import Path

import cv2
import keras_segmentation
import tensorflow as tf
import json
from keras import backend as K


class KerasTraining():

    _configurated = False

    _ML_ROOT_PATH = None
    _dataset_root_path = None
    _weights_path = None
    _training_id = None

    _load_weights_path = None

    _epochs_to_train = 0
    _use_gpu = True

    def __init__(self, config_file):
        print("Initialization")
        # Resolve MachineLearning folder path
        self._ML_ROOT_PATH = Path(__file__).resolve().parents[2]

        if not self.read_config(config_file):
            print("Invalid config file.")
            return None

        tf.logging.set_verbosity(tf.logging.ERROR)
        if self._use_gpu:
            # Use GPU for computing
            tf.Session(config=tf.ConfigProto(log_device_placement=True))
            print("Available GPUs: " +
                  str(K.tensorflow_backend._get_available_gpus()))
        else:
            print("Using CPU for computing")

        self.model = keras_segmentation.models.unet.vgg_unet(
            n_classes=6, input_height=768, input_width=384
            )

        print("Initialization completed")

    def read_config(self, config_file):
        """ Loads configuration data from .json file.
        """
        with open(config_file, "r") as config_file:
            config_data = json.load(config_file)
            print(config_data)

            if not config_data.get("dataset_path"):
                print("No path to dataset found in config.")
                return False
            if not config_data.get("weights_path"):
                print("No path for weights found in config.")
                return False

            for key, value in config_data.items():
                if "dataset_path" == key:
                    self._dataset_root_path = self._ML_ROOT_PATH / value
                elif "weights_path" == key:
                    self._weights_path = self._ML_ROOT_PATH / value
                elif "num_epochs" == key:
                    self._epochs_to_train = value
                elif "load_weights_path" == key:
                    self.load_weights_path = value
                else:
                    print(f"Entry {key} (value {value}) not recognized "
                          "as valid configuration data.")
            self._configurated = True
            return True
        return False

    def load_weights(self, weights):
        """ Load weights wrapper.
        """
        if weights is not None:
            self.model.load_weights(weights)

    def init_segmentation_folder_tree(self):
        segm_path = self._dataset_root_path / "_segmentation"
        if segm_path.is_dir():
            shutil.rmtree(segm_path)

            try:
                os.makedirs(segm_path / "training/masks")
                os.makedirs(segm_path / "training/masks_rgb")
                os.makedirs(segm_path / "validation/masks")
                os.makedirs(segm_path / "validation/masks_rgb")
            except OSError:
                print("Cannot make _segmentation folder tree.")

    def train(self,
              load_weights=None,
              resume=False,
              save_weights=True):
        """ Trains neural network.

        Args:
            num_epochs: Number of epoches to train.
        """
        if not self._configurated:
            print("Not configurated!")
            return False

        if not self._epochs_to_train:
            self._epochs_to_train = 1

        if resume:
            print("Continue training with loaded weights.")

        # TODO: Make validation elegent!

        if load_weights is not None:
            load_weights = str(load_weights)
        else:
            if self._load_weights_path is not None:
                load_weights = str(self._load_weights_path)
            else:
                load_weights = None

        train_imgs = str(self._dataset_root_path / "training/images")
        print((self._dataset_root_path / "training/images").resolve())
        train_annots = str(self._dataset_root_path / "training/annotations")
        val_images = str(self._dataset_root_path / "validation/images")
        val_annots = str(self._dataset_root_path / "validation/annotations")

        self.model.train(
                train_images=train_imgs,
                train_annotations=train_annots,
                checkpoints_path=None,  # FIXME
                batch_size=1,
                validate=True,
                val_images=val_images,
                val_annotations=val_annots,
                val_batch_size=1,
                epochs=self._epochs_to_train,
                auto_resume_checkpoint=resume,
                load_weights=load_weights
            )

        return True

    def predict(self, img_dpath=None, segm_dpath=None):
        """ Predicts segmentation map.
        """
        if not self._configurated:
            print("Not configurated!")
            return False

        self.init_segmentation_folder_tree()

        if not img_dpath or not segm_dpath:
            img_dpath = self._dataset_root_path / "validation/images"
            segm_dpath = self._dataset_root_path / "_segmentation/validation"

        ImagesForPrediction = [Path(f) for f in os.listdir(img_dpath)]
        print("Starting prediction for " + str(len(ImagesForPrediction)) +
              " images.")
        for img_fname in ImagesForPrediction:
            out_fname = img_fname.stem + ".jpg"
            out_fpath = str(segm_dpath / "masks_rgb" / out_fname)
            inpFname = str(img_dpath / img_fname)
            outImg = self.model.predict_segmentation(
                inp=inpFname,
                out_fname=out_fpath
            )
            mask_path = segm_dpath / "masks" / out_fname
            if not cv2.imwrite(str(mask_path), outImg):
                print(f"Image mask {mask_path} not written!")
        print("Prediction completed")

    def evaluate(self):
        # TODO
        pass


if __name__ == "__main__":
    if len(sys.argv) == 2:
        config_file = sys.argv[1]
    else:
        print("No config file provided.")
        sys.exit()

    kerasTraining = KerasTraining(config_file)
    kerasTraining.train()
    kerasTraining.predict()
    kerasTraining.evaluate()
