from sklearn.neural_network import MLPClassifier
import os
import PIL as pil
import numpy as np

def training(X_train, Y_train):
    clf = MLPClassifier(hidden_layer_sizes=(30, 30, 30), activation='relu', validation_fraction=0.3,
    solver='adam', learning_rate_init=0.005, learning_rate='adaptive')
    
    clf.fit(X_train, Y_train)

def dataPreprocess():
    X_train = np.zeros(2)
    Y_train = np.zeros(2)
    image19=pil.Image.open("chodník kostky malá zhora synthetized19.JPG").convert('RGB')
    imageMask19=pil.Image.open("chodník kostky malá zhora synthetizedMask19.JPG").convert('RGB')
    image88=pil.Image.open("chodník kostky malá zhora synthetized88.JPG").convert('RGB')
    imageMask88=pil.Image.open("chodník kostky malá zhora synthetizedMask88.JPG").convert('RGB')

    X_train[0] = image19
    X_train[1] = image88
    Y_train[0] = imageMask19
    Y_train[1] = imageMask88

    return X_train, Y_train

if __name__ == "__main__":
    X_train, Y_train = dataPreprocess()
    training(X_train, Y_train)
