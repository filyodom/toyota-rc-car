import json
import os
import random
import sys
from pathlib import Path

import cv2
import matplotlib.pyplot as plt
import numpy as np

# plotting for testing purposes.
PLOTTING = True

# create RGB masks for presentations
# instead of grayscale masks
PRESENTATION = False

# True for automated generation of masks
# False when manually anotated masks exist in folder:
# SOURCE_IMGS_PATH/TerrainName_Mask/ImageName
# with the same name as the original image
GENERATE_MASKS = False

# Suffix of a mask to be loaded.
# Must be specified when GENERATE_MASKS = False.
# If different from image suffix e.g. "PNG".
# None otherwise.
SUFFIX = "PNG"

# load reference mask with pre-annotated unknown terrain
# must be same for all images and placed in SourceImages folder
# with name refMask.jpg
# e.g. can be used when using bird eye view with one terrain
# and black parts(unknown) on the bottom of the image
REF_MASK = False

# Unknown terrain, DO NOT CHANGE.
# Will be loaded from Json file.
OTHER = None

# Use same folder for background
# and foreground images. DO NOT CHANGE.
# Changes dynamically in main
# True if arg2 and arg3 are same and len is 1
FG_BG_SAME = None

# path to root folder MachineLearning
MACHINE_LEARNING_ROOT_PATH = Path(__file__).resolve().parents[2]
# path to folder with folders obtaining images for synthetization
SOURCE_IMGS_PATH = MACHINE_LEARNING_ROOT_PATH / "Segmentation/SourceImages"
# path used to create path where synthetized images will be stored
SYNTHETIZED_IMGS_PATH = MACHINE_LEARNING_ROOT_PATH \
                        / "Segmentation/SynthetizedImages"

# path where synthetized images will be stored
# path is created according as:
# SYNTHETIZED_IMGS_PATH / arg1 / MergedImgs
MERGED_IMGS_PATH = None
# SYNTHETIZED_IMGS_PATH / arg1 / MergedMasks
MERGED_MASKS_PATH = None

# path to JSON file with terrain ids and colours
JSON_PATH = MACHINE_LEARNING_ROOT_PATH / "Segmentation/seg_annot_labels.JSON"
# loaded JSON data
CONFIG_DATA = None

# number of rotations for foreground images
# affects total number of synthetized images
ROTATIONS_DEGREE = 20

# number for repeating synthetization
# number of synthetized images will be:
# numberOfBackgroundImages * SYNTH_REPETITION * (360 / ROTATIONS_DEGREE)
SYNTH_REPETITION = 16

# number of foreground images used for merging to one background
SAMPLE_NUM = 3

# global variable for changing pixelt to black/OTHER
# before saving according to loaded mask, DO NOT CHANGE
MASK = None


class SynthetizeImages():

    def __init__(self):
        global OTHER
        self.CheckDirs()
        self.LoadJson()
        if (PRESENTATION):
            OTHER = CONFIG_DATA['labels']['other']['color']
        else:
            OTHER = CONFIG_DATA['labels']['other']['id']

    def CheckDirs(self):
        if not os.path.exists(JSON_PATH):
            print(
                "Path to JSON config file "
                "does not exist: {}".format(JSON_PATH), file=sys.stderr)
            exit(1)
        if not os.path.exists(MERGED_IMGS_PATH):
            os.makedirs(MERGED_IMGS_PATH)
        if not os.path.exists(MERGED_MASKS_PATH):
            os.makedirs(MERGED_MASKS_PATH)

    def SynthMultipleTimes(self, backImgs, foreImgs):
        counter = 0
        for synth in range(0, SYNTH_REPETITION):
            counter = self.MergeImages(backImgs, foreImgs, counter)

    def MergeImages(self, imgToMerge1, imgToMerge2, counter):
        global MASK
        for rot in range(0, int(360/ROTATIONS_DEGREE)):
            for idxBackFol, backgroundFol in enumerate(imgToMerge1):
                sourceImgsBack = [f for f in os.listdir(backgroundFol)
                                  if os.path.isfile(backgroundFol / f)]
                for idxBackImg, backgroundImg in enumerate(sourceImgsBack):
                    fileName = "synth_{}_".format(Path(backgroundImg).stem)
                    backImg, backMask = self.LoadImg(backgroundFol
                                                     / backgroundImg,
                                                     True)
                    hB, wB, _ = backImg.shape

                    ForeImgToMerge = imgToMerge2.copy()

                    if (not FG_BG_SAME):
                        if (backgroundFol in ForeImgToMerge):
                            ForeImgToMerge.remove(backgroundFol)

                    sourceImgsFore = []

                    for idxForeFol, foregroundFol in enumerate(ForeImgToMerge):
                        sourceImgsFore += [foregroundFol / f
                                           for f in os.listdir(foregroundFol)
                                           if os.path.isfile(foregroundFol / f)
                                           ]
                    if (len(sourceImgsFore) >= SAMPLE_NUM):
                        sourceImgsFore = random.sample(sourceImgsFore,
                                                       SAMPLE_NUM)
                    else:
                        sourceImgsFore = random.sample(sourceImgsFore,
                                                       len(ForeImgToMerge))

                    for idxForeImg, foregroundImg in enumerate(sourceImgsFore):
                        foreImg, foreMask = self.LoadImg(foregroundFol
                                                         / foregroundImg,
                                                         False)

                        hF, wF, _ = foreImg.shape
                        if(wF > wB):
                            wF = wB
                        if(hF > hB):
                            hF = hB

                        randCropEndX = random.randint(int(wF/2), wF-1)
                        randCropEndY = random.randint(int(hF/2), hF-1)
                        randCropStartX = random.randint(0, int(randCropEndX/2))
                        randCropStartY = random.randint(0, int(randCropEndY/2))

                        foreImg, foreMask = self.CropImage(foreImg,
                                                           foreMask,
                                                           randCropStartX,
                                                           randCropStartY,
                                                           randCropEndX,
                                                           randCropEndY)

                        foreImg = self.RotateImage(foreImg,
                                                   rot*ROTATIONS_DEGREE)
                        foreMask = self.RotateImage(foreMask,
                                                    rot*ROTATIONS_DEGREE)

                        randMergeStartX = random.randint(0,
                                                         wB - 1 -
                                                         (randCropEndX -
                                                          randCropStartX))
                        randMergeStartY = random.randint(0,
                                                         hB - 1 -
                                                         (randCropEndY -
                                                          randCropStartY))
                        randMergeEndX = randMergeStartX +\
                            (randCropEndX - randCropStartX)
                        randMergeEndY = randMergeStartY +\
                            (randCropEndY - randCropStartY)

                        tmpImg = np.zeros((hB, wB, 3))
                        foreImg[foreImg[:, :, 0] <= 6] = [0, 0, 0]
                        foreImg[foreImg[:, :, 1] <= 6] = [0, 0, 0]
                        foreImg[foreImg[:, :, 2] <= 6] = [0, 0, 0]
                        tmpImg[randMergeStartY:randMergeEndY,
                               randMergeStartX:randMergeEndX] = foreImg

                        if (PRESENTATION):
                            tmpMask = np.ones((hB, wB, 3))
                            tmpMask = tmpMask * OTHER
                            tmpMask[randMergeStartY:randMergeEndY,
                                    randMergeStartX:randMergeEndX,
                                    :] = foreMask   # crop
                        else:
                            tmpMask = np.ones((hB, wB))
                            tmpMask = tmpMask * OTHER
                            tmpMask[randMergeStartY:randMergeEndY,
                                    randMergeStartX:randMergeEndX
                                    ] = foreMask    # crop

                        # not black pixels
                        nBlPixMaskMerg = (tmpImg != [0., 0., 0.])
                        nBlPixMaskToMerg = (tmpMask != OTHER)

                        backImg[nBlPixMaskMerg] = tmpImg[nBlPixMaskMerg]
                        backMask[nBlPixMaskToMerg] = tmpMask[nBlPixMaskToMerg]

                        # keep black pixels from picture black
                        # and for those pixels change mask to OTHER
                        # only when mask is loaded from file
                        if (MASK is not None):
                            backImg[MASK == OTHER] = [0, 0, 0]
                            backMask[MASK == OTHER] = OTHER

                        if (PLOTTING):
                            self.Plot(backImg, backMask)

                    # backImg = cv2.resize(backImg, (384, 768))
                    # backMask = cv2.resize(backMask, (384, 768))

                    # imgPers, maskPers = self.PerspTransform(backImg,
                    #                                         fileName,
                    #                                         counter,
                    #                                         False,
                    #                                         backMask)
                    # imgInvPers, maskInvPers = self.PerspTransform(imgPers,
                    #                                               fileName,
                    #                                               counter,
                    #                                               True,
                    #                                               maskPers)

                    cv2.imwrite("{}/{}{}.png".format(
                                MERGED_IMGS_PATH,
                                fileName,
                                '{:05d}'.format(counter)), backImg)
                    cv2.imwrite("{}/{}{}.png".format(
                                MERGED_MASKS_PATH,
                                fileName,
                                '{:05d}'.format(counter)), backMask)
                    counter += 1
        return counter

    def PerspTransform(self, img, fileName, counter, invTrans, mask=None):
        IMAGE_H, IMAGE_W, _ = img.shape
        IMAGE_W1 = 0.53 * IMAGE_W
        IMAGE_W2 = 0.44 * IMAGE_W
        IMAGE_W3 = 0.32 * IMAGE_W
        IMAGE_W4 = 0.30 * IMAGE_W
        RECTANGLE_DEFORMATION = IMAGE_W * 2
        trans = None

        if (invTrans):
            dst = np.float32([
                            [0, IMAGE_H],
                            [IMAGE_W, IMAGE_H],
                            [IMAGE_W4, 0],
                            [IMAGE_W - IMAGE_W3, 0]])
            src = np.float32([
                            [IMAGE_W2, RECTANGLE_DEFORMATION],
                            [IMAGE_W1, RECTANGLE_DEFORMATION],
                            [0, 0],
                            [IMAGE_W, 0]])
            # Inverse transformation matrix
            trans = cv2.getPerspectiveTransform(dst, src)
        else:
            dst = np.float32([
                            [0, RECTANGLE_DEFORMATION],
                            [IMAGE_W, RECTANGLE_DEFORMATION],
                            [IMAGE_W4, 0],
                            [IMAGE_W - IMAGE_W3, 0]])
            src = np.float32([
                            [IMAGE_W2, IMAGE_H],
                            [IMAGE_W1, IMAGE_H],
                            [0, 0],
                            [IMAGE_W, 0]])
            # The transformation matrix
            trans = cv2.getPerspectiveTransform(src, dst)

        warped_img = cv2.warpPerspective(img, trans,
                                         (IMAGE_W, RECTANGLE_DEFORMATION))
        warped_mask = None
        if (mask):
            warped_mask = cv2.warpPerspective(mask, trans,
                                              (IMAGE_W, RECTANGLE_DEFORMATION))

            warped_mask[(warped_img == [0., 0., 0.]).all(axis=2)] = OTHER
            if (PLOTTING):
                self.Plot(warped_img, warped_mask)

        # warped_imgRGB = cv2.cvtColor(warped_img, cv2.COLOR_BGR2RGB)
        # cv2.imwrite("{}/MergedImgsPers"
        #             "/{}{}.png".format(SYNTHETIZED_IMGS_PATH,
        #                                fileName,
        #                                counter), warped_imgRGB)
        # warped_maskRGB = cv2.cvtColor(warped_mask, cv2.COLOR_BGR2RGB)
        # cv2.imwrite("{}/MergedMasksPers"
        #             "/{}{}.png".format(SYNTHETIZED_IMGS_PATH,
        #                                fileName,
        #                                counter), warped_maskRGB)
        return warped_img, warped_mask

    def RotateImage(self, imageToRotate, angle):
        (h, w) = imageToRotate.shape[:2]
        center = (w / 2, h / 2)
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotatedImage = cv2.warpAffine(imageToRotate, M, (w, h))

        return rotatedImage

    def CropImage(self, imageToCrop, maskToCrop, x1, y1, x2, y2):
        cropedImage = imageToCrop[y1:y2, x1:x2, :]
        cropedImageMask = maskToCrop[y1:y2, x1:x2]
        cropedImageMask[(cropedImage == [0., 0., 0.]).all(axis=2)] = OTHER

        return cropedImage, cropedImageMask

    def GetUnknownPercentage(self, img):
        h, w = img.shape
        npCropedImage = img
        blackPixelsMask = (npCropedImage == [0., 0., 0.]).all(axis=2)
        cntBlackPixels = len(np.argwhere(blackPixelsMask))
        cntPixels = w * h
        blackPixelsPercentage = cntBlackPixels / cntPixels

        return blackPixelsPercentage, blackPixelsMask

    def prepImgsPath(self, arg2, arg3):
        imgs1 = []
        imgs2 = []
        for background in arg2:
            if os.path.exists(SOURCE_IMGS_PATH / background):
                imgs1.append(SOURCE_IMGS_PATH / background)  # folder of images
            else:
                print(
                    "Path to folder with background images "
                    "does not exist: {}".format(
                        SOURCE_IMGS_PATH / background), file=sys.stderr)
        for foreground in arg3:
            if os.path.exists(SOURCE_IMGS_PATH / foreground):
                imgs2.append(SOURCE_IMGS_PATH / foreground)  # folder of images
            else:
                print(
                    "Path to folder with foreground images "
                    "does not exist: {}".format(
                        SOURCE_IMGS_PATH / foreground), file=sys.stderr)
        if (len(imgs1) == 0 or len(imgs2) == 0):
            exit(1)

        return imgs1, imgs2

    def LoadImg(self, imgPath, isBackground, isMask=False):
        global MASK
        if (not Path.exists(imgPath)):
            print("Path to image doesn't exist: imgPath", file=sys.stderr)
            return
        loadedImg = cv2.imread(str(imgPath))

        # when loading only mask
        # just load it and return it
        if (isMask):
            if isBackground:
                MASK = loadedImg.copy()
            return loadedImg

        # when anotated mask exist, load it
        # return loaded image and mask
        if (not GENERATE_MASKS):
            if (SUFFIX):
                maskPath = Path("{}_Mask".format(imgPath.resolve().parents[0]))\
                            / "{}.{}".format(os.path.splitext(imgPath.resolve().name)[0], SUFFIX)
            else:
                maskName = os.path.splitext(imgPath.resolve().name)
                maskPath = Path("{}_Mask".format(imgPath.resolve().parents[0]))\
                                / "{}.{}".format(maskName[0], maskName[1])

            imgMask = cv2.imread(str(maskPath))
            if imgMask is None:
                loadedImg, imgMask = self.CreateMask(loadedImg, imgPath, isBackground)
            else:
                imgMask = cv2.cvtColor(imgMask, cv2.COLOR_BGR2GRAY)

            # change white frame from anotation tool to OTHER
            if len(imgMask[imgMask == 255]) > 0:
                imgMask[imgMask == 255] = OTHER

            if isBackground:
                MASK = imgMask.copy()

            return loadedImg, imgMask

        # load reference mask with pre-annotated unknown terrain
        if (REF_MASK):
            terrain = CONFIG_DATA['labels'][imgPath.parents[0].stem]['id']
            imgMask = cv2.imread(str(SOURCE_IMGS_PATH / "refMask.jpg"), 0)
            imgMask[imgMask > OTHER] = terrain

            # change white frame from anotation tool to OTHER
            if len(imgMask[imgMask == 255]) > 0:
                imgMask[imgMask == 255] = OTHER

            if isBackground:
                MASK = imgMask.copy()

            return loadedImg, imgMask

        loadedImg, imgMask = self.CreateMask(loadedImg, imgPath, isBackground)
        return loadedImg, imgMask
        # # when anotated mask doesn't exist and must be created
        # # create the mask and return loaded image and created mask
        # if (PRESENTATION):
        #     terrain = CONFIG_DATA['labels'][imgPath.parents[0].stem]['color']
        #     imgMask = loadedImg.copy()
        #     imgMask[:, :, :] = terrain
        #     imgMask[loadedImg == 0] = OTHER
        # else:
        #     terrain = CONFIG_DATA['labels'][imgPath.parents[0].stem]['id']
        #     imgMask = (cv2.cvtColor(loadedImg, cv2.COLOR_BGR2GRAY)).copy()
        #     imgMask[imgMask <= 6] = OTHER
        #     imgMask[imgMask > 1] = terrain

        # MASK = None

        # return loadedImg, imgMask

    def CreateMask(self, loadedImg, imgPath, isBackground):
        global MASK
        # when anotated mask doesn't exist and must be created
        # create the mask and return loaded image and created mask
        if (PRESENTATION):
            terrain = CONFIG_DATA['labels'][imgPath.parents[0].stem]['color']
            imgMask = loadedImg.copy()
            imgMask[:, :, :] = terrain
            imgMask[loadedImg == 0] = OTHER
        else:
            terrain = CONFIG_DATA['labels'][imgPath.parents[0].stem]['id']
            imgMask = (cv2.cvtColor(loadedImg, cv2.COLOR_BGR2GRAY)).copy()
            imgMask[imgMask <= 6] = OTHER
            imgMask[imgMask > 1] = terrain

        if isBackground:
            MASK = None

        return loadedImg, imgMask

    def LoadJson(self):
        global CONFIG_DATA
        with open(JSON_PATH) as json_file:
            CONFIG_DATA = json.load(json_file)

    def Plot(self, img1, img2):
        plt.figure()
        plt.imshow(img1)
        plt.figure()
        plt.imshow(img2)
        plt.show()


if __name__ == "__main__":

    if (len(sys.argv) == 1):
        arg1 = "dataset_17_12"  # "4_IDs_08_10"
        arg2 = ["anot_jizda"]
                # ["grass",
                # "asphalt",
                # "gravel"]
        arg3 = ["grass",
                "gravel",
                "anot_jizda"]
        print(
            "Using default arguments: \narg1: {}\narg2: {}\n"
            "arg3: {}".format(arg1, arg2, arg3))
    elif (len(sys.argv) == 4):
        # folder name of the output images
        arg1 = sys.argv[1]
        # folder of background images
        arg2 = map(str, sys.argv[2].strip('[]').split(','))
        # folder of foreground images
        arg3 = map(str, sys.argv[3].strip('[]').split(','))
        print(
            "Using input arguments: \narg1: {}\narg2: {}\n"
            "arg3: {}".format(arg1, arg2, arg3))
    else:
        print(
            "Not enough input arguments. Got {}, but expected was 3. "
            "1st argument must be string with the name of the output folder "
            "2nd argument must be list of folder names with backgrounds "
            "and 3rd argument must be list of folder names with images "
            "to place on background. Example input parameters: "
            "SynImgs \"[\"asphalt\"]\" \"[\"asphalt\",\"cobble_stone\"]\""
            .format(len(sys.argv)-1), file=sys.stderr)
        exit(1)

    if (arg2 == arg3 and len(arg2) == 1):
        FG_BG_SAME = True
    else:
        FG_BG_SAME = False

    MERGED_IMGS_PATH = SYNTHETIZED_IMGS_PATH / arg1 / "MergedImgs"
    MERGED_MASKS_PATH = SYNTHETIZED_IMGS_PATH / arg1 / "MergedMasks"

    synImg = SynthetizeImages()
    imgs1, imgs2 = synImg.prepImgsPath(arg2, arg3)

    numOfBackImages = 0
    for backImgs in imgs1:
        numOfBackImages += len(os.listdir(backImgs))

    print("Number of repetitions is: {}. Rotation angle is: {}. "
          "Number of background images is: {}.\n"
          "Total number of images will be: {}."
          .format(SYNTH_REPETITION,
                  ROTATIONS_DEGREE,
                  numOfBackImages,
                  int(numOfBackImages * SYNTH_REPETITION *
                      (360 / ROTATIONS_DEGREE))))
    input("\nPress Enter to start synthetizing...")

    synImg.SynthMultipleTimes(imgs1, imgs2)
