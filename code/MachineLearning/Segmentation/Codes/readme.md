# Segmentation ReadMe File

## Running training and segmentation

Core scripts are *segmentator.py* and *models.py*. Segmentator provides interface to training and segmentation, while models contain CNN models.

*segmentator.py* script contains command line interface, but class KerasSegmentator can be used as module aswell. CLI provides also simple help by running `segmentator.py -h`. Example command for training and segmentation:
```
python3 segmentator.py -m seg_model/seg_model.h5 -e 50 --train --segment -d "../Datasets/dataset_1" -r "real_data"
```

## Running scripts on FEE GPU server
Training of neural networks can be done on *cantor* or *taylor* GPU servers.

0) Copy all files (scripts and source data) to server using `scp` (just for the first time), i.e.
    ```
    scp -r local_dir username@cantor.felk.cvut.cz:~/remote_dir
    ```
1) Connect to GPU server using SSH:
    ```
    ssh username@cantor.felk.cvut.cz
    ```
2) Load modules using 'module load' or
    ```
    source keras_seg_init.sh
    ```
    There are several GPU units available on server and some might be already computing. There can be listed by command `nvidia-smi`. It is requested by server administrators to select one GPU. By default, GPU 0 is selected, but providing argument while calling *keras_seg_init.sh* selects different GPU.


3) Run file in Python (select correct version according to loaded modules).

### Detaching running scripts
When connection is lost, running scripts stop. In order to run scripts (training) after disconnecting of SSh connection, use
```
screen
python script.py
```
then pres *ctrl+a* and *ctrl+d* respectively to detach and run script. Then, SSH connection can be terminated without interrupting script.

Reataching shall be then done using
```
screen -r
```

One can also view all running scripts (with users) by running
```
ps -up `nvidia-smi -q -x | grep pid | sed -e 's/<pid>//g' -e 's/<\/pid>//g' -e 's/^[[:space:]]*//'`
```

## FAQ
* **Can I install more modules?**

    Pip is not available on the server. Neverthless, modules may be installed by cloning repository and then manually installing by
    ```
    python setup.py install --user
    ```
* **Running script throws 'Invalid syntax' exception.**

    Script is written in Python 3, which is not default version and might not be loaded as module. Try `source keras_seg_init.sh`.

* **Running machine learning is slow. Can I speed them up computatioons?**

    There are several reasons, why computation might be slower, then possible.

    * Set maximal performance settings of Jetson.

    * Use cross compiled TensorRT.

    * Set maximum performance model for nvidia frameworks by `sudo nvpmodel -m 0`.
