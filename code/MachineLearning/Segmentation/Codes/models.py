#!/usr/bin/env python3

from keras.models import Model

from keras.layers.convolutional import Conv2D, UpSampling2D, Conv2DTranspose
from keras.layers.pooling import MaxPooling2D
from keras.layers import \
    (Input, BatchNormalization, Activation, Dropout, Reshape, ZeroPadding2D)
from keras.layers.merge import concatenate

from keras.optimizers import Adam, SGD, Adadelta


def _conv2d_block(
        input_tensor, n_filters, kernel_size=3, batchnorm=False
        ):
    # first layer
    x = Conv2D(filters=n_filters, kernel_size=(kernel_size, kernel_size),
               kernel_initializer="he_normal", activation='relu',
               padding="same")(input_tensor)
    if batchnorm:
        x = BatchNormalization()(x)
    x = Activation("relu")(x)
    # second layer
    x = Conv2D(filters=n_filters, kernel_size=(kernel_size, kernel_size),
               kernel_initializer="he_normal", activation='relu',
               padding="same")(x)
    if batchnorm:
        x = BatchNormalization()(x)
    x = Activation("relu")(x)
    return x


def get_unet_seg(
        im_height, im_width, num_classes
        ):
    img_input = Input(shape=(im_height, im_width, 3))

    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(img_input)
    conv1 = Dropout(0.2)(conv1)
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv1)
    pool1 = MaxPooling2D((2, 2))(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
    conv2 = Dropout(0.2)(conv2)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2)
    pool2 = MaxPooling2D((2, 2))(conv2)

    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
    conv3 = Dropout(0.2)(conv3)
    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv3)

    up1 = concatenate([UpSampling2D((2, 2))(conv3), conv2], axis=-1)
    conv4 = Conv2D(64, (3, 3), activation='relu', padding='same')(up1)
    conv4 = Dropout(0.2)(conv4)
    conv4 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv4)

    up2 = concatenate([UpSampling2D((2, 2))(conv4), conv1], axis=-1)
    conv5 = Conv2D(32, (3, 3), activation='relu', padding='same')(up2)
    conv5 = Dropout(0.2)(conv5)
    conv5 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv5)

    out = Conv2D(num_classes, (3, 3), padding='same', activation='sigmoid')(conv5)

    model = Model(input=img_input, output=out)

    model.compile(
        optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy']
        )

    return model


def get_unet_camvid(
        im_height, im_width, num_classes
        ):
    inputs = Input((im_height, im_width, 3))

    drop = 0.5

    conv1 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(inputs)
    conv1 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool1)
    conv2 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool2)
    conv3 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    conv4 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool3)
    conv4 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4)
    drop4 = Dropout(drop)(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)  # was 2,2

    conv5 = Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool4)
    conv5 = Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv5)
    drop5 = Dropout(drop)(conv5)

    up6 = Conv2D(512, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(drop5))  # was 2,2
    merge6 = concatenate([drop4, up6])
    conv6 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge6)
    conv6 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv6)
    up7 = Conv2D(256, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv6))
    merge7 = concatenate([conv3, up7])
    conv7 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge7)
    conv7 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv7)
    up8 = Conv2D(128, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv7))
    merge8 = concatenate([conv2, up8])
    conv8 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge8)
    conv8 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)

    up9 = Conv2D(64, 2, activation='relu', padding='same', kernel_initializer='he_normal')(
        UpSampling2D(size=(2, 2))(conv8))
    merge9 = concatenate([conv1, up9])
    conv9 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge9)
    conv9 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
    conv9 = Conv2D(num_classes, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)

    conv10 = Conv2D(num_classes, 3, padding='same', activation='sigmoid')(conv9)
    model = Model(input=inputs, output=conv10)

    optimizer = Adam(lr=5e-4)
    # optimizer = SGD(lr=0.01, clipvalue=0.5)

    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

    return model


def get_unet_kerseg(
        im_height, im_width, num_classes, optim="adadelta"
        ):
    # If input size is None (CNN), input can be any size
    # input_img = Input((None, None, 3))
    input_img = Input((im_height, im_width, 3))

    x = Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv1')(input_img)
    x = Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv2')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x)
    f1 = x
    # Block 2
    x = Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv1')(x)
    x = Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv2')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x)
    f2 = x

    # Block 3
    x = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv1')(x)
    x = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv2')(x)
    x = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv3')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x)
    f3 = x

    # Block 4
    x = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv1')(x)
    x = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv2')(x)
    x = Conv2D(512, (3, 3), activation='relu', padding='same', name='block4_conv3')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x)
    f4 = x

    # Block 5
    x = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv1')(x)
    x = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv2')(x)
    x = Conv2D(512, (3, 3), activation='relu', padding='same', name='block5_conv3')(x)
    x = MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(x)
    f5 = x

    o = f4

    o = (ZeroPadding2D((1, 1)))(o)
    o = (Conv2D(512, (3, 3), padding='valid'))(o)
    o = (BatchNormalization())(o)

    o = (UpSampling2D((2, 2)))(o)
    o = (concatenate([o, f3], axis=-1))
    o = (ZeroPadding2D((1, 1)))(o)
    o = (Conv2D(256, (3, 3), padding='valid'))(o)
    o = (BatchNormalization())(o)

    o = (UpSampling2D((2, 2)))(o)
    o = (concatenate([o, f2], axis=-1))
    o = (ZeroPadding2D((1, 1)))(o)
    o = (Conv2D(128, (3, 3), padding='valid'))(o)
    o = (BatchNormalization())(o)

    o = (UpSampling2D((2, 2)))(o)

    # if l1_skip_conn:
    #     o = ( concatenate([o,f1],axis=-1) )

    o = (ZeroPadding2D((1, 1)))(o)
    o = (Conv2D(64, (3, 3), padding='valid'))(o)
    o = (BatchNormalization())(o)

    o = (UpSampling2D((2, 2)))(o) # delete?

    o = Conv2D(num_classes, (3, 3), padding='same')(o)

    # Last part
    o_shape = Model(input_img, o).output_shape
    output_height = o_shape[1]
    output_width = o_shape[2]
    # o = (Reshape((output_height, output_width, -1)))(o)  # not convolution layer!!!
    o = (Activation('softmax', name="activation_softmax"))(o)  # Default
    # o = (Activation('sigmoid', name="activation_sigmoid"))(o)

    model = Model(input=input_img, output=o)

    if "adadelta" == optim:
        optimizer = Adadelta(lr=1.0, rho=0.95, decay=0.0)  # Default parameters' values
    # optimizer = Adadelta(lr=3, rho=0.95, decay=0)
    elif "sgd" == optim:
        optimizer = SGD(lr=0.01, momentum=0.0, decay=0.0)  # Default parameters' values
    # optimizer = SGD(lr=0.005, momentum=0.0, decay=0.0)
    elif "adam" == optim:
        optimizer = Adam()
    else:
        optimizer = Adadelta(lr=1.0, rho=0.95, decay=0.0)  # Default parameters' values

    model.compile(
        optimizer=optimizer,
        loss='categorical_crossentropy',
        metrics=['categorical_accuracy'])

    return model


def get_unet(
        im_height, im_width, n_classes, dropout=0.85, batchnorm=True
    ):

    input_img = Input((im_height, im_width, 3), name='img')

    # TODO: optional or/and conditional resize?

    # contracting path
    c1 = _conv2d_block(
        input_img, n_filters=n_classes, kernel_size=3, batchnorm=batchnorm
        )
    p1 = MaxPooling2D((2, 2))(c1)
    p1 = Dropout(dropout*0.5)(p1)

    c2 = _conv2d_block(
        p1, n_filters=n_classes*2, kernel_size=3, batchnorm=batchnorm
        )
    p2 = MaxPooling2D((2, 2))(c2)
    p2 = Dropout(dropout)(p2)

    c3 = _conv2d_block(
        p2, n_filters=n_classes*4, kernel_size=3, batchnorm=batchnorm
        )
    p3 = MaxPooling2D((2, 2))(c3)
    p3 = Dropout(dropout)(p3)

    c4 = _conv2d_block(
        p3, n_filters=n_classes*8, kernel_size=3, batchnorm=batchnorm
        )
    p4 = MaxPooling2D(pool_size=(2, 2))(c4)
    p4 = Dropout(dropout)(p4)

    c5 = _conv2d_block(
        p4, n_filters=n_classes*16, kernel_size=3, batchnorm=batchnorm
        )

    # expansive path
    u6 = Conv2DTranspose(n_classes*8, (3, 3), strides=(2, 2), padding='same')(c5)
    u6 = concatenate([u6, c4])
    u6 = Dropout(dropout)(u6)
    c6 = _conv2d_block(
        u6, n_filters=n_classes*8, kernel_size=3, batchnorm=batchnorm
        )

    u7 = Conv2DTranspose(n_classes*4, (3, 3), strides=(2, 2), padding='same')(c6)
    u7 = concatenate([u7, c3])
    u7 = Dropout(dropout)(u7)
    c7 = _conv2d_block(
        u7, n_filters=n_classes*4, kernel_size=3, batchnorm=batchnorm
        )

    u8 = Conv2DTranspose(n_classes*2, (3, 3), strides=(2, 2), padding='same')(c7)
    u8 = concatenate([u8, c2])
    u8 = Dropout(dropout)(u8)
    c8 = _conv2d_block(
        u8, n_filters=n_classes*2, kernel_size=3, batchnorm=batchnorm
        )

    u9 = Conv2DTranspose(n_classes*1, (3, 3), strides=(2, 2), padding='same')(c8)
    u9 = concatenate([u9, c1], axis=3)
    u9 = Dropout(dropout)(u9)
    c9 = _conv2d_block(
        u9, n_filters=n_classes*1, kernel_size=3, batchnorm=batchnorm
        )

    outputs = Conv2D(n_classes, (3, 3), padding='same', activation='sigmoid')(c9)

    model = Model(inputs=[input_img], outputs=[outputs])

    model.compile(
        optimizer=Adam(lr=1e-4), loss="categorical_crossentropy", metrics=["accuracy"])
    # sparse_categorical_crossentropy

    return model


model_getters = {
    "get_unet": get_unet,
    "get_unet_seg": get_unet_seg,
    "get_unet_camvid": get_unet_camvid,
    "get_unet_kerseg": get_unet_kerseg
}
