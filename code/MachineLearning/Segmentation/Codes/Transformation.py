import cv2
import numpy as np
import matplotlib.pyplot as plt
import pathlib
import os
# https://nikolasent.github.io/opencv/2017/05/07/Bird's-Eye-View-Transformation.html
borderWide=1000
IMAGE_H = 720
IMAGE_W = 1280

#cycle
fn = pathlib.Path(__file__).parent / 'example.py'
#path=str(fn.parent)+"/images/"
path = "input\\"
inputImageNames=os.listdir(path)
inputImageNames.sort()
#inputImageNames.remove('output')
#cycle
#dst=np.float32([[0, IMAGE_H], [IMAGE_W, IMAGE_H], [0, 0], [IMAGE_W, 0]])
#src = np.float32([[261, 578], [2933, 580], [1486, 345], [1746, 346]])
dst = np.float32([[0, IMAGE_H], [IMAGE_W, IMAGE_H], [380, 0], [IMAGE_W-410, 0]])
src = np.float32([[560, IMAGE_H], [680, IMAGE_H], [0, 0], [IMAGE_W, 0]])
#dst = np.float32([[669, IMAGE_H], [811, IMAGE_H], [0, 0], [IMAGE_W, 0]])
M = cv2.getPerspectiveTransform(src, dst) # The transformation matrix
Minv = cv2.getPerspectiveTransform(dst, src) # Inverse transformation

#len(inputImageNames)
#for i in range(0,10):
for i in range(0,len(inputImageNames)):
    #img = cv2.imread('./img_1565268930.4475617AAA.jpg') # Read the test img
    img = cv2.imread(path+inputImageNames[i])
    
    
 
    # img = img[450:(450+IMAGE_H), 0:IMAGE_W] # Apply np slicing for ROI crop
    #img = img[350:(350+IMAGE_H), 0:IMAGE_W] # Apply np slicing for ROI crop
    """ plt.imshow(img)
    plt.show() """
    warped_img = cv2.warpPerspective(img, M, (IMAGE_W, IMAGE_H)) # Image warping
    #cv2.imwrite('output\\output_perspective_birdsEye'+str(i)+'.png',warped_img)
    #warped_img = cv2.warpPerspective(warped_img, M, (IMAGE_W, IMAGE_H)) # Image warping
    cv2.imwrite('output\\output_perspective_birdsEye_Back'+str(i)+'.png',warped_img)




""" import cv2
import numpy as np
import matplotlib.pyplot as plt
import pathlib
import os
# https://nikolasent.github.io/opencv/2017/05/07/Bird's-Eye-View-Transformation.html
borderWide=1000
IMAGE_H = 720
IMAGE_W = 1280+2*borderWide
IMAGE_W2=1280
#cycle
fn = pathlib.Path(__file__).parent / 'example.py'
path=str(fn.parent)+"/images/"
inputImageNames=os.listdir(path)
inputImageNames.sort()
inputImageNames.remove('output')
#cycle
dst=np.float32([[0, IMAGE_H], [IMAGE_W, IMAGE_H], [0, 0], [IMAGE_W, 0]])
src = np.float32([[261, 578], [2933, 580], [1486, 345], [1746, 346]])

#dst = np.float32([[669, IMAGE_H], [811, IMAGE_H], [0, 0], [IMAGE_W, 0]])
M = cv2.getPerspectiveTransform(src, dst) # The transformation matrix
Minv = cv2.getPerspectiveTransform(dst, src) # Inverse transformation

#len(inputImageNames)
#for i in range(0,10):
for i in range(0,len(inputImageNames)):
    #img = cv2.imread('./img_1565268930.4475617AAA.jpg') # Read the test img
    img = cv2.imread(path+inputImageNames[i])
    ppp=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
    img=np.full(((720,1280+2*borderWide,3)), 0.0, dtype=None, order='C')
    img[:,borderWide:img.shape[1]-borderWide,:]=ppp[:,0:1280,:]
    
 
    # img = img[450:(450+IMAGE_H), 0:IMAGE_W] # Apply np slicing for ROI crop
    #img = img[350:(350+IMAGE_H), 0:IMAGE_W] # Apply np slicing for ROI crop
    warped_img = cv2.warpPerspective(img, M, (IMAGE_W, IMAGE_H)) # Image warping
    
    cv2.imwrite(path+'output/output_perspective_birdsEye'+str(i)+'.jpg',warped_img)


 """