#!/usr/bin/env python3

""" KerasSegmentator class provides interface to CNN training and segmentation.

This code is greatly inspired by tutorial made by Tobias Sterbak
provided under MIT license
(https://www.depends-on-the-definition.com/unet-keras-segmenting-images/)
and unet-rgb segmentation model by ShawDa
(https://github.com/ShawDa/unet-rgb)

TODO: Analyze class pizel rate in whole training set and compute class weights
for training.

TODO: Setup pixel-weighted loss function or categorical accuracy.
"""

import os
import shutil
from pathlib import Path
import csv
import argparse
from numpy import random

import numpy as np
from PIL import Image
import json

from tqdm import tqdm

import tensorflow as tf

from keras import backend as K
from keras.models import load_model, save_model
from sklearn.metrics import confusion_matrix
from keras.callbacks import ModelCheckpoint
from keras.preprocessing.image import img_to_array, load_img

import models

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt  # noqa: E402


class KerasSegmentator():

    def __init__(self, height, width, num_classes, model_path,
                 use_gpu=True, real_weight=10, var_input=False,
                 optim="adadelta"):
        """ Initializes KerasSegmentator.

        During initialization, NN model is either created or loaded from file.

        Arguments:
            height: NN input image height.
            width: NN input image width.
            num_classes: Total number of classes to recognize (including
            non-recognizable zero).
            model_path: Path to Keras model to load. If file does not exists,
            new model will be created on provided path during training.
            use_gpu: Flag for using GPU (if available).
            real_weights: Real sample training weights (equal weight is 1).
            var_input: If true, model is made with variable input size.

        TODO: Create or load model in separate methods?
        """
        self.img_height = height
        self.img_width = width
        self.num_classes = num_classes

        model_path = Path(model_path)
        self._model_path = model_path
        self._out_path = model_path.parents[0]

        # DEBUG: was in the end
        if use_gpu:
            # Use GPU for computing
            tf.Session(config=tf.ConfigProto(log_device_placement=True))
            print("Available GPUs: " +
                  str(K.tensorflow_backend._get_available_gpus()))
        else:
            print("Using CPU for computing")

        self.model = None
        if Path(self._model_path).is_file():
            try:
                self.model = load_model(str(model_path))
                print(f"Model loaded from {str(model_path)}.")
            except (IOError, ImportError) as e:
                print(e)
                print("Model not imported.")
        else:
            print("Model not loaded, creating new one.")
            os.makedirs(self._out_path, exist_ok=True)

            get_model = models.get_unet_kerseg
            # get_model = models.get_unet_camvid

            if not var_input:
                self.model = get_model(
                    self.img_height,
                    self.img_width,
                    self.num_classes,
                    optim
                )
            else:
                self.model = get_model(
                    None,
                    None,
                    self.num_classes,
                    optim
                )

            print("Model created.")

        # self.model.summary()

        model_size = self._get_model_memory_usage()
        print(f"Model memmory usage (BS=1) is {model_size} GB.")

        # Resolve MachineLearning folder path
        self._ML_ROOT_PATH = Path(__file__).resolve().parents[2]

        self.X_train = np.array([])
        self.y_train = np.array([])
        self.X_valid = np.array([])
        self.y_valid = np.array([])

        self._REAL_WEIGHT = real_weight  # 10 is pretty good

        self.dataset_path = None

        self.num_train_samples = None
        self.num_valid_samples = None

        self.train_img_fnames = np.array([])
        self.annot_img_fnames = np.array([])

    def _get_model_memory_usage(self, batch_size=1):
        """ Computes approximate amount of memory needed for segmentator model.
        """

        model = self.model
        shapes_mem_count = 0
        for l in model.layers:
            single_layer_mem = 1
            for s in l.output_shape:
                if s is None:
                    continue
                single_layer_mem *= s
            shapes_mem_count += single_layer_mem

        trainable_count = np.sum([K.count_params(p) for p in set(model.trainable_weights)])
        non_trainable_count = np.sum([K.count_params(p) for p in set(model.non_trainable_weights)])

        number_size = 4.0
        if K.floatx() == 'float16':
            number_size = 2.0
        if K.floatx() == 'float64':
            number_size = 8.0

        total_memory = number_size*(batch_size*shapes_mem_count + trainable_count + non_trainable_count)
        gbytes = np.round(total_memory / (1024.0 ** 3), 3)

        return gbytes

    def load_weights(self, weights_file):
        """ Load weights to a model from file.
        """
        if self.model:
            print(self.model.load_weights(str(weights_file), skip_mismatch=False))
            print(f"Weights loaded from {weights_file}.")
            return True
        else:
            print("No model available. Weights not loaded.")
            return False

    def save_weights(self, weights_file=None):
        """ Save trained weights to a model from file.
        """
        if self.model:
            if weights_file is None:
                weights_file = self._model_path.stem + "weights.h5"
            print(self.model.save_weights(str(weights_file)))
            print(f"Weights saved to {weights_file}.")
            return True
        else:
            print("No model available. Weights not loaded.")
            return False

    def save_model(self, model_file=None):
        """ Save trained weights to a model from file.
        """
        if self.model:
            if model_file is None:
                model_file = str(self._model_path)
            print(save_model(self.model, str(model_file)))
            print(f"Model saved to {model_file}.")
            return True
        else:
            print("No model available. Weights not loaded.")
            return False

    def _get_generator(self,
                       checked_data,
                       batch_size,
                       randomize=True):
        """ Returns generator of data from image paths.

        All data are first shuffeled and splitted to batches according
        to batch size. Then, batches are yielded as tuple (X, y, weights).

        Arguments:
            checked_data: List of paths to images.
            batch_size: Batch size.
        """

        # Shuffle and divide data (indexes) to batches
        data_len = len(checked_data)
        all_idxs = np.arange(data_len)
        if randomize:
            np.random.shuffle(all_idxs)

        batches_range = range((data_len + batch_size - 1) // batch_size)
        idxs_batches = [all_idxs[i*batch_size:(i+1)*batch_size] for i in batches_range]

        batch = 0
        num_batches = len(batches_range)
        while True:

            curr_batch_size = len(idxs_batches[batch])

            X = np.zeros(
                (curr_batch_size, self.img_height, self.img_width, 3),
                dtype=np.uint8)
            y = np.zeros(
                (curr_batch_size, self.img_height, self.img_width, self.num_classes),
                dtype=np.uint8
                )
            weights = np.ones((curr_batch_size,), dtype=np.uint8)

            # for n in range(batch_size):
            for n in range(curr_batch_size):

                # sample_idx = random.choice(data_len, 1)[0]
                sample_idx = idxs_batches[batch][n]

                img_path = checked_data[sample_idx][0]
                annot_path = checked_data[sample_idx][1]

                img = load_img(img_path)
                annot = load_img(annot_path, grayscale=True)

                if img_path.stem != annot_path.stem:
                    print("Img and annot do not match! "
                          f"{img_path}, {annot_path}")

                x_img = img_to_array(img)
                y_annot = img_to_array(annot)
                y_annot = self._inflate_mask_dim(y_annot)

                X[n] = x_img

                y[n] = y_annot

                weights[n] = self.sample_weights[sample_idx]

            if batch < (num_batches - 1):
                batch += 1
            else:
                batch = 0

            yield (X, y, weights)

    def _get_data_generators(self,
                             dataset_dir,
                             expand_train_subdirs=[],
                             batch_size=1,
                             randomize=False):
        """ Creates data generators for training and validation data.

        Arguments:
            dataset_dir:
        """

        self.dataset_path = Path(dataset_dir)

        train_dirs = [Path(dataset_dir, "training")]
        valid_dir = Path(dataset_dir, "validation")

        num_training_samples = len([name for name in os.listdir(train_dirs[0] / "images") if os.path.isfile(name)])

        if expand_train_subdirs:
            expand_train_subdirs = \
                [Path(dataset_dir) / d for d in expand_train_subdirs]

            train_dirs.extend(expand_train_subdirs)

        self._train_dirs = train_dirs

        print(f"Loading data from {str(dataset_dir)}.")

        # Checking data
        # FIXME: segmentation requires 'training' folder
        train_data_checked = self._check_img_annots(train_dirs)
        valid_data_checked = self._check_img_annots([valid_dir])

        self.num_train_samples = len(train_data_checked)
        self.num_valid_samples = len(valid_data_checked)

        train_fdata = train_data_checked
        valid_fdata = valid_data_checked

        self.sample_weights = np.ones(
            (self.num_train_samples,),
            dtype=np.uint8
            )
        num_real_training = self.num_train_samples-num_training_samples

        self.sample_weights[num_real_training:-1] = self._REAL_WEIGHT

        # Save training and validation file names for saving segmented
        # images
        self.train_img_fnames = [fd[0].stem for fd in train_fdata]
        self.valid_img_fnames = [fd[0].stem for fd in valid_fdata]

        train_generator = self._get_generator(
            train_data_checked, batch_size, randomize)
        valid_generator = self._get_generator(
            valid_data_checked, batch_size, randomize)

        return train_generator, valid_generator

    def load_data(self, dataset_dir, expand_train_subdirs=None, training=True,
                  validation=True):
        """ Loads training and validation data from dataset.

        Arguments:
            dataset_dir: Path to dataset root directory
            expand_train_subdirs: Paths to directories with additional
            training data (relative to dataset_dir).

        TODO: use data generator instead?
        """

        self.dataset_path = Path(dataset_dir)

        train_dirs = [Path(dataset_dir, "training")]
        valid_dir = Path(dataset_dir, "validation")

        num_training_samples = len([name for name in os.listdir(train_dirs[0] / "images") if os.path.isfile(name)])

        if expand_train_subdirs:
            expand_train_subdirs = \
                [Path(dataset_dir) / d for d in expand_train_subdirs]

            train_dirs.extend(expand_train_subdirs)

        self._train_dirs = train_dirs

        print(f"Loading training data from {str(train_dirs)}.")

        # Checking data
        train_data_checked = self._check_img_annots(train_dirs)
        valid_data_checked = self._check_img_annots([valid_dir])

        total_num_training_samples = len(train_data_checked)

        train_fdata = train_data_checked
        valid_fdata = valid_data_checked

        # Save training and validation file names for saving segmented
        # images
        self.train_img_fnames = [fd[0].stem for fd in train_fdata]
        self.valid_img_fnames = [fd[0].stem for fd in valid_fdata]

        self.sample_weights = np.ones(
            (total_num_training_samples,),
            dtype=np.uint8
            )
        num_real_training = total_num_training_samples-num_training_samples

        self.sample_weights[num_real_training:-1] = self._REAL_WEIGHT

        if training:
            self.X_train, self.y_train = self._load_data_dirs(train_data_checked)
        if validation:
            print(f"Loading validation data from {str(valid_dir)}")
            self.X_valid, self.y_valid = self._load_data_dirs(valid_data_checked)

        return True

    def _check_img_annots(self, paths):
        """ Check if all files (images and annotations) exist.
        """

        checked_data = []

        if not isinstance(paths, list):
            paths = [paths]

        for path in paths:
            img_dir = Path(path) / "images"
            annots_dir = Path(path) / "annotations"

            if not img_dir.is_dir() and annots_dir.is_dir():
                print(f"Path {path} is not valid!")
                return checked_data

            for img in img_dir.iterdir():

                # Check if mask exists
                if img.is_file():
                    annot = (annots_dir / img.stem).with_suffix(".png")
                    if annot.exists() and annot.is_file():
                        checked_data.append((img, annot,))
                    else:
                        print(f"No mask found for image {img}, "
                              f"looked for {annot}.")

        return checked_data

    def _load_data_dirs(self, checked_data):
        num_images = len(checked_data)

        X = np.zeros(
            (num_images, self.img_height, self.img_width, 3),
            dtype=np.uint8
            )
        y = np.zeros(
            (num_images, self.img_height, self.img_width, self.num_classes),
            dtype=np.uint8
            )

        n = 0
        for d in tqdm(checked_data):
            img_path = d[0]
            annot_path = d[1]

            img = load_img(img_path)
            annot = load_img(annot_path, grayscale=True)

            if img_path.stem != annot_path.stem:
                print("Img and annot do not match! "
                      f"{img_path}, {annot_path}")

            x_img = img_to_array(img)
            y_annot = img_to_array(annot)
            y_annot = self._inflate_mask_dim(y_annot)

            X[n] = x_img

            y[n] = y_annot

            n += 1

        return X, y

    def train_generator(self, dataset_dir, expand_train_subdirs=None,
                        epochs=1, batch_size=1):
        train_gen, valid_gen = self._get_data_generators(
            dataset_dir=dataset_dir,
            expand_train_subdirs=expand_train_subdirs,
            batch_size=batch_size
            )

        callbacks = [
            ModelCheckpoint(
                str(self._model_path),
                verbose=1,
                monitor='val_categorical_accuracy',  # ('val_los', 'val_categorical_accuracy')
                save_best_only=True
                # save_weights_only=True
                )
        ]

        use_multiprocessing = False
        if valid_gen:
            results = self.model.fit_generator(
                generator=train_gen,
                validation_data=valid_gen,
                use_multiprocessing=use_multiprocessing,
                epochs=epochs,
                steps_per_epoch=self.num_train_samples // batch_size,
                validation_steps=self.num_valid_samples // batch_size,
                verbose=1,
                callbacks=callbacks
            )
        else:
            print("Splitting training data to cross-validation.")
            results = self.model.fit_generator(
                generator=train_gen,
                epochs=epochs,
                use_multiprocessing=use_multiprocessing,
                steps_per_epoch=self.num_train_samples // batch_size,
                validation_steps=self.num_valid_samples // batch_size,
                verbose=1,
                callbacks=callbacks,
                validation_split=0.1,
                shuffle=True
            )

        self._plot_losses(results, acc=True)
        self._save_train_info(results)

        return results

    def train(self, epochs=1, batch_size=1):

        callbacks = [
            ModelCheckpoint(
                self._model_path,
                verbose=1,
                monitor='val_loss',  # 'val_categorical_accuracy'
                save_best_only=True
                # save_weights_only=True
                )
        ]

        if self.X_valid.any() and self.y_valid.any():
            val_data = (self.X_valid, self.y_valid)
            results = self.model.fit(
                self.X_train,
                self.y_train,
                batch_size=batch_size,
                epochs=epochs,
                verbose=1,
                callbacks=callbacks,
                validation_data=val_data,
                sample_weight=self.sample_weights
            )
        else:
            val_data = None
            print("Splitting training data to cross-validation.")
            results = self.model.fit(
                self.X_train,
                self.y_train,
                batch_size=batch_size,
                epochs=epochs,
                verbose=1,
                callbacks=callbacks,
                validation_split=0.1,
                shuffle=True
            )

        self._plot_losses(results, acc=True)
        self._save_train_info(results)

        print(f"Best model saved to {self._model_path}.")

        return results

    def _inflate_mask_dim(self, mask):

        inflated = np.repeat(mask, self.num_classes, axis=2)

        for i in range(self.num_classes):
            class_mask = inflated[:, :, i]
            class_mask[class_mask != i] = 0
            inflated[:, :, i] = class_mask  # Unneccessary?

        return inflated

    def _deflate_mask_dim(self, mask):

        # TODO: replace with to_categorical?

        deflated = np.zeros((mask.shape[0], mask.shape[1], 1), dtype=np.uint8)

        for i in range(self.num_classes):
            # deflated[:, :] += np.amax(mask, axis=2)*i
            # class_mask = mask[:, :, i, np.newaxis]
            mask[:, :, i] *= i
            deflated[:, :, 0] += mask[:, :, i]

        return deflated

    def segment_one(self, image):
        """ Segments one image.

        Argumets:
            image: NumPy array of input image.
        """

        # exp_shape = (self.img_height, self.img_width, 3)
        # if image.shape != exp_shape:
        #     print(f"Wrong image shape! Expected {exp_shape}, "
        #           f"got {image.shape}.")
        #     return False

        if self.model is None:
            print("Cannot segment, no model available!")
            return False

        batch_size = 1
        proba_img = self.model.predict(
            np.asarray([image]), verbose=1, batch_size=batch_size
            )

        pred_img = proba_img.argmax(axis=-1).astype(np.uint8)[0]

        return (pred_img, proba_img)

    def segment_generator(self, save_rgb, dataset_dir=None, conf_mat=False):
        """ Segments validation images.

        Arguments:
            save_rgb: If True, rgb segmentation masks are saved also.
            dataset_dir: Path to dataset directory. Validation images are
            loaded and segmented from this path.
            conf_mat: If True, confusion matrix is computed. Can take large
            amount of memory and computation power!
        """
        seg_batch_size = 1

        if dataset_dir is None:
            dataset_dir = self.dataset_path

        _, valid_gen = self._get_data_generators(
            dataset_dir=dataset_dir,
            batch_size=seg_batch_size,
            randomize=False
            )

        if self.model is None:
            print("Cannot segment, no model available!")
            return False

        use_multiprocessing = False

        print(f"num_valid_samples={self.num_valid_samples}")

        # TODO: move this to trianing??
        print("Evaluating...")
        score = self.model.evaluate_generator(
            generator=valid_gen,
            steps=self.num_valid_samples // seg_batch_size,
            verbose=1,
            use_multiprocessing=use_multiprocessing
            )
        print('Validation loss:', score[0])
        print('Validation accuracy:', score[1])

        _, valid_gen = self._get_data_generators(
            dataset_dir=dataset_dir,
            batch_size=1,
            randomize=False
            )

        print("Predicting...")
        proba_val = self.model.predict_generator(
            generator=valid_gen,
            steps=self.num_valid_samples // seg_batch_size,
            verbose=1,
            use_multiprocessing=use_multiprocessing
            )

        preds_val = proba_val.argmax(axis=-1).astype(np.uint8)

        if conf_mat:
            print("Computing confusion matrix...")
            valid_data_path = self.dataset_path / "validation"
            checked_data = self._check_img_annots([valid_data_path])
            _, y_labels = self._load_data_dirs(checked_data)
            true_val = y_labels.argmax(axis=-1).astype(np.uint8)

            self.conf_matrix = self._get_conf_matrix_all(
                true_val,
                preds_val,
                save_csv=True
                )

        self._init_segmentation_folder_tree()

        if save_rgb:
            segm_root_path = self._ML_ROOT_PATH / "Segmentation"
            self._load_surfaces_config(segm_root_path / "seg_annot_labels.json")

        # Save segmented images
        print("Saving segmented images...")
        for i, p in enumerate(tqdm(preds_val)):
            f_name = self.valid_img_fnames[i]
            self._save_mask(p, f_name, train=False, rgb=save_rgb)

        return True

    def segment(self, seg_all=False, model_path=None, save_rgb=False):

        if not self.X_train.any() or not self.y_train.any():
            print("Training data not loaded!")

            # FIXME: loading will fail, if no validation data is provided
            # self.load_data(self.dataset_path)
            seg_all = False

        if self.X_valid.any() and self.y_valid.any():
            segment_valid = True
        else:
            segment_valid = False
            print("Validation data invalid")

        # Load best model
        if self.model is None:
            print(f"Model loaded from {model_path}")
            self.model = load_model(model_path)
        else:
            # TODO: delete this, use only all-in-one model
            print(f"Weights loaded from {model_path}")
            self.model.load_weights(model_path)

        batch_size = 1

        if segment_valid:
            print("Evaluating...")
            score = self.model.evaluate(
                self.X_valid, self.y_valid, verbose=1, batch_size=batch_size
                )
            print('Validation loss:', score[0])
            print('Validation accuracy:', score[1])

        # Predict on train and validation set
        print("Predicting...")
        if seg_all:
            proba_train = self.model.predict(
                self.X_train, verbose=1, batch_size=batch_size
                )
        if segment_valid:
            proba_val = self.model.predict(
                self.X_valid, verbose=1, batch_size=batch_size
                )

        K.clear_session()  # test?
        if seg_all:
            preds_train = proba_train.argmax(axis=-1).astype(np.uint8)
        if segment_valid:
            preds_val = proba_val.argmax(axis=-1).astype(np.uint8)

            print("Computing confusion matrix...")
            self.conf_matrix = self._get_conf_matrix_all(
                self.y_valid.argmax(axis=-1).astype(np.uint8),
                preds_val,
                save_csv=True
                )
            # self.conf_mat_vals = np.unique([*self.y_valid.argmax(axis=-1),
            #                                 *preds_val])

        # FIXME: take only best guess
        # preds_train = (preds_train > 0.5).astype(np.uint8)
        # preds_val = (preds_val > 0.5).astype(np.uint8)

        self._init_segmentation_folder_tree()

        print("Saving segmented files.")

        if save_rgb:
            segm_root_path = self._ML_ROOT_PATH / "Segmentation"
            self._load_surfaces_config(segm_root_path / "seg_annot_labels.json")

        if seg_all:
            for i, p in enumerate(tqdm(preds_train)):
                f_name = self.train_img_fnames[i]
                self._save_mask(p, f_name, True, save_rgb)

        if segment_valid:
            for i, p in enumerate(tqdm(preds_val)):
                f_name = self.valid_img_fnames[i]
                self._save_mask(p, f_name, False, save_rgb)

        return True

    def _get_conf_matrix_all(self, y_true, y_pred, save_csv):
        """ Get confusion matrix from true a predicted labels.

        Confusion matrix:

        _______| pred_A | pred_B | ...
        true_A |    x   |    x   | ...
        true_B |    x   |    x   | ...
          ...  |   ...  |   ...  | ...
        """

        assert len(y_true) == len(y_pred), \
            "y_true and y_pred are not the same length!"

        conf_mat_all = confusion_matrix(
            y_true.flatten(),
            y_pred.flatten(),
            labels=range(self.num_classes)).astype(np.uint64,)

        self.conf_mat_vals = range(self.num_classes)

        conf_mat_out = []
        true = [str(f"true_{i}") for i in self.conf_mat_vals]
        pred = [str(f"pred_{i}") for i in self.conf_mat_vals]

        conf_mat_out.append(["X", *pred])
        for i in self.conf_mat_vals:
            row = [true[i], *conf_mat_all[i]]
            conf_mat_out.append(row)

        if save_csv:
            with open(self._out_path / 'conf_mat.csv', 'w') as outfile:
                writer = csv.writer(outfile, delimiter=',')
                writer.writerows(conf_mat_out)

        return conf_mat_all, conf_mat_out

    def _save_mask(self, mask, fname, train=True, rgb=False):
        """ Save mask to file.
        """
        # TODO: make method public, don't save here, just return mask
        if train is True:
            folder = "training"
        else:
            folder = "validation"

        img = Image.fromarray(mask, 'L')
        f_path = self.dataset_path / "_segmentation" / folder / "masks"
        img.save(f_path / (fname + ".png"), "PNG")
        # np.savetxt(f_path / (fname + ".txt"), mask, delimiter=' ', fmt="%d")
        if rgb:
            rgb_mask = np.zeros(
                (mask.shape[0], mask.shape[1], 3),
                dtype=np.uint8
            )
            colors = self.id_color_map
            colors[0] = [255, 255, 255]
            for i in range(self.num_classes):
                # if i == 1:
                #     print(sum(mask[mask[:,:]==i]))
                color = colors.get(i)
                if not color:
                    print(f"No color available for surface with ID {i}")
                    color = [255, 255, 255]
                rgb_mask[:, :, 0] += ((mask[:, :] == i)*(color[0])).astype(np.uint8)
                rgb_mask[:, :, 1] += ((mask[:, :] == i)*(color[1])).astype(np.uint8)
                rgb_mask[:, :, 2] += ((mask[:, :] == i)*(color[2])).astype(np.uint8)

            img = Image.fromarray(rgb_mask, "RGB")
            f_path = self.dataset_path / "_segmentation" / folder / "masks_rgb"
            img.save(f_path / (fname + ".png"), "PNG")

        return True

    def _load_surfaces_config(self, config_file):

        id_color_map = {}
        with open(config_file, "r") as config_file:
            config_data = json.load(config_file)
            for surface in config_data["labels"].values():
                id_color_map[surface["id"]] = surface["color"]

        self.id_color_map = id_color_map
        return True

    def _init_segmentation_folder_tree(self):
        """ Delete and initialize _segmentation folder.
        """
        segm_path = self.dataset_path / "_segmentation"
        if not segm_path.is_dir():
            try:
                os.mkdir(segm_path)
            except OSError:
                print(f"Creation of the directory {segm_path} failed.")
                return False

        shutil.rmtree(segm_path)

        try:
            os.makedirs(segm_path / "training/masks")
            os.makedirs(segm_path / "training/masks_rgb")
            os.makedirs(segm_path / "validation/masks")
            os.makedirs(segm_path / "validation/masks_rgb")
        except OSError:
            print("Cannot make _segmentation folder tree.")
            shutil.rmtree(segm_path)
            return False

    def _plot_losses(self, results, acc=False):

        plt.figure(figsize=(10.24, 7.68))
        plt.style.use("ggplot")
        plt.title("Learning curve - losses")
        plt.plot(results.history["loss"], label="loss")
        plt.plot(results.history["val_loss"], label="val_loss")
        plt.plot(np.argmin(results.history["val_loss"]),
                 np.min(results.history["val_loss"]),
                 marker="x",
                 color="r",
                 label="best model")

        plt.xlabel("Epochs")
        plt.ylabel("log_loss")
        plt.legend()

        plt.savefig(str(Path(self._out_path) / "losses_graph.png"), dpi=100)

        if acc:
            plt.figure(figsize=(10.24, 7.68))
            plt.style.use("ggplot")
            plt.title("Learning curve - categorical accuracy")

            plt.plot(results.history["categorical_accuracy"],
                     label="categorical_accuracy")
            plt.plot(results.history["val_categorical_accuracy"],
                     label="val_categorical_accuracy")
            plt.xlabel("Epochs")
            plt.ylabel("Accuracy")
            plt.legend()

            plt.savefig(str(Path(self._out_path) / "cat_acc_graph.png"), dpi=100)

    def _save_train_info(self, results, save_hist=True):

        loss = results.history["loss"]
        val_loss = results.history["val_loss"]
        categorical_accuracy = results.history["categorical_accuracy"]
        val_categorical_accuracy = results.history["val_categorical_accuracy"]

        epochs = range(1, len(loss) + 1)
        best_epoch = np.argmin(val_loss)

        data = {
            "num_epochs": len(epochs),
            "best_epoch": int(best_epoch),
            "val_loss": float(val_loss[best_epoch]),
            "val_acc": float(val_categorical_accuracy[best_epoch]),
            "train_dirs": str([str(d) for d in self._train_dirs]),
            "real_data_sample_weight": float(self._REAL_WEIGHT),
            "model_file": str(self._model_path)
        }

        with open(Path(self._out_path) / 'info.txt', 'w') as outfile:
            json.dump(data, outfile)

        if save_hist:
            # Save training history to .csv file
            out_fpath = Path(self._out_path) / "train_hist.csv"
            with open(out_fpath, mode='w') as out_file:
                writer = csv.writer(out_file, delimiter=',')
                header = [
                    "loss",
                    "val_loss",
                    "categorical_accuracy",
                    "val_categorical_accuracy"
                    ]
                writer.writerow(header)

                for e in epochs:
                    data = [
                        loss[e-1],
                        val_loss[e-1],
                        categorical_accuracy[e-1],
                        val_categorical_accuracy[e-1]
                        ]
                    writer.writerow(data)

        return True


if __name__ == "__main__":

    # TODO: separate this to a separate file 'segtools.py'

    parser = argparse.ArgumentParser(description='Keras segmentation CLI.')

    parser.add_argument('-m', '--model', dest='model_path', type=str,
                        action='store', default="model_segm.h5",
                        help="Path for saving/loading training model.")
    parser.add_argument('-t', '--train', dest='train', action='store_true',
                        default=False, help='Model shall be trained.')
    parser.add_argument('-s', '--segment', dest='segment', action='store_true',
                        default=False, help='Segmentation shall be performed.')

    parser.add_argument('-w', '--weights', dest='real_weight', type=float,
                        action='store', default=10,
                        help="Set sample weight of real training data.")
    parser.add_argument('-e', '--epochs', dest='epochs', type=int,
                        action='store', default=10,
                        help="Number of epochs to train.")
    parser.add_argument('-d', '--dataset', dest='dataset', type=str,
                        action='store', default="../Datasets/dataset_05_09_up",
                        help="Path to training dataset.")
    parser.add_argument('-r', '---realdata', dest='real_data',
                        action='store', default=None, nargs='+',
                        help="Path to real world data dataset.")
    parser.add_argument('-b', '--batch', dest='batch_size', type=int,
                        action='store', default=5,
                        help="Path to training dataset.")
    parser.add_argument('-o', '--optimizer', dest='optimizer', type=str,
                        action='store', default="adadelta",
                        help="Path to training dataset.")

    args = parser.parse_args()

    print("Initializing")
    segmentator = KerasSegmentator(height=960,
                                   width=640,
                                   num_classes=14,
                                   model_path=args.model_path,
                                   real_weight=args.real_weight,
                                   var_input=True,
                                   optim=args.optimizer
                                   )
    print("Segmentator initialized")

    if args.train:
        segmentator.train_generator(
            dataset_dir=args.dataset,
            expand_train_subdirs=args.real_data,
            epochs=args.epochs,
            batch_size=args.batch_size
            )

    if args.segment:
        segmentator.segment_generator(
            dataset_dir=args.dataset,
            save_rgb=True,
            conf_mat=True)  # Disable to segment faster

    input("Press enter to exit...")
