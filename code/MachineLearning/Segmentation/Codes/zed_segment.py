# from SynthetizeImages import PerspTransform
import cv2
import matplotlib.pyplot as plt
import numpy as np
import pyzed.sl as sl
from PIL import Image
from segmentator import KerasSegmentator

from scipy import stats


def get_front_surface(seg_img, d, r):

    img_size = seg_img.shape

    if r > d or (d + r) > img_size[1]:
        raise ValueError("Invalid combination of 'r' and 'd' argument!")

    y1 = img_size[0] - d - r
    y2 = img_size[0] - d + r
    x1 = (img_size[1] // 2) - r
    x2 = (img_size[1] // 2) + r

    area = seg_img[y1:y2+1, x1:x2+1]

    mode, count = stats.mode(area, axis=None)

    area_size = (2*r+1)**2

    accuracy = count[0] / area_size

    res = mode[0]

    return (res, accuracy)


def get_warp_transform(IMAGE_W, IMAGE_H, IMAGE_H_DEFORM):

    p01 = [570, 0]
    p02 = [685, 0]
    p03 = [181, 394]
    p04 = [1072, 403]

    percentalBorder = 0.35
    p11 = [IMAGE_W * percentalBorder, 0]
    p12 = [IMAGE_W - (IMAGE_W * percentalBorder), 0]
    p13 = [IMAGE_W * percentalBorder, IMAGE_H_DEFORM]
    p14 = [IMAGE_W - (IMAGE_W * percentalBorder), IMAGE_H_DEFORM]
    src = np.float32([p01, p02, p03, p04])
    dst = np.float32([p11, p12, p13, p14])

    M = cv2.getPerspectiveTransform(src, dst)

    return M


# Init transform
cut = 300
IMAGE_W = 1280
IMAGE_H = 720 - cut
IMAGE_H_DEFORM = round(IMAGE_W*(3/2))

M = get_warp_transform(IMAGE_W, IMAGE_H, IMAGE_H_DEFORM)

# Init ZED camera parameters
init = sl.InitParameters()
init.camera_resolution = sl.RESOLUTION.RESOLUTION_HD720
init.camera_linux_id = 0
init.camera_fps = 5

camera = sl.Camera()
if not camera.is_opened():
    print("Opening ZED Camera...")
status = camera.open(init)
if status != sl.ERROR_CODE.SUCCESS:
    print(repr(status))

mat = sl.Mat()

# segmentator = KerasSegmentator(height=960, width=640, num_classes=12,
#                                model_path="model_e75_w30.h5",
#                                real_weight=30)
segmentator = KerasSegmentator(height=960, width=640, num_classes=12,
                               model_path="model_var_in.h5",
                               real_weight=30)

runtime = sl.RuntimeParameters()
# Disabling computing depth image significantly improves
# image obtaining speed
runtime.enable_depth = False

plot = 0

while True:
    err = camera.grab(runtime)

    # If grabbing image successfull, save to buffer
    if err == sl.ERROR_CODE.SUCCESS:

        camera.retrieve_image(mat, sl.VIEW.VIEW_RIGHT)

        image = mat.get_data()

        img = image[0:720, 0:1280, 0:3]

        img = img[cut:(cut+IMAGE_H), 0:IMAGE_W]

        if plot > 2:
            plt.imshow(img)
            plt.show()

        # imgTrans = PerspTransform(imgLeft, True)

        img = cv2.warpPerspective(img, M,
                                  (IMAGE_W, IMAGE_H_DEFORM))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        if plot > 1:
            plt.imshow(img)
            plt.show()

        # img = cv2.resize(img, (640, 960))
        img = cv2.resize(img, (384, 768))

        out = segmentator.segment_one(np.asarray(img))[0]

        res, acc = get_front_surface(out, 150, 50)

        print(f"Surface {res} with {acc*100} % accuracy.")

        if plot > 0:
            plt.imshow(out)
            plt.show()
