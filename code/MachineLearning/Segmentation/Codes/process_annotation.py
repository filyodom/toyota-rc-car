#!/usr/bin/env python3

import sys
import os
from pathlib import Path
from shutil import copyfile

import cv2

""" This script processes PixelAnnotationTool output to project output
structure.
"""


def _remove_border(img):
    """ Remove border from image by copying neighbour pixels.
    """
    img[0, :] = img[1, :]
    img[-1, :] = img[-2, :]
    img[:, 0] = img[:, 1]
    img[:, -1] = img[:, -2]

    img[0, 0] = img[1, 1]
    img[0, -1] = img[1, -2]
    img[-1, 0] = img[-2, 1]
    img[-1, -1] = img[-2, -2]

    return img


def _process_image(file_path):
    """ Copy image to 'images'.
    """
    file_path = Path(file_path)
    img_suffs = [".jpg", ".JPG", ".jpeg", ".JPEG", ".png", ".PNG"]
    if file_path.suffix not in img_suffs:
        print(f"File {file_path} is not image.")
        return False
    out_file = dir_path / "images" / file_path.name
    copyfile(file_path, out_file)
    print(f"Mask {str(out_file)} saved.")
    return True


def _process_mask(file_path):
    # Remove border
    annot_img = cv2.imread(str(file_path), cv2.IMREAD_GRAYSCALE)
    _remove_border(annot_img)

    # Save annotation mask to file
    file_name = Path(file_path).stem[:-15] + ".png"
    out_file = dir_path / "annotations" / file_name
    cv2.imwrite(str(out_file), annot_img)
    print(f"Mask {str(out_file)} processed and saved.")
    return True


if __name__ == "__main__":

    if len(sys.argv) == 1:
        dir_path = Path(".")
    elif len(sys.argv) == 2:
        dir_path = Path(sys.argv[1])
    else:
        print("Wrong number of arguments!")
        sys.exit()

    file_path_list = [dir_path / p for p in os.listdir(dir_path)]
    print(file_path_list)

    if not file_path_list:
        print("No files found!")

    os.makedirs(dir_path / "images", exist_ok=True)
    os.makedirs(dir_path / "annotations", exist_ok=True)

    for file_path in file_path_list:

        # Skip dirs
        if os.path.isdir(file_path):
            continue

        if "_watershed_mask" in str(file_path):
            _process_mask(file_path)

        elif "_color_mask" in str(file_path):
            continue

        elif "_mask" in str(file_path):
            continue

        else:
            _process_image(file_path)
