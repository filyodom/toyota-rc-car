# Datasets ReadMe
All machine learning segmentation scripts except specific folder structure for datasets.
## Folder structure
Dataset in Datasets folder should have following structure:
```
/mydataset*
    /training
        /annotation
        /images
    /validation
        /annotation
        /images
    /_segmentation
        /training
            /masks
            /masks_rgb
        /validation
            /masks
            /masks_rgb
    /my_training*
        /annotation
        /images
```
Folder *_segmentation* is dedicated to temporary segmented masks and it's content is deleted before every segmentation (NN prediction). It is reccomended to save important files immediately after segmentation to other location.

Folder names marked with '*' can have custom name.

## Data structure
It is required for dataset files (both train and validation) to have specific properties:
* Images are in .jpg or .png format and annotations are in .png format.
* Corresponding files (image and it's annotation) shall have same name, width, height and number of channels.