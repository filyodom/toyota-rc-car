clear
clc
format long g

%% GYRO

ABS_off1 = importdata('data/ABSoff1/meas_data.txt',',',1);
ABS_off2 = importdata('data/ABSoff2/meas_data.txt',',',1);
ABS_off3 = importdata('data/ABSoff3/meas_data.txt',',',1);
ABS_off4 = importdata('data/ABSoff4/meas_data.txt',',',1);
ABS_off5 = importdata('data/ABSoff5/meas_data.txt',',',1);
ABS_off6 = importdata('data/ABSoff6/meas_data.txt',',',1);
ABS_off7 = importdata('data/ABSoff7/meas_data.txt',',',1);
ABS_off8 = importdata('data/ABSoff8/meas_data.txt',',',1);
ABS_off9 = importdata('data/ABSoff9/meas_data.txt',',',1);
ABS_off10 = importdata('data/ABSoff10/meas_data.txt',',',1);
ABS_off11 = importdata('data/ABSoff11/meas_data.txt',',',1);
ABS_off12 = importdata('data/ABSoff12/meas_data.txt',',',1);
ABS_off13 = importdata('data/ABSoff13/meas_data.txt',',',1);
ABS_off14 = importdata('data/ABSoff14/meas_data.txt',',',1);
ABS_off15 = importdata('data/ABSoff15/meas_data.txt',',',1);
ABS_on1 = importdata('data/ABSon1/meas_data.txt',',',1);
ABS_on2 = importdata('data/ABSon2/meas_data.txt',',',1);
ABS_on3 = importdata('data/ABSon3/meas_data.txt',',',1);
ABS_on4 = importdata('data/ABSon4/meas_data.txt',',',1);
ABS_on5 = importdata('data/ABSon5/meas_data.txt',',',1);
ABS_on6 = importdata('data/ABSon6/meas_data.txt',',',1);
ABS_on7 = importdata('data/ABSon7/meas_data.txt',',',1);
ABS_on8 = importdata('data/ABSon8/meas_data.txt',',',1);
ABS_on9 = importdata('data/ABSon9/meas_data.txt',',',1);
ABS_on10 = importdata('data/ABSon10/meas_data.txt',',',1);
ABS_on11 = importdata('data/ABSon11/meas_data.txt',',',1);
ABS_on12 = importdata('data/ABSon12/meas_data.txt',',',1);
ABS_on13 = importdata('data/ABSon13/meas_data.txt',',',1);
ABS_on14 = importdata('data/ABSon14/meas_data.txt',',',1);
ABS_on15 = importdata('data/ABSon15/meas_data.txt',',',1);
ABS_spatna1 = importdata('data/ABSspatna1/meas_data.txt',',',1);
ABS_spatna2 = importdata('data/ABSspatna2/meas_data.txt',',',1);
ABS_spatna3 = importdata('data/ABSspatna3/meas_data.txt',',',1);
ABS_spatna4 = importdata('data/ABSspatna4/meas_data.txt',',',1);
ABS_spatna5 = importdata('data/ABSspatna5/meas_data.txt',',',1);
ABS_spatna6 = importdata('data/ABSspatna6/meas_data.txt',',',1);
ABS_spatna7 = importdata('data/ABSspatna7/meas_data.txt',',',1);
ABS_spatna8 = importdata('data/ABSspatna8/meas_data.txt',',',1);
ABS_spatna9 = importdata('data/ABSspatna9/meas_data.txt',',',1);
ABS_spatna10 = importdata('data/ABSspatna10/meas_data.txt',',',1);

%ABS_on = {ABS_on1.data,ABS_on2.data,ABS_on3.data,ABS_on4.data,ABS_on5.data,ABS_on6.data,ABS_on8.data,ABS_on9.data,ABS_on11.data};
%ABS_off = {ABS_off1.data,ABS_off5.data,ABS_off6.data,ABS_off7.data,ABS_off10.data,ABS_off12.data,ABS_off13.data,ABS_off15.data};
%ABS_spatna = {ABS_spatna1.data,ABS_spatna2.data,ABS_spatna3.data,ABS_spatna4.data,ABS_spatna6.data,ABS_spatna7.data,ABS_spatna8.data,ABS_spatna9.data,ABS_spatna10.data};

ABS_on = {ABS_on1.data,ABS_on2.data,ABS_on3.data,ABS_on4.data,ABS_on5.data,ABS_on6.data,ABS_on7.data,ABS_on8.data,ABS_on9.data,ABS_on10.data,ABS_on11.data,ABS_on12.data,ABS_on13.data,ABS_on14.data,ABS_on15.data};
ABS_off = {ABS_off1.data,ABS_off2.data,ABS_off3.data,ABS_off4.data,ABS_off5.data,ABS_off6.data,ABS_off7.data,ABS_off8.data,ABS_off9.data,ABS_off10.data,ABS_off11.data,ABS_off12.data,ABS_off13.data,ABS_off14.data,ABS_off15.data};
ABS_spatna = {ABS_spatna1.data,ABS_spatna2.data,ABS_spatna3.data,ABS_spatna4.data,ABS_spatna5.data,ABS_spatna6.data,ABS_spatna7.data,ABS_spatna8.data,ABS_spatna9.data,ABS_spatna10.data};


for k = 1:length(ABS_on)
    ABS_on{1,k}(:,1) = ABS_on{1,k}(:,1) - ABS_on{1,k}(1,1);
end

for k = 1:length(ABS_off)
    ABS_off{1,k}(:,1) = ABS_off{1,k}(:,1) - ABS_off{1,k}(1,1);
end

for k = 1:length(ABS_spatna)
    ABS_spatna{1,k}(:,1) = ABS_spatna{1,k}(:,1) - ABS_spatna{1,k}(1,1);
end

for i = 1:length(ABS_on)
    for j = 1:length(ABS_on{1,i}(:,1))
        if ABS_on{1,i}(j,31) > 10000
            ABS_on{1,i}(j,31) = ABS_on{1,i}(j-1,31);
        end
        if ABS_on{1,i}(j,31) > 1700
            %ABS_on_turn(:,:,i) = ABS_on{1,i}(j-50:j+200,:);
            time = ABS_on{1,i}(j,1);
            for m = j:length(ABS_on{1,i})
                ABS_on_turn{1,i}(m-j+1,:) = ABS_on{1,i}(m,:);
                if ABS_on{1,i}(m,1) - time > 2.5
                    break
                end
            end
            break
        end
    end
end

for i = 1:length(ABS_off)
    for j = 1:length(ABS_off{1,i}(:,1))
        if ABS_off{1,i}(j,31) > 10000
            ABS_off{1,i}(j,31) = ABS_off{1,i}(j-1,31);
        end
        if ABS_off{1,i}(j,31) > 1700 || ABS_off{1,i}(j,31) < 1300
            time = ABS_off{1,i}(j,1);
            for m = j:length(ABS_off{1,i})
                ABS_off_turn{1,i}(m-j+1,:) = ABS_off{1,i}(m,:);
                if ABS_off{1,i}(m,1) - time > 2.5
                    break
                end
            end
            break
        end
    end
end

for i = 1:length(ABS_spatna)
    for j = 1:length(ABS_spatna{1,i}(:,1))
        if ABS_spatna{1,i}(j,31) > 10000
            ABS_spatna{1,i}(j,31) = ABS_spatna{1,i}(j-1,31);
        end
        if ABS_spatna{1,i}(j,31) > 1700 || ABS_spatna{1,i}(j,31) < 1200
            time = ABS_spatna{1,i}(j,1);
            for m = j:length(ABS_spatna{1,i})
                ABS_spatna_turn{1,i}(m-j+1,:) = ABS_spatna{1,i}(m,:);
                if ABS_spatna{1,i}(m,1) - time > 2.5
                    break
                end
            end
            break
        end
    end
end

for k = 1:length(ABS_on_turn)
    ABS_on_turn{1,k}(:,1) = ABS_on_turn{1,k}(:,1) - ABS_on_turn{1,k}(1,1);
end

for k = 1:length(ABS_off_turn)
    ABS_off_turn{1,k}(:,1) = ABS_off_turn{1,k}(:,1) - ABS_off_turn{1,k}(1,1);
end

for k = 1:length(ABS_spatna_turn)
    ABS_spatna_turn{1,k}(:,1) = ABS_spatna_turn{1,k}(:,1) - ABS_spatna_turn{1,k}(1,1);
end

subplot(2,1,1);
title('Gyro 1');
xlabel('Time [s]');
ylabel('Angle [rad]');
set(gca,'FontSize',15);
hold on;
grid on;
xlim([0 2.5]);

for l = 1:length(ABS_on_turn)
    g = ABS_on_turn{1,l}(:,10) - 0.01;
    t = ABS_on_turn{1,l}(:,1);
    g = medfilt1(g, 5);
    angle = cumtrapz(t,g);
    a = plot(t, angle, 'LineWidth', 1.5, 'Color', 'r');
end

%legend('Blue Meanies');

for l = 1:length(ABS_off_turn)
    g = ABS_off_turn{1,l}(:,10) - 0.01;
    t = ABS_off_turn{1,l}(:,1);
    g = medfilt1(g, 5);
    angle = cumtrapz(t,g);
    b = plot(t, angle, 'LineWidth', 1.5, 'Color', 'b');
end

for l = 1:length(ABS_spatna_turn)
    g = ABS_spatna_turn{1,l}(:,10) - 0.01;
    t = ABS_spatna_turn{1,l}(:,1);
    g = medfilt1(g, 5);
    angle = cumtrapz(t,g);
    c = plot(t, angle, 'LineWidth', 1.5, 'Color', 'g');
end

legend([a,b,c],{'ABS on','ABS off','ABS wrong params'});

subplot(2,1,2);
title('Gyro 2');
xlabel('Time [s]');
ylabel('Angle [rad]');
set(gca,'FontSize',15);
hold on;
grid on;
xlim([0 2.5]);

for l = 1:length(ABS_on_turn)
    g = ABS_on_turn{1,l}(:,13) - 0.02;
    t = ABS_on_turn{1,l}(:,1);
    g = medfilt1(g, 5);
    angle = cumtrapz(t,g);
    max_angle_abs_on(l,1) = angle(end,1);
    plot(t, angle, 'LineWidth', 1.5, 'Color', 'r');
end

for l = 1:length(ABS_off_turn)
    g = ABS_off_turn{1,l}(:,13) - 0.02;
    t = ABS_off_turn{1,l}(:,1);
    g = medfilt1(g, 5);
    angle = cumtrapz(t,g);
    max_angle_abs_off(l,1) = angle(end,1);
    plot(t, angle, 'LineWidth', 1.5, 'Color', 'b');
end

for l = 1:length(ABS_spatna_turn)
    g = ABS_spatna_turn{1,l}(:,13) - 0.02;
    t = ABS_spatna_turn{1,l}(:,1);
    g = medfilt1(g, 5);
    angle = cumtrapz(t,g);
    max_angle_abs_wrong(l,1) = angle(end,1);
    plot(t, angle, 'LineWidth', 1.5, 'Color', 'g');
end

sgtitle('Right turn','FontSize',20);

%% BOXPLOT

max_angle_abs_wrong = [max_angle_abs_wrong; NaN; NaN; NaN; NaN; NaN];

figure;
boxplot([max_angle_abs_off,max_angle_abs_wrong,max_angle_abs_on],'Labels',{'ABS off','ABS wrong','ABS on'});
ylabel('Heading after the turn [rad]');
set(gca,'FontSize',15);
title('Right turn','FontSize',20);

%% RPM

A = 10;

figure;
subplot(3,2,1);
plot(ABS_off_turn{1,3}(:,1),ABS_off_turn{1,3}(:,33), 'Color', 'b');
title('ABS off');
xlabel('Time [s]');
ylabel('Velocity [rpm]');
set(gca,'FontSize',10);
grid on;
xlim([0 2.5]);
ylim([0 10000]);

subplot(3,2,2);
plot(ABS_off_turn{1,3}(:,1),abs(ABS_off_turn{1,3}(:,36) - 1720), 'Color', 'r');
title('ABS off');
xlabel('Time [s]');
ylabel('Throttle reference');
set(gca,'FontSize',10);
grid on;
xlim([0 2.5]);
ylim([0 800]);

subplot(3,2,3);
plot(ABS_spatna_turn{1,A}(:,1),ABS_spatna_turn{1,A}(:,33), 'Color', 'b');
title('ABS wrong');
xlabel('Time [s]');
ylabel('Velocity [rpm]');
set(gca,'FontSize',10);
grid on;
xlim([0 2.5]);
ylim([0 10000]);

subplot(3,2,4);
plot(ABS_spatna_turn{1,A}(:,1),abs(ABS_spatna_turn{1,A}(:,36) - 1720), 'Color', 'r');
title('ABS wrong');
xlabel('Time [s]');
ylabel('Throttle reference');
set(gca,'FontSize',10);
grid on;
xlim([0 2.5]);
ylim([0 800]);

subplot(3,2,5);
plot(ABS_on_turn{1,2}(:,1),ABS_on_turn{1,2}(:,33), 'Color', 'b');
title('ABS on');
xlabel('Time [s]');
ylabel('Velocity [rpm]');
set(gca,'FontSize',10);
grid on;
xlim([0 2.5]);
ylim([0 10000]);

subplot(3,2,6);
plot(ABS_on_turn{1,2}(:,1),abs(ABS_on_turn{1,2}(:,36) - 1720), 'Color', 'r');
title('ABS on');
xlabel('Time [s]');
ylabel('Throttle reference');
set(gca,'FontSize',10);
grid on;
xlim([0 2.5]);
ylim([0 800]);