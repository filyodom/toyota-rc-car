ABS_off= importdata('ABSoffZatacka/meas_data.txt',',',1);
ABS_on = importdata('ABSonZatacka/meas_data.txt',',',1);

t_off = ABS_off.data(:,1);
t_on = ABS_on.data(:,1);

gx1_off = ABS_off.data(:,8);
gy1_off = ABS_off.data(:,9);
gz1_off = ABS_off.data(:,10);
gx1_on = ABS_on.data(:,8);
gy1_on = ABS_on.data(:,9);
gz1_on = ABS_on.data(:,10);

gx2_off = ABS_off.data(:,11);
gy2_off = ABS_off.data(:,12);
gz2_off = ABS_off.data(:,13);
gx2_on = ABS_on.data(:,11);
gy2_on = ABS_on.data(:,12);
gz2_on = ABS_on.data(:,13);

gx1_off = medfilt1(gx1_off, 100) + 0.0074;
gy1_off = medfilt1(gy1_off, 100) - 0.0096;
gz1_off = medfilt1(gz1_off, 100) - 0.0149;

gx1_on = medfilt1(gx1_on, 100) + 0.0074;
gy1_on = medfilt1(gy1_on, 100) - 0.0106;
gz1_on = medfilt1(gz1_on, 100) - 0.0170;

gx2_off = medfilt1(gx2_off, 100) - 0.0183;
gy2_off = medfilt1(gy2_off, 100) + 0.0464;
gz2_off = medfilt1(gz2_off, 100) - 0.0305;

gx2_on = medfilt1(gx2_on, 100) - 0.0195;
gy2_on = medfilt1(gy2_on, 100) + 0.0476;
gz2_on = medfilt1(gz2_on, 100) - 0.0318;

%plot(t_on,gz1_on);
%plot(t_on,gz2_on);
%plot(t_off,gz2_off);

gx1_off_angle = cumtrapz(t_off,gx1_off);
gy1_off_angle = cumtrapz(t_off,gy1_off);
gz1_off_angle = cumtrapz(t_off,gz1_off);

gx1_on_angle = cumtrapz(t_on,gx1_on);
gy1_on_angle = cumtrapz(t_on,gy1_on);
gz1_on_angle = cumtrapz(t_on,gz1_on);

gx2_off_angle = cumtrapz(t_off,gx2_off);
gy2_off_angle = cumtrapz(t_off,gy2_off);
gz2_off_angle = cumtrapz(t_off,gz2_off);

gx2_on_angle = cumtrapz(t_on,gx2_on);
gy2_on_angle = cumtrapz(t_on,gy2_on);
gz2_on_angle = cumtrapz(t_on,gz2_on);

figure
hold on

plot(t_off,gx1_off_angle);
plot(t_off,gy1_off_angle);
plot(t_off,gz1_off_angle);

figure
hold on

plot(t_on,gx1_on_angle);
plot(t_on,gy1_on_angle);
plot(t_on,gz1_on_angle);

figure
hold on

plot(t_off,gx2_off_angle);
plot(t_off,gy2_off_angle);
plot(t_off,gz2_off_angle);

figure
hold on

plot(t_on,gx2_on_angle);
plot(t_on,gy2_on_angle);
plot(t_on,gz2_on_angle);