%% GYRO

ABS_off= importdata('ABSoffZatackaDlazba/meas_data.txt',',',1);
ABS_on = importdata('ABSonZatackaDlazba/meas_data.txt',',',1);

t_off = ABS_off.data(:,1);
t_on = ABS_on.data(:,1) - 129;

gz1_off = ABS_off.data(:,10);
gz1_on = ABS_on.data(:,10);
gz2_off = ABS_off.data(:,13);
gz2_on = ABS_on.data(:,13);

gz1_off = medfilt1(gz1_off, 50) - 0.0149;
gz1_on = medfilt1(gz1_on, 50) - 0.0170;
gz2_off = medfilt1(gz2_off, 50) - 0.0305;
gz2_on = medfilt1(gz2_on, 50) - 0.0318;

gz1_off_angle = cumtrapz(t_off,gz1_off);
gz1_on_angle = cumtrapz(t_on,gz1_on);
gz2_off_angle = cumtrapz(t_off,gz2_off);
gz2_on_angle = cumtrapz(t_on,gz2_on);

figure
hold on

plot(t_off, gz1_off_angle, 'LineWidth', 2);
plot(t_on, gz1_on_angle, 'LineWidth', 2);
plot(t_off, gz2_off_angle, 'LineWidth', 2);
plot(t_on, gz2_on_angle, 'LineWidth', 2);

%% PWM

figure
hold on

plot(t_on, ABS_on.data(:,36), 'LineWidth', 2);
plot(t_on, medfilt1(ABS_on.data(:,33), 1), 'LineWidth', 2);

%% CAMERA

ABS_off= importdata('ABSoffZatackaDlazba/zed_recording.csv',',',1);
ABS_on = importdata('ABSonZatackaDlazba/zed_recording.csv',',',1);

t_off = ABS_off.data(:,7);
t_on = ABS_on.data(:,7) - 130;

figure
hold on

plot(t_off, medfilt1(ABS_off.data(:,6), 5), 'LineWidth', 2);
plot(t_on, medfilt1(ABS_on.data(:,6), 5), 'LineWidth', 2);