A = importdata('/Users/dominik/ownCloud/skola/toyota/toyota-rc-car/tests/rpm_pwm/meas_data.csv',';',1);
figure
hold on
xlim([1520 1720])
ylim([0 18000])

RPM = A.data(494:2360,34);
PWM = A.data(494:2360,36);
scatter(PWM,RPM,20,'filled');
x1 = linspace(1520,1720);
y1 = 137*x1-213260;
plot(x1,y1,'LineWidth',2);
