%topview = imread("output/output_perspective_birdsEye105.jpg");
topview = imread("stin/output_perspective_birdsEye177.jpg");
topview = rgb2gray(topview);

seg_image = zeros(length(topview), length(topview(1,:)), 'uint8');


for row = 1:length(topview)
    for col = 1:length(topview(1,:))
        if topview(row,col) < 125
            seg_image(row,col) = 0;
        else
            seg_image(row,col) = 255;
        end
    end
end

num = 30
W = (1/num^2)*ones(num);

conv_image = conv2(seg_image, W);
conv_image = uint8(conv_image);

final_image = zeros(length(topview), length(topview(1,:)), 'uint8');

for row = 1:length(topview)
    for col = 1:length(topview(1,:))
        if conv_image(row,col) < 255 / 2
            final_image(row,col) = 0;
        else
            final_image(row,col) = 255;
        end
    end
end

imshow(final_image)