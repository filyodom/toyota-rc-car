# Git repository maintanance notes

## Renaming and deleting

Do not rename folders in file explorer, but do it in terminal with command:
```
git mv foo/ bar/ # renames folder 'foo' to 'bar'
```

The same applies for deleting files or folders:
```
git rm foo # removes file 'foo'
git rm -r bar/ # removes folder 'bar'
```

Otherwise, local and remote repository will be different.
After this, you can commit and push changes.

## Pushing code
To avoid creating "train stations" (messy branch merging places), it is recoomended to take following steps:

1) **Commit** your changes in your working branch by `git commit ...` or `git gui`. Best practice is not to name it *master*.

2) **Fetch** changes from repository by `git fetch`. This downloads pushed changes from other people, so your local *master* is different from repository *master* branch and your working branch does not grow from repository *master*.

3) **Stash** uncommited changess by `git stash`. This saves all uncommited changes to temporary memory. It is neccessary for performing next step (rebasing).

4) **Rebase** your branch to repository *master* branch by `git rebase origin/master`. Now your branch grows from repository *master*.

5) **Create** temporary master branch by `git branch master`. It is just another name for your working branch.

6) **Push** your changes to repository by `git push` or `git push origin master`.

7) **Delete** *master* branch by `git branch master -d`. Since your working branch still exists, it will not be deleted by this.

This procedere keeps your work in working branch (with different name then master), but when you want to merge, your changes are pushed to master branch and no train station is created.